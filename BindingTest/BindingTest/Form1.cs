﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BindingTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //binding label to textBox:
            Binding binding = new Binding("Text", //св-во в Target компоненте (целевом компоненте), которое используется для привязки
                                                   //регистронезависимо, только св-во (не public field)
                this.textBox1, //источник
                "Text", //свойство источника данных
                        //регистронезависимо, только св-во (не public field)
                true //применить привязку немедленно, когда пришли данные (важно при получении данных из БД)
                );
            this.label1.DataBindings.Add(binding); //двунаправленная привязка (при изменении одного любого компонента изменяется другой)

            //binding label to class'es public property:
            MyClass myclass = new MyClass("some value of class public property");
            Binding binding2 = new Binding("Text", myclass, "Info",/*name of class public property*/ true);
            this.label2.DataBindings.Add(binding2); //однонаправленная привязка (при интерфейсном изменении отображаемого значения
                                                    //public св-ва его реальное значение внутри класса не изменится;
                                                    //при изменении значения св-ва в классе, изменения отобразятся в интерфейсе)
        }
    }
}
