﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BindingTest
{
    public class MyClass
    {
       private string info;

        public MyClass(string val)
        {
            Info = val;
        }
        protected MyClass()
        {
            Info = string.Empty;
        }

        public string Info
        {
            get
            {
                return info;
            }

            set
            {
                info = value;
            }
        }
    }
}
