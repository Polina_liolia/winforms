﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComboBoxNorthWindLineTester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            cBox_CustomerID.DropDownStyle = ComboBoxStyle.DropDown;
            cBox_CustomerID.AutoCompleteMode = AutoCompleteMode.Suggest;
            cBox_CustomerID.AutoCompleteSource = AutoCompleteSource.ListItems;

            DataSet ds = new DataSet();
            string connectionString = @"Data Source=POLINA11-30\SQLEXPRESS;Initial Catalog=NORTHWIND;Integrated Security=True";
           // string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=NORTHWIND.MDF;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("select * from[dbo].[Customers];", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds, "CUSTOMERS");
                }
            }

            //bindings:
            cBox_CustomerID.DataSource = ds.Tables["CUSTOMERS"];
            cBox_CustomerID.DisplayMember = "CustomerID";
          //  cBox_CustomerID.ValueMember = "CustomerID";

           
            txt_Address.DataBindings.Add(new Binding("Text", ds.Tables["CUSTOMERS"], "Address"));
            txt_CompanyName.DataBindings.Add(new Binding("Text", ds.Tables["CUSTOMERS"], "CompanyName"));
            txt_ContactTitle.DataBindings.Add(new Binding("Text", ds.Tables["CUSTOMERS"], "ContactTitle"));
        }
    }
}
