﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;//для работы с файлом App.config - из System.configuration.dll
using System.Data.SqlClient;// из драйвера MS SQL Server-а
using System.Data.SqlTypes;//System.Data.dll
using MSServerTestConnection;

namespace ComboBoxTester
{
    public partial class MainForm : Form
    {
        List<Customer> customers = new List<Customer>();
        private BindingList<Customer> list;
        private BindingList<Player> players;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Внешний вид Выпадающего списка
            cboxCustomers.DropDownStyle = ComboBoxStyle.DropDown;
            cboxCustomers.AutoCompleteMode = AutoCompleteMode.Suggest;
            cboxCustomers.AutoCompleteSource = AutoCompleteSource.ListItems;
            #endregion
            #region Соединение с базой данных
            DataSet ds = new DataSet();
            string connectionString = @"Data Source=POLINA11-30\SQLEXPRESS;Initial Catalog=db28pr10;Integrated Security=True";
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from[dbo].[CUSTOMERS] as Customers ");
            sb.Append(" left join [dbo].[MEN] as Men ");
            sb.Append(" on Men.[MEN_ID] = Customers.[ID_MEN] ");
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(sb.ToString(), con))
                {
                     using (SqlDataReader reader = command.ExecuteReader())
                     {
                         while (reader.Read())
                         {
                            int ID;
                            Int32.TryParse(reader["CUSTOMERS_ID"].ToString(), out ID);
                            string nick = reader["NICKNAME"].ToString();//reader.GetString(1);
                            string phone = reader["PHONE"].ToString();//reader.GetString(2);
                            string email = reader["EMAIL"].ToString();//DateTime datevalue = reader.GetDateTime(2);
                            string lname = reader["LNAME"].ToString();
                            string name= reader["NAME"].ToString();
                            int year;
                            Int32.TryParse(reader["YEAR"].ToString(), out year);
                            customers.Add(new Customer(nick, phone, email, ID, name, lname, year));
                         }
                     }               
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds, "CUSTOMERS");
                }
                #endregion
            #region Привязки
                //first comboBox (without auto complete)
                cbox1.DataSource = ds.Tables["CUSTOMERS"];//источник данных для отображения - как правило DataTable
                cbox1.DisplayMember = "LNAME";//имя столбца, знач.кот.отобр.
                cbox1.ValueMember = "CUSTOMERS_ID";//имя id столбца для поиска строки, из кот.берем столбец

                //second comboBox (auto complete turned on)
                cboxCustomers.DataSource = ds.Tables["CUSTOMERS"];//источник данных для отображения - как правило DataTable
                cboxCustomers.DisplayMember = "NAME";//имя столбца, знач.кот.отобр.
                cboxCustomers.ValueMember = "CUSTOMERS_ID";//имя id столбца для поиска строки, из кот.берем столбец

                BindingSource bs = new BindingSource(ds, "CUSTOMERS");
                cboxCustomers.DataBindings.Add("SelectedValue", bs, "CUSTOMERS_ID");
                cboxCustomers.DataBindings.Add(new Binding("Text", bs, "NICKNAME"));
                bs.Position = 1;//выделели элемент в ComboBox-е программно

                list = new BindingList<Customer>();
                list.Add(customers[0]);
                list.Add(customers[1]);

                //third comboBox, data source - BindingList<Customer>
                cboxCustomerClass.DataSource = list;
                cboxCustomerClass.DisplayMember = "Phone";//Здесь это имя public свойства
                //textBox, binded with third and fourth comboBoxes:
                txtBoxCustomer.DataBindings.Add(new Binding("Text", list, "Phone"));

                players = new BindingList<Player>();
                players.Add(new Player("Иванов", 10));
                players.Add(new Player("Петров", 20));
                players.Add(new Player("Сидоров", 30));

                //fourth comboBox, data source - BindingList<Player>
                //class Player implements interface INotifyPropertyChanged
                //so, class props can be changed using interface, and changes will be immidiatly displayed
                cboxPlayers.DataSource = players;
                cboxPlayers.DisplayMember = "PlayerName";
                //textBox, binded with third and fourth comboBoxes:
                txtBoxPlayer.DataBindings.Add(new Binding("Text", players, "PlayerName"));
            }
                #endregion
           
        }
    }
}
