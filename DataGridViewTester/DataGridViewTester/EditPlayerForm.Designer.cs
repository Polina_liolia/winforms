﻿namespace DataGridViewTester
{
    partial class EditPlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Close = new System.Windows.Forms.Button();
            this.txt_PlayerName = new System.Windows.Forms.TextBox();
            this.lbl_PlayerAge = new System.Windows.Forms.Label();
            this.ch_Sex = new System.Windows.Forms.CheckBox();
            this.lbl_PlayerName = new System.Windows.Forms.Label();
            this.lbl_AgeTitle = new System.Windows.Forms.Label();
            this.lbl_Sex = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(220, 132);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 0;
            this.btn_Close.Text = "Закрыть";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // txt_PlayerName
            // 
            this.txt_PlayerName.Location = new System.Drawing.Point(13, 80);
            this.txt_PlayerName.Name = "txt_PlayerName";
            this.txt_PlayerName.Size = new System.Drawing.Size(100, 20);
            this.txt_PlayerName.TabIndex = 1;
            // 
            // lbl_PlayerAge
            // 
            this.lbl_PlayerAge.AutoSize = true;
            this.lbl_PlayerAge.Location = new System.Drawing.Point(145, 84);
            this.lbl_PlayerAge.Name = "lbl_PlayerAge";
            this.lbl_PlayerAge.Size = new System.Drawing.Size(26, 13);
            this.lbl_PlayerAge.TabIndex = 2;
            this.lbl_PlayerAge.Text = "Age";
            // 
            // ch_Sex
            // 
            this.ch_Sex.AutoSize = true;
            this.ch_Sex.Location = new System.Drawing.Point(215, 84);
            this.ch_Sex.Name = "ch_Sex";
            this.ch_Sex.Size = new System.Drawing.Size(80, 17);
            this.ch_Sex.TabIndex = 3;
            this.ch_Sex.Text = "checkBox1";
            this.ch_Sex.UseVisualStyleBackColor = true;
            // 
            // lbl_PlayerName
            // 
            this.lbl_PlayerName.AutoSize = true;
            this.lbl_PlayerName.Location = new System.Drawing.Point(43, 61);
            this.lbl_PlayerName.Name = "lbl_PlayerName";
            this.lbl_PlayerName.Size = new System.Drawing.Size(34, 13);
            this.lbl_PlayerName.TabIndex = 4;
            this.lbl_PlayerName.Text = "ФИО";
            // 
            // lbl_AgeTitle
            // 
            this.lbl_AgeTitle.AutoSize = true;
            this.lbl_AgeTitle.Location = new System.Drawing.Point(136, 61);
            this.lbl_AgeTitle.Name = "lbl_AgeTitle";
            this.lbl_AgeTitle.Size = new System.Drawing.Size(49, 13);
            this.lbl_AgeTitle.TabIndex = 5;
            this.lbl_AgeTitle.Text = "Возраст";
            // 
            // lbl_Sex
            // 
            this.lbl_Sex.AutoSize = true;
            this.lbl_Sex.Location = new System.Drawing.Point(235, 61);
            this.lbl_Sex.Name = "lbl_Sex";
            this.lbl_Sex.Size = new System.Drawing.Size(27, 13);
            this.lbl_Sex.TabIndex = 6;
            this.lbl_Sex.Text = "Пол";
            // 
            // EditPlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 173);
            this.Controls.Add(this.lbl_Sex);
            this.Controls.Add(this.lbl_AgeTitle);
            this.Controls.Add(this.lbl_PlayerName);
            this.Controls.Add(this.ch_Sex);
            this.Controls.Add(this.lbl_PlayerAge);
            this.Controls.Add(this.txt_PlayerName);
            this.Controls.Add(this.btn_Close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditPlayerForm";
            this.Text = "Изменение сведений об игроке";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.TextBox txt_PlayerName;
        private System.Windows.Forms.Label lbl_PlayerAge;
        private System.Windows.Forms.CheckBox ch_Sex;
        private System.Windows.Forms.Label lbl_PlayerName;
        private System.Windows.Forms.Label lbl_AgeTitle;
        private System.Windows.Forms.Label lbl_Sex;
    }
}