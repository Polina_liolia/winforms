﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewTester
{
    public partial class EditPlayerForm : Form
    {
        public EditPlayerForm()
        {
            InitializeComponent();
        }

        public EditPlayerForm(string tableName, DataSet ds, int rowIndex)
        {
            InitializeComponent();
            //настройка привязок (binding)
            BindingSource bs = new BindingSource(ds, tableName);
            bs.Position = rowIndex;
            txt_PlayerName.DataBindings.Add(new Binding("Text", bs, "PlayerName"));
            lbl_PlayerAge.DataBindings.Add(new Binding("Text", bs, "Age"));
            Binding bss = new Binding("Checked", bs, "Sex", true); //true - указывает на необходимость осуществлять преобразование
            //поле Sex - это 0 или 1
            //св-во checked - это ищщд
            //поэтому нужны преобразования из bool в string и наоборот:
            bss.Parse += new ConvertEventHandler(booleanValueToString);
            bss.Format += new ConvertEventHandler(strValueToBoolean);
            ch_Sex.DataBindings.Add(bss);
            ch_Sex.Text = "Woman";

           
        }

        private void strValueToBoolean(object sender, ConvertEventArgs e)
        {
            if (!(e.Value is DBNull))
            {
                if (((string)e.Value) == "1")
                {
                    e.Value = true;
                }
                else
                {
                    e.Value = false;
                }
            }
        }

        private void booleanValueToString(object sender, ConvertEventArgs e)
        {
            if (!(e.Value is DBNull))
            {
                if (((bool)e.Value) == true)
                {
                    e.Value = "Woman";
                }
                else
                {
                    e.Value = "Man";
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
