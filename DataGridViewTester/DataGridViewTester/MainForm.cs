﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewTester
{
    public partial class MainForm : Form
    {
        DataSet dataSet = new DataSet("MyDataSet"); //аналог базы данных в памяти
        public MainForm()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Создание БД
            createGame();
            createPlayer();
            createScore();
            #endregion

            #region Настройка внешнего вида DataGridView
            dgv.DataSource = dataSet;
            dgv.DataMember = "players";
            dgv.AllowUserToDeleteRows = false;
            dgv.AllowUserToAddRows = false;
            dgv.AllowUserToOrderColumns = true;
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv.ReadOnly = true;
            dgv.RowHeadersVisible = false;
            dgv.RowTemplate.Height = 24;
            dgv.RowTemplate.Resizable = DataGridViewTriState.False;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.Columns["Age"].HeaderText = "Возраст";
            dgv.Columns["Age"].Width = 55;
            dgv.Columns["Player_ID"].Visible = false;
            dgv.Columns["PlayerName"].HeaderText = "Имя игрока";
            dgv.Columns["PlayerName"].Width = 500;
            dgv.Columns["Sex"].HeaderText = "Пол";
            dgv.Columns["Sex"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.CellFormatting += dgv_CellFormatting;

            #endregion
        }

      

        private void createPlayer()
        {
            
            //хранит множество таблиц-объекты класса DataTable
            //некоторые таблицы - загружаются запросами select из БД, некоторые - программно
            DataTable table = new DataTable("players");
            table.Columns.AddRange(new DataColumn[] { new DataColumn("PlayerName"),
                new DataColumn("Age"),
                new DataColumn("Sex"),
                new DataColumn("Player_ID")
            });
            dataSet.Tables.Add(table);
            DataRow row = dataSet.Tables["players"].NewRow();
            row["Player_ID"] = "1";
            row["PlayerName"] = "Ivanov";
            row["Age"] = "10";
            row["Sex"] = "1";

            dataSet.Tables["players"].Rows.Add(row);
            dataSet.Tables["players"].AcceptChanges();//если dataSet связан с БД, в этот момент происходит отправка в БД
            //фактически - выполняется insert

            DataRow row2 = dataSet.Tables["players"].NewRow();
            row2["Player_ID"] = "2";
            row2["PlayerName"] = "Petrov";
            row2["Age"] = "20";
            row2["Sex"] = "1";

            dataSet.Tables["players"].Rows.Add(row2);
            dataSet.Tables["players"].AcceptChanges();
        }

        private void createGame()
        {
            DataTable table = new DataTable("games");
            table.Columns.AddRange(new DataColumn[] { new DataColumn("GameName"),
                new DataColumn("CountOfPlayers"),
                new DataColumn("Game_ID")
            });
            dataSet.Tables.Add(table);
            DataRow row = dataSet.Tables["games"].NewRow();
            row["GameName"] = "Game1";
            row["CountOfPlayers"] = "10";
            row["Game_ID"] = "1";

            dataSet.Tables["games"].Rows.Add(row);
            dataSet.Tables["games"].AcceptChanges();
        }

        private void createScore()
        {
            DataTable table = new DataTable("score");
            table.Columns.AddRange(new DataColumn[] { new DataColumn("Player"),
                new DataColumn("Game"),
                new DataColumn("Score")
            });
            dataSet.Tables.Add(table);
            DataRow row = dataSet.Tables["score"].NewRow();
            row["Player"] = "Ivanov";
            row["Game"] = "Football";
            row["Score"] = "5:1";

            dataSet.Tables["score"].Rows.Add(row);
            dataSet.Tables["score"].AcceptChanges();
        }

        private void dgv_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.Value.ToString() == "M")
            {
                e.Value = "1";
            }
            else if (e.Value.ToString() == "W")
            {
                e.Value = "0";
            }
        }
        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dGrV = (DataGridView)sender;
            string nameColumn = dGrV.Columns[e.ColumnIndex].Name;
            //цвет выбеделния ячейки

            if (dGrV.Rows[e.RowIndex].Cells[e.ColumnIndex] == dGrV.CurrentCell)
            {
                e.CellStyle.SelectionBackColor = Color.DarkSeaGreen;
            }

            if ((e.Value != null) && (nameColumn == "Sex") && (e.Value.ToString() == "1"))
            {
                e.Value = "M".ToUpper();

                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.ForeColor = Color.WhiteSmoke;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                e.CellStyle.Font = new Font("Times New Roman", 7, FontStyle.Bold);
            }
            if ((e.Value != null) && (nameColumn == "Sex") && (e.Value.ToString() == "0"))
            {
                e.Value = "W".ToUpper();

                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.ForeColor = Color.WhiteSmoke;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                e.CellStyle.Font = new Font("Times New Roman", 7, FontStyle.Bold);
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //int row_number = e.RowIndex;
            int row_number = -1;
            DataGridView DbGrid = sender as DataGridView;
            DataRow dr = null;
            if ((DbGrid.VirtualMode == false) && /*в режиме "заполнен полностью"*/
              (DbGrid.BindingContext != null))//если заполнен DataSource
                
            {
                if (DbGrid.BindingContext[DbGrid.DataSource, DbGrid.DataMember].Count > 0)
                {
                    DataRowView drv = (DataRowView)DbGrid.BindingContext[DbGrid.DataSource, DbGrid.DataMember].Current;
                    dr = drv.Row;
                    row_number = dr.Table.Rows.IndexOf(dr);//обязательно нахожу строку из Грида в DataSet
                    //из-за сортировки они могу не совпадать
                    EditPlayerForm editForm = new EditPlayerForm("players", dataSet, row_number);
                    editForm.ShowDialog();
                }
            }



            EditPlayerForm form = new EditPlayerForm("players", dataSet, row_number);
            form.ShowDialog();


        }
    }
}
