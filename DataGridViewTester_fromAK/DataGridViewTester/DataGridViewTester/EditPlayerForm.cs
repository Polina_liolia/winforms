﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewTester
{
    public partial class EditPlayerForm : Form
    {
        public EditPlayerForm()
        {
            InitializeComponent();
        }
        public EditPlayerForm(string tableName, DataSet ds, int rowIndex)
        {
            InitializeComponent();
            //настройка  привязок - binding-а
            BindingSource bs = new BindingSource(ds, tableName);
            /*
             аналог команд:
              dgv.DataSource = dataSet;
              dgv.DataMember = "players";//"games";//
            */
            txtPlayerName.DataBindings.Add(new Binding("Text", bs, "PlayerName"));
            lblPlayerAge.DataBindings.Add(new Binding("Text", bs, "Age"));
            //Установленный 4-й параметр в true означает, что на стадии установки
            //Binding-а необходимо выполнять форматирующие преобразования,
            //заданные по умолчанию.Форматирующие преобраз.по умолч. не совсем 
            //справляются. Например: не могут преобразов. "1" в true  и наоборот;
            //не могут сформировать значение DBNull.value для поля типа Date
            Binding bss = new Binding("Checked", bs, "Sex", true);
            //поле Sex - Это или целое 0 или целое 1
            //свойство Checked -это bool
            //поэтому нужны преобразования из bool в String и наоборот
            bss.Parse += new ConvertEventHandler(boolaenValueToString);
            bss.Format += new ConvertEventHandler(strValueToBoolean);
            chSex.DataBindings.Add(bss);
            chSex.Text = "Woman";

            bs.Position = rowIndex;

        }
        public void strValueToBoolean(object sender, ConvertEventArgs cevent)
        {
            if (!(cevent.Value is DBNull))
            {
                if (((string)cevent.Value) == "1")
                {
                    cevent.Value = true;
                }
                else
                {
                    cevent.Value = false;
                }
            }
            else
            {
                cevent.Value = false;
            }
        }
        public void boolaenValueToString(object sender, ConvertEventArgs cevent)
        {
            if (!(cevent.Value is DBNull))
            {
                if (((bool)cevent.Value) == true)
                {
                    cevent.Value = "1";
                }
                else
                {
                    cevent.Value = "0";
                }
            }
            else
            {
                cevent.Value = "0";
            }

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
