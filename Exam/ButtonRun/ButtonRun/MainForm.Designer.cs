﻿namespace ButtonRun
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_running = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_running
            // 
            this.btn_running.Location = new System.Drawing.Point(303, 214);
            this.btn_running.Name = "btn_running";
            this.btn_running.Size = new System.Drawing.Size(119, 66);
            this.btn_running.TabIndex = 0;
            this.btn_running.Text = "Catch me!";
            this.btn_running.UseVisualStyleBackColor = true;
            this.btn_running.Click += new System.EventHandler(this.btn_running_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 534);
            this.Controls.Add(this.btn_running);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Catch the button!";
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_running;
    }
}

