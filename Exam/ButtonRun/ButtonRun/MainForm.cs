﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ButtonRun
{
    public partial class MainForm : Form
    {
        private Rectangle form_rect;
       
        public MainForm()
        {
            InitializeComponent();
            form_rect = this.ClientRectangle;
            
        }

        private void btn_running_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You've done it!", "Catched!", MessageBoxButtons.OK);
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            Point topLeft = btn_running.Location;
            Point topRight = new Point(topLeft.X + btn_running.Width, topLeft.Y);
            Point bottomLeft = new Point(topLeft.X, topLeft.Y + btn_running.Height);
            Point bottomRight = new Point(topLeft.X + btn_running.Width, topLeft.Y + btn_running.Height);
            Point topCenter = new Point(topLeft.X + btn_running.Width / 2, topLeft.Y);
            Point bottomCenter = new Point(topLeft.X + btn_running.Width / 2, topLeft.Y + btn_running.Height);
            Point rightCenter = new Point(topLeft.X + btn_running.Width, topLeft.Y + btn_running.Height / 2);
            Point leftCenter = new Point(topLeft.X, topLeft.Y + btn_running.Height / 2);

            if (mouseOnTheButton(e.Location))
            {
                if (noWay_Left())
                    move_right(true);
                else
                    move_left();

                if (noWay_Down())
                    move_up(true);
                else
                    move_down();
            }
            if (mouseNearPoint(e.Location, topCenter))
            {
                if (noWay_Down())
                    move_up(true); // big move in back direction 
                else
                    move_down();
            }
            else if (mouseNearPoint(e.Location, bottomCenter))
            {
                if (noWay_Up())
                    move_down(true);
                else
                    move_up();
            }
            else if (mouseNearPoint(e.Location, rightCenter))
            {
                if (noWay_Left())
                    move_right(true);
                else
                    move_left();
            }
            else if (mouseNearPoint(e.Location, leftCenter))
            {
                if (noWay_Right())
                    move_left(true);
                else
                    move_right();
            }
            else if (mouseNearPoint(e.Location, topLeft))
            {
                if (noWay_Right())
                    move_left(true);
                else
                    move_right();

                if (noWay_Down())
                    move_up(true); 
                else
                    move_down();
            }
            else if (mouseNearPoint(e.Location, topRight))
            {
                if (noWay_Left())
                    move_right(true);
                else
                    move_left();

                if (noWay_Down())
                    move_up(true);
                else
                    move_down();
            }
            else if (mouseNearPoint(e.Location, bottomLeft))
            {
                if (noWay_Right())
                    move_left(true);
                else
                    move_right();

                if (noWay_Up())
                    move_down(true);
                else
                    move_up();
            }
            else if (mouseNearPoint(e.Location, bottomRight))
            {
                if (noWay_Left())
                    move_right(true);
                else
                    move_left();

                if (noWay_Up())
                    move_down(true);
                else
                    move_up();
            }
        }

        private double calcDistanceBetweenPoints(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        private bool mouseNearPoint(Point mouse_location, Point btn_point)
        {
            return calcDistanceBetweenPoints(mouse_location, btn_point) < 20;
        }

        private bool mouseOnTheButton(Point mouse_location)
        {
            return mouse_location.X >= btn_running.Location.X &&
                mouse_location.X <= btn_running.Location.X + btn_running.Width &&
                mouse_location.Y >= btn_running.Location.Y &&
                mouse_location.Y <= btn_running.Location.Y + btn_running.Height;
        }

        private void move_right(bool run_away = false)
        {
            int margin = !run_away ? 20 : 20 + btn_running.Width;
            btn_running.Left += margin;
        }

        private void move_left(bool run_away = false)
        {
            int margin = !run_away ? 20 : 20 + btn_running.Width;
            btn_running.Left -= margin;
        }

        private void move_up(bool run_away = false)
        {
            int margin = !run_away ? 20 : 20 + btn_running.Height;
            btn_running.Top -= margin;
        }

        private void move_down(bool run_away = false)
        {
            int margin = !run_away ? 20 : 20 + btn_running.Height;
            btn_running.Top += margin;
        }

        private bool noWay_Left()
        {
            return btn_running.Location.X < 20;
        }
        private bool noWay_Right()
        {
            return btn_running.Location.X + btn_running.Width + 20 > this.ClientSize.Width;
        }
        private bool noWay_Up()
        {
            return btn_running.Location.Y < 20;
        }
        private bool noWay_Down()
        {
            return btn_running.Location.Y + btn_running.Height + 20 > this.ClientSize.Height;
        }
    }
}
