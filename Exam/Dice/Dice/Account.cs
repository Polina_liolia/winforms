﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateEventConsoleTest
{      
    public class Account : INotifyPropertyChanged
    {
        private double sum;

        public double Sum
        {
            get
            {
                return sum;
            }

            set
            {
                sum = value;
                NotifyPropertyChanged("Sum");
            }
        }

        public Account(double sum)
        {
            Sum = sum;
        }

        //для уведомления пользователя о событии, касающемся состояния его счета:
        public delegate void AccountStateHandler(string message); //указатель на ф-цию

        //событие, возникающее при снятии денег со счета:
        public event AccountStateHandler Withdrowed;

        //событие, возникающее при зачислении денег на счет:
        public event AccountStateHandler Added;

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        public void Put (double sum)
        {
            Sum += sum;
            NotifyPropertyChanged("Sum");
            if (Added != null)
                Added(string.Format("На счет поступило {0}, баланс: {1}", sum, Sum)); //генерация события
        }

        public void Withdraw (double sum)
        {
            if (sum <= Sum)
            {
                Sum -= sum;
                NotifyPropertyChanged("Sum");
                if (Withdrowed != null)
                    Withdrowed(string.Format("Со счета списано {0}, баланс: {1}", sum, Sum)); //генерация события
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed("На счету недостаточно средств для списания"); //генерация события
            }
        }      
    } 
}
