﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Dice
{
    internal class ListViewItemComparer : IComparer
    {
        private int index;

        public ListViewItemComparer(int key)
        {
            this.index = key;
        }

        public int Compare(object x, object y)
        {
            ListViewItem lvi1;
            ListViewItem lvi2;
            if (x is ListViewItem)
            {
                lvi1 = x as ListViewItem;
                if (lvi1 == null)
                    throw new ArgumentException("First argument is not a ListViewItem instance.");
            }
            else throw new ArgumentException("First argument is not a ListViewItem instance.");
            if (y is ListViewItem)
            {
                lvi2 = y as ListViewItem;
                if (lvi2 == null)
                    throw new ArgumentException("Second argument is not a ListViewItem instance.");
            }
            else throw new ArgumentException("Second argument is not a ListViewItem instance.");
            double account1, account2;
            bool parsed1 = Double.TryParse(lvi1.SubItems[index].Text, out account1);
            bool parsed2 = Double.TryParse(lvi2.SubItems[index].Text, out account2);
            if (!parsed1 || !parsed2)
                throw new FormatException("Account data is not valid.");
            return -1 * account1.CompareTo(account2);
        }
    }
}