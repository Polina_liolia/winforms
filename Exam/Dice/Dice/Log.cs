﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice
{
    public class Log
    {
        private static int counter = 1;
        public int RowNumber { get; }
        public DateTime Date_time { get; }
        public playersAction Action { get; }
        public string Name_client { get; }
        public double Account_sum { get; }
        public Log(playersAction action, string name_client, double account_sum)
        {
            RowNumber = counter++;
            Date_time = DateTime.Now;
            Action = action;
            Name_client = name_client;
            Account_sum = account_sum;
        }
        public Log (playersAction action, string name_client, double account_sum, DateTime dt)
        {
            RowNumber = counter++;
            Date_time = dt;
            Action = action;
            Name_client = name_client;
            Account_sum = account_sum;
        }

        public Log(int row_number, playersAction action, string name_client, double account_sum, DateTime dt)
        {
            RowNumber = row_number;
            Date_time = dt;
            Action = action;
            Name_client = name_client;
            Account_sum = account_sum;
        }
    }
}
