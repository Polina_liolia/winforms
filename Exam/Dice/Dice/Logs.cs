﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Dice
{
    public class Logs : IList<Log>
    {
        private List<Log> logs;
        public Logs()
        {
            logs = new List<Log>();
        }

        #region IList<Log> 
        public Log this[int index]
        {
            get
            {
                return logs[index];
            }
            set { } //readonly
        }

        public int Count
        {
            get
            {
                return logs.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public void Add(Log item)
        {
            logs.Add(item);
        }

        public void Add(playersAction action, string name_client, double account_sum)
        {
            Log item = new Log(action, name_client, account_sum);
            logs.Add(item);
        }

        public void Clear()
        {
            logs.Clear();
        }

        public bool Contains(Log item)
        {
            return logs.Contains(item);
        }

        public void CopyTo(Log[] array, int arrayIndex)
        {
            logs.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Log> GetEnumerator()
        {
           return  logs.GetEnumerator();
        }

        public int IndexOf(Log item)
        {
            return logs.IndexOf(item);
        }

        public void Insert(int index, Log item)
        {
            logs.Insert(index, item);
        }

        public bool Remove(Log item)
        {
            return false;
        }

        public void RemoveAt(int index)
        {
            
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return logs.GetEnumerator();
        }
        #endregion

        #region XML reader and writer
        public void readXML(string path)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            if (xDoc.ChildNodes.Count == 0)
                return;// xml file is empty, nothing to read
            using (XmlReader reader = XmlReader.Create(path))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element) //if it is an element node
                    {
                        if (reader.Name == "log_row")
                        {
                            DateTime dateTime;
                            bool dateParsed = DateTime.TryParse(reader.GetAttribute("date_time"), out dateTime);
                            if (!dateParsed)
                                continue; //unable to read this element date info

                            string action_str = reader.GetAttribute("action");
                            /* public enum playersAction{ INIT, WIN, LOSE};*/
                            playersAction players_action = action_str == "INIT" ? playersAction.INIT :
                                action_str == "WIN" ? playersAction.WIN :
                                playersAction.LOSE;
                            string name = reader.GetAttribute("name_client");
                            string result = reader.ReadElementContentAsString();
                            Regex accountRegex = new Regex(@"\d+");
                            Match m = accountRegex.Match(result);
                            string accountStr = m.Value;
                            double account;
                            bool accountParseResult = Double.TryParse(accountStr, out account);
                            if (!accountParseResult)
                                continue;//unable to read this element date info
                            logs.Add(new Log(players_action, name, account, dateTime));
                        }
                    }
                }
            }
        }

        public void writeXML(string path)
        {
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteRaw("\n");
                writer.WriteStartElement("log"); //корневой эл-т
                writer.WriteRaw("\n");

                foreach(Log l in logs)
                {
                    writer.WriteStartElement("log_row"); //эл-т, вложенный в корневой эл-т
                    writer.WriteAttributeString("rownum", l.RowNumber.ToString()); //атрибут 
                    writer.WriteAttributeString("date_time", l.Date_time.ToString()); //атрибут
                    writer.WriteAttributeString("action", l.Action.ToString("G")); //атрибут
                    writer.WriteAttributeString("name_client", l.Name_client); //атрибут
                    writer.WriteString(string.Format("Account ${0}", l.Account_sum));
                    writer.WriteEndElement();
                    writer.WriteRaw("\n");
                }
                writer.WriteEndDocument(); //закрыли корневой эл-т, закончили писать док-т
            }
        }
        #endregion
    }
}
