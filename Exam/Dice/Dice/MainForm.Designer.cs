﻿namespace Dice
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_selectPlayers = new System.Windows.Forms.Button();
            this.btn_newPlayer = new System.Windows.Forms.Button();
            this.cBox_firstPlayer = new System.Windows.Forms.ComboBox();
            this.cBox_secondPlayer = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(16, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Players:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Second player";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "First player";
            // 
            // btn_selectPlayers
            // 
            this.btn_selectPlayers.Location = new System.Drawing.Point(241, 117);
            this.btn_selectPlayers.Name = "btn_selectPlayers";
            this.btn_selectPlayers.Size = new System.Drawing.Size(75, 23);
            this.btn_selectPlayers.TabIndex = 12;
            this.btn_selectPlayers.Text = "OK";
            this.btn_selectPlayers.UseVisualStyleBackColor = true;
            this.btn_selectPlayers.Click += new System.EventHandler(this.btn_selectPlayers_Click);
            // 
            // btn_newPlayer
            // 
            this.btn_newPlayer.Location = new System.Drawing.Point(35, 116);
            this.btn_newPlayer.Name = "btn_newPlayer";
            this.btn_newPlayer.Size = new System.Drawing.Size(75, 23);
            this.btn_newPlayer.TabIndex = 13;
            this.btn_newPlayer.Text = "New player";
            this.btn_newPlayer.UseVisualStyleBackColor = true;
            this.btn_newPlayer.Click += new System.EventHandler(this.btn_newPlayer_Click);
            // 
            // cBox_firstPlayer
            // 
            this.cBox_firstPlayer.FormattingEnabled = true;
            this.cBox_firstPlayer.Location = new System.Drawing.Point(143, 42);
            this.cBox_firstPlayer.Name = "cBox_firstPlayer";
            this.cBox_firstPlayer.Size = new System.Drawing.Size(192, 21);
            this.cBox_firstPlayer.TabIndex = 14;
            // 
            // cBox_secondPlayer
            // 
            this.cBox_secondPlayer.FormattingEnabled = true;
            this.cBox_secondPlayer.Location = new System.Drawing.Point(143, 71);
            this.cBox_secondPlayer.Name = "cBox_secondPlayer";
            this.cBox_secondPlayer.Size = new System.Drawing.Size(192, 21);
            this.cBox_secondPlayer.TabIndex = 15;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 157);
            this.Controls.Add(this.cBox_secondPlayer);
            this.Controls.Add(this.cBox_firstPlayer);
            this.Controls.Add(this.btn_newPlayer);
            this.Controls.Add(this.btn_selectPlayers);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Authorization";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_selectPlayers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_newPlayer;
        private System.Windows.Forms.ComboBox cBox_firstPlayer;
        private System.Windows.Forms.ComboBox cBox_secondPlayer;
    }
}

