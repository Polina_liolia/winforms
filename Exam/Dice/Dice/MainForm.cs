﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace Dice
{
    public partial class MainForm : Form
    {
        
        private BindingList<Player> players_firs = new BindingList<Player>();
        private BindingList<Player> players_second = new BindingList<Player>();
        private Player player1 = null;
        private Player player2 = null;
        private PlayForm playForm;
        private Logs logs = new Logs();

        public MainForm()
        {
            InitializeComponent();
            logs.readXML(@"..\..\players.xml");
            populatePlayersListsFromLogs();
            player1 = players_firs.Count > 0 ? players_firs[0] : null;
            player2 = players_second.Count > 1 ? players_second[1] : 
                players_second.Count > 0 ? players_second[0] : null;
            #region Players' comboBoxes settings 
            cBox_firstPlayer.DataSource = players_firs;
            cBox_firstPlayer.ValueMember = "name";
            cBox_firstPlayer.SelectedIndex = 0;
            cBox_secondPlayer.DataSource = players_second;
            cBox_secondPlayer.ValueMember = "name";
            cBox_secondPlayer.SelectedIndex = 1;

            cBox_setSettings(cBox_firstPlayer);
            cBox_setSettings(cBox_secondPlayer);
            #endregion
        }

        private void cBox_setSettings(ComboBox cBox)
        {
            cBox.DropDownStyle = ComboBoxStyle.DropDown;
            cBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            cBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            cBox.Format += CBox_Players_Format;
            cBox.SelectedIndexChanged += CBox_SelectedIndexChanged;
        }

        private void CBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                ComboBox cBox = sender as ComboBox;
                if (cBox != null)
                {
                    string playersNameChoosen = cBox.SelectedValue.ToString();
                    Player current_player = players_firs.SingleOrDefault<Player>(p => p.Name == playersNameChoosen);
                    if (cBox == cBox_firstPlayer)
                    {
                        player1 = current_player;
                    } 
                    else if (cBox == cBox_secondPlayer)
                    {
                        player2 = current_player;
                    }
                }
            }
        }

        //to show player's name and his account sum together in the combo box
        private void CBox_Players_Format(object sender, ListControlConvertEventArgs e)
        {
            if (e.ListItem is Player)
            {
                Player player = e.ListItem as Player;
                if (player != null)
                {
                    e.Value = string.Format("{0} (${1})", player.Name, player.AccountSum);
                }
            }
        }

        //reads players' data from file 
        private void populatePlayersListsFromLogs()
        {
            foreach(Log l in logs)
            {
                //looking for such a player in the Binding list:
                Player playerFromList = players_firs.FirstOrDefault(p => p.Name == l.Name_client);
                Player playerFromLog = new Player(l.Name_client, l.Account_sum, l.Action, l.Date_time);
                //if such player's data already in list:
                if (playerFromList != null) 
                {
                    if (playerFromList.ActionStatusChanged > l.Date_time) //actual data is in list
                        continue;
                    else //actual data is in logs
                    {
                        int index = players_firs.IndexOf(playerFromList);
                        if (index >= 0)
                        {
                            players_firs[index] = playerFromLog;
                            players_second[index] = playerFromLog;
                        }
                    }
                }
                else //new player
                {
                    players_firs.Add(playerFromLog);
                    players_second.Add(playerFromLog);
                }
            }                  
        }

        private void btn_selectPlayers_Click(object sender, EventArgs e)
        {
            if (player1 == player2 || player1 == null || player2 == null) //if the same players were selected
            {
                MessageBox.Show("Choose two different players.",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (player1.AccountSum <= 0) //if the first playe has no money to make bill
            {
                MessageBox.Show(string.Format("{0} has no money to play.", player1.Name),
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (player2.AccountSum <= 0) //if the first playe has no money to make bill
            {
                MessageBox.Show(string.Format("{0} has no money to play.", player2.Name),
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                playForm = new PlayForm(ref players_firs, ref player1, ref player2, ref logs);
                this.Hide();
                playForm.ShowDialog();
                this.Show();
            }
        }

        private void btn_newPlayer_Click(object sender, EventArgs e)
        {
            Player newPlayer = new Player("", 200);
            NewPlayerForm np_form = new NewPlayerForm(ref newPlayer, players_firs);
            np_form.ShowDialog();
            if (newPlayer.Name != "")
            {
                players_firs.Add(newPlayer);
                players_second.Add(newPlayer);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            logs.writeXML(@"..\..\players.xml");
        }
    }
}
