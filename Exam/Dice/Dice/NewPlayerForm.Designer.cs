﻿namespace Dice
{
    partial class NewPlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_newPlayerName = new System.Windows.Forms.TextBox();
            this.btn_AddPlayer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txt_newPlayerName
            // 
            this.txt_newPlayerName.Location = new System.Drawing.Point(64, 27);
            this.txt_newPlayerName.Name = "txt_newPlayerName";
            this.txt_newPlayerName.Size = new System.Drawing.Size(174, 20);
            this.txt_newPlayerName.TabIndex = 1;
            // 
            // btn_AddPlayer
            // 
            this.btn_AddPlayer.Location = new System.Drawing.Point(64, 74);
            this.btn_AddPlayer.Name = "btn_AddPlayer";
            this.btn_AddPlayer.Size = new System.Drawing.Size(75, 23);
            this.btn_AddPlayer.TabIndex = 2;
            this.btn_AddPlayer.Text = "Add player";
            this.btn_AddPlayer.UseVisualStyleBackColor = true;
            this.btn_AddPlayer.Click += new System.EventHandler(this.btn_AddPlayer_Click);
            // 
            // NewPlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 122);
            this.Controls.Add(this.btn_AddPlayer);
            this.Controls.Add(this.txt_newPlayerName);
            this.Controls.Add(this.label1);
            this.Name = "NewPlayerForm";
            this.Text = "NewPlayerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_newPlayerName;
        private System.Windows.Forms.Button btn_AddPlayer;
    }
}