﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice
{
    public partial class NewPlayerForm : Form
    {
        private BindingList<Player> all_players;
        private Player newPlayer;
        public NewPlayerForm(ref Player newPlayer, BindingList<Player> all_players)
        {
            InitializeComponent();
            this.all_players = all_players;
            this.newPlayer = newPlayer;
        }

        private void btn_AddPlayer_Click(object sender, EventArgs e)
        {
            if (txt_newPlayerName.Text == "")
                MessageBox.Show("Input new player's name!", "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else if (all_players.FirstOrDefault(p=>p.Name == txt_newPlayerName.Text) != null)
            {
                MessageBox.Show("Such player already exists.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                newPlayer.Name = txt_newPlayerName.Text;
                this.Close();
            }
        }
    }
}
