﻿namespace Dice
{
    partial class PlayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.gBox_player1 = new System.Windows.Forms.GroupBox();
            this.lbl_playerRate1 = new System.Windows.Forms.Label();
            this.lbl_playerAccount1 = new System.Windows.Forms.Label();
            this.lbl_playerName1 = new System.Windows.Forms.Label();
            this.txt_playerRate1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gBox_player2 = new System.Windows.Forms.GroupBox();
            this.lbl_playerRate2 = new System.Windows.Forms.Label();
            this.lbl_playerAccount2 = new System.Windows.Forms.Label();
            this.lbl_playerName2 = new System.Windows.Forms.Label();
            this.txt_playerRate2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btn_ThrowDice = new System.Windows.Forms.Button();
            this.btn_bestScores = new System.Windows.Forms.Button();
            this.pBox_dice1 = new System.Windows.Forms.PictureBox();
            this.pBox_dice2 = new System.Windows.Forms.PictureBox();
            this.gBox_player1.SuspendLayout();
            this.gBox_player2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_dice1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_dice2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Игрок: ";
            // 
            // gBox_player1
            // 
            this.gBox_player1.Controls.Add(this.lbl_playerRate1);
            this.gBox_player1.Controls.Add(this.lbl_playerAccount1);
            this.gBox_player1.Controls.Add(this.lbl_playerName1);
            this.gBox_player1.Controls.Add(this.txt_playerRate1);
            this.gBox_player1.Controls.Add(this.label4);
            this.gBox_player1.Controls.Add(this.label3);
            this.gBox_player1.Controls.Add(this.label2);
            this.gBox_player1.Controls.Add(this.label1);
            this.gBox_player1.Location = new System.Drawing.Point(12, 22);
            this.gBox_player1.Name = "gBox_player1";
            this.gBox_player1.Size = new System.Drawing.Size(240, 159);
            this.gBox_player1.TabIndex = 1;
            this.gBox_player1.TabStop = false;
            this.gBox_player1.Text = "Player1";
            // 
            // lbl_playerRate1
            // 
            this.lbl_playerRate1.AutoSize = true;
            this.lbl_playerRate1.Location = new System.Drawing.Point(117, 78);
            this.lbl_playerRate1.Name = "lbl_playerRate1";
            this.lbl_playerRate1.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerRate1.TabIndex = 7;
            // 
            // lbl_playerAccount1
            // 
            this.lbl_playerAccount1.AutoSize = true;
            this.lbl_playerAccount1.Location = new System.Drawing.Point(117, 50);
            this.lbl_playerAccount1.Name = "lbl_playerAccount1";
            this.lbl_playerAccount1.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerAccount1.TabIndex = 6;
            // 
            // lbl_playerName1
            // 
            this.lbl_playerName1.AutoSize = true;
            this.lbl_playerName1.Location = new System.Drawing.Point(117, 26);
            this.lbl_playerName1.Name = "lbl_playerName1";
            this.lbl_playerName1.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerName1.TabIndex = 5;
            // 
            // txt_playerRate1
            // 
            this.txt_playerRate1.Location = new System.Drawing.Point(111, 101);
            this.txt_playerRate1.Name = "txt_playerRate1";
            this.txt_playerRate1.Size = new System.Drawing.Size(100, 20);
            this.txt_playerRate1.TabIndex = 4;
            this.txt_playerRate1.TextChanged += new System.EventHandler(this.rate_changed);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ставка:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Размер ставки, $:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Счет:";
            // 
            // gBox_player2
            // 
            this.gBox_player2.Controls.Add(this.lbl_playerRate2);
            this.gBox_player2.Controls.Add(this.lbl_playerAccount2);
            this.gBox_player2.Controls.Add(this.lbl_playerName2);
            this.gBox_player2.Controls.Add(this.txt_playerRate2);
            this.gBox_player2.Controls.Add(this.label8);
            this.gBox_player2.Controls.Add(this.label12);
            this.gBox_player2.Controls.Add(this.label13);
            this.gBox_player2.Controls.Add(this.label14);
            this.gBox_player2.Location = new System.Drawing.Point(299, 22);
            this.gBox_player2.Name = "gBox_player2";
            this.gBox_player2.Size = new System.Drawing.Size(234, 159);
            this.gBox_player2.TabIndex = 2;
            this.gBox_player2.TabStop = false;
            this.gBox_player2.Text = "Player2";
            // 
            // lbl_playerRate2
            // 
            this.lbl_playerRate2.AutoSize = true;
            this.lbl_playerRate2.Location = new System.Drawing.Point(126, 84);
            this.lbl_playerRate2.Name = "lbl_playerRate2";
            this.lbl_playerRate2.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerRate2.TabIndex = 15;
            // 
            // lbl_playerAccount2
            // 
            this.lbl_playerAccount2.AutoSize = true;
            this.lbl_playerAccount2.Location = new System.Drawing.Point(126, 56);
            this.lbl_playerAccount2.Name = "lbl_playerAccount2";
            this.lbl_playerAccount2.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerAccount2.TabIndex = 14;
            // 
            // lbl_playerName2
            // 
            this.lbl_playerName2.AutoSize = true;
            this.lbl_playerName2.Location = new System.Drawing.Point(126, 32);
            this.lbl_playerName2.Name = "lbl_playerName2";
            this.lbl_playerName2.Size = new System.Drawing.Size(0, 13);
            this.lbl_playerName2.TabIndex = 13;
            // 
            // txt_playerRate2
            // 
            this.txt_playerRate2.Location = new System.Drawing.Point(120, 107);
            this.txt_playerRate2.Name = "txt_playerRate2";
            this.txt_playerRate2.Size = new System.Drawing.Size(100, 20);
            this.txt_playerRate2.TabIndex = 12;
            this.txt_playerRate2.TextChanged += new System.EventHandler(this.rate_changed);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Ставка:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Размер ставки, $:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Счет:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Игрок: ";
            // 
            // btn_ThrowDice
            // 
            this.btn_ThrowDice.Location = new System.Drawing.Point(366, 201);
            this.btn_ThrowDice.Name = "btn_ThrowDice";
            this.btn_ThrowDice.Size = new System.Drawing.Size(167, 23);
            this.btn_ThrowDice.TabIndex = 3;
            this.btn_ThrowDice.Text = "Бросить!";
            this.btn_ThrowDice.UseVisualStyleBackColor = true;
            this.btn_ThrowDice.Click += new System.EventHandler(this.btn_ThrowDice_Click);
            // 
            // btn_bestScores
            // 
            this.btn_bestScores.Location = new System.Drawing.Point(366, 230);
            this.btn_bestScores.Name = "btn_bestScores";
            this.btn_bestScores.Size = new System.Drawing.Size(167, 23);
            this.btn_bestScores.TabIndex = 4;
            this.btn_bestScores.Text = "Рекорды";
            this.btn_bestScores.UseVisualStyleBackColor = true;
            this.btn_bestScores.Click += new System.EventHandler(this.btn_bestScores_Click);
            // 
            // pBox_dice1
            // 
            this.pBox_dice1.Location = new System.Drawing.Point(75, 187);
            this.pBox_dice1.Name = "pBox_dice1";
            this.pBox_dice1.Size = new System.Drawing.Size(70, 70);
            this.pBox_dice1.TabIndex = 5;
            this.pBox_dice1.TabStop = false;
            // 
            // pBox_dice2
            // 
            this.pBox_dice2.Location = new System.Drawing.Point(173, 187);
            this.pBox_dice2.Name = "pBox_dice2";
            this.pBox_dice2.Size = new System.Drawing.Size(70, 70);
            this.pBox_dice2.TabIndex = 6;
            this.pBox_dice2.TabStop = false;
            // 
            // PlayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 261);
            this.Controls.Add(this.pBox_dice2);
            this.Controls.Add(this.pBox_dice1);
            this.Controls.Add(this.btn_bestScores);
            this.Controls.Add(this.btn_ThrowDice);
            this.Controls.Add(this.gBox_player2);
            this.Controls.Add(this.gBox_player1);
            this.Name = "PlayForm";
            this.Text = "Play dice";
            this.Load += new System.EventHandler(this.PlayForm_Load);
            this.gBox_player1.ResumeLayout(false);
            this.gBox_player1.PerformLayout();
            this.gBox_player2.ResumeLayout(false);
            this.gBox_player2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_dice1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_dice2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gBox_player1;
        private System.Windows.Forms.Label lbl_playerRate1;
        private System.Windows.Forms.Label lbl_playerAccount1;
        private System.Windows.Forms.Label lbl_playerName1;
        private System.Windows.Forms.TextBox txt_playerRate1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gBox_player2;
        private System.Windows.Forms.Label lbl_playerRate2;
        private System.Windows.Forms.Label lbl_playerAccount2;
        private System.Windows.Forms.Label lbl_playerName2;
        private System.Windows.Forms.TextBox txt_playerRate2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btn_ThrowDice;
        private System.Windows.Forms.Button btn_bestScores;
        private System.Windows.Forms.PictureBox pBox_dice1;
        private System.Windows.Forms.PictureBox pBox_dice2;
    }
}