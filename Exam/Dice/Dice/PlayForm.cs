﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice
{
    public partial class PlayForm : Form
    {
        private BindingList<Player> players;
        private Player player1;
        private Player player2;
        private int rate1;
        private int rate2;
        private Logs logs;
        private string[] imgs = {
            "../../dice_img/dice1.png",
            "../../dice_img/dice2.png",
            "../../dice_img/dice3.png",
            "../../dice_img/dice4.png",
            "../../dice_img/dice5.png",
            "../../dice_img/dice6.png"};

        public PlayForm(ref BindingList<Player> players, ref Player player1, ref Player player2, ref Logs logs)
        {
            InitializeComponent();
            this.players = players;
            this.player1 = player1;
            this.player2 = player2;
            this.logs = logs;
        }

        private void PlayForm_Load(object sender, EventArgs e)
        {
            lbl_playerName1.Text = player1.Name;
            lbl_playerName2.Text = player2.Name;
            lbl_playerAccount1.DataBindings.Add(new Binding("Text", player1, "AccountSum"));
            lbl_playerAccount2.DataBindings.Add(new Binding("Text", player2, "AccountSum"));
            lbl_playerRate1.Text = "10";
            lbl_playerRate2.Text = "10";
        }

        //checking value inputed
        private void rate_changed(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txt = sender as TextBox;
                if (txt != null)
                { 
                    int rate;
                    bool parsed = Int32.TryParse(txt.Text, out rate);
                    if (!parsed || rate > 12 || rate <= 1)
                    {
                        txt.ForeColor = Color.Red;
                    }
                    else
                    {
                        txt.ForeColor = Color.Black;
                    }
                }
            }
        }
        
        //checks, if both inputed rates are valid
        private bool rates_correct()
        {
            int rate1;
            bool parsed1 = Int32.TryParse(txt_playerRate1.Text, out rate1);
            int rate2;
            bool parsed2 = Int32.TryParse(txt_playerRate2.Text, out rate2);
            if (!parsed1 || !parsed2 || rate1 > 12 || rate1 <= 1 || rate2 > 12 || rate2 <= 1)
                return false;
            this.rate1 = rate1;
            this.rate2 = rate2;
            return true;
        }

        //checks if player has enough money to play
        private bool check_players_account(Player player)
        {
            return player.AccountSum >= 10 ? true : false;
        }

        private void btn_ThrowDice_Click(object sender, EventArgs e)
        {
            if (rates_correct())
            {
                if (!check_players_account(player1))
                {
                    MessageBox.Show(string.Format("{0} has no money to play.", player1.Name), "Game over",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                if (!check_players_account(player2))
                {
                    MessageBox.Show(string.Format("{0} has no money to play.", player2.Name), "Game over",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                Random rand = new Random();
                int ivorie1 = rand.Next(1, 7);
                int ivorie2 = rand.Next(1, 7);
                int sum = ivorie1 + ivorie2;
                pBox_dice1.Image = Image.FromFile(imgs[ivorie1-1]);
                pBox_dice2.Image = Image.FromFile(imgs[ivorie2-1]);
                StringBuilder game_result_msg = new StringBuilder();
                if (sum == rate1)
                {
                    game_result_msg.Append(string.Format("{0} wins! ", player1.Name));
                    player1.Account.Put(10);
                    logs.Add(playersAction.WIN, player1.Name, player1.AccountSum);
                }
                else
                {
                    game_result_msg.Append(string.Format("{0} lose! ", player1.Name));
                    player1.Account.Withdraw(10);
                    logs.Add(playersAction.LOSE, player1.Name, player1.AccountSum);
                }
                if (sum == rate2)
                {
                    game_result_msg.Append(string.Format("{0} wins! ", player2.Name));
                    player2.Account.Put(10);
                    logs.Add(playersAction.WIN, player2.Name, player2.AccountSum);
                }
                else
                {
                    game_result_msg.Append(string.Format("{0} lose! ", player2.Name));
                    player2.Account.Withdraw(10);
                    logs.Add(playersAction.LOSE, player2.Name, player2.AccountSum);
                }
                MessageBox.Show(game_result_msg.ToString(), "Game result", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Wrong rate format: it can be from 2 to 12.", "Rate error",
                            MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void btn_bestScores_Click(object sender, EventArgs e)
        {
            PlayersRaitingForm raitingForm = new PlayersRaitingForm(players);
            raitingForm.ShowDialog();
        }
    }
}
