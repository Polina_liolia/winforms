﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelegateEventConsoleTest;
using System.Windows.Forms;
using System.ComponentModel;

namespace Dice
{
    public enum playersAction{ INIT, WIN, LOSE};
    
    public class Player : INotifyPropertyChanged
    {
        public string Name { get; set; }
        private Account account;
        private playersAction actionStatus;
        private DateTime actionStatusChanged;

        #region Public properties
        public Account Account
        {
            get
            {
                return account;
            }

            set
            {
                account = value;
                NotifyPropertyChanged("Account");
                NotifyPropertyChanged("AccountSum");
            }
        }

        public double AccountSum
        {
            get
            {
                return Account.Sum;
            }
        }

        public playersAction ActionStatus
        {
            get
            {
                return actionStatus;
            }

            set
            {
                actionStatus = value;
                NotifyPropertyChanged("ActionStatus");
            }
        }

        public DateTime ActionStatusChanged
        {
            get
            {
                return actionStatusChanged;
            }

            set
            {
                actionStatusChanged = value;
                NotifyPropertyChanged("actionStatusChanged");
            }
        }
        #endregion

        public Player(string name, double accountSum)
        {
            Name = name;
            Account = new Account(accountSum);
            ActionStatus = playersAction.INIT;
            ActionStatusChanged = DateTime.Now;
            Account.Added += Account_sum_changing;
            Account.Withdrowed += Account_sum_changing;
        }

        public Player(string name, double accountSum, playersAction action, DateTime status_changed)
        {
            Name = name;
            Account = new Account(accountSum);
            ActionStatus = action;
            ActionStatusChanged = status_changed;
            Account.Added += Account_sum_changing;
            Account.Withdrowed += Account_sum_changing;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        //account sum is changing event handler
        private void Account_sum_changing(string message)
        {
            MessageBox.Show(string.Format("{0}: {1}", Name, message), "Account status",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            NotifyPropertyChanged("AccountSum");
        }
    }
 }