﻿namespace Dice
{
    partial class PlayersRaitingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_Raiting = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // lv_Raiting
            // 
            this.lv_Raiting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_Raiting.Location = new System.Drawing.Point(0, 0);
            this.lv_Raiting.Name = "lv_Raiting";
            this.lv_Raiting.Size = new System.Drawing.Size(284, 261);
            this.lv_Raiting.TabIndex = 0;
            this.lv_Raiting.UseCompatibleStateImageBehavior = false;
            // 
            // PlayersRaitingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lv_Raiting);
            this.Name = "PlayersRaitingForm";
            this.Text = "PlayersRaitingForm";
            this.Load += new System.EventHandler(this.PlayersRaitingForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lv_Raiting;
    }
}