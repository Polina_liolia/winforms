﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice
{
    public partial class PlayersRaitingForm : Form
    {
        private BindingList<Player> players;
        public PlayersRaitingForm(BindingList<Player> players)
        {
            InitializeComponent();
            this.players = players;
        }

        private void PlayersRaitingForm_Load(object sender, EventArgs e)
        {
            lv_Raiting.View = View.Details; 
            lv_Raiting.GridLines = true;
            lv_Raiting.FullRowSelect = true;
            lv_Raiting.MultiSelect = false; 
            lv_Raiting.Columns.Add("Name", 150);
            lv_Raiting.Columns.Add("Account, $", 100);
            foreach(Player p in players)
            {
                lv_Raiting.Items.Add(new ListViewItem(new string[] { p.Name, p.AccountSum.ToString() }));
            }
            lv_Raiting.ListViewItemSorter = new ListViewItemComparer(1);//sort by second column
            lv_Raiting.Sort();
        }
    }
}
