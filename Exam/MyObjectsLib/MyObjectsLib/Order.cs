﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MyObjectsLib
{
    public class Order : IEquatable<Order>, IComparable, IComparer<Order>, ICloneable, INotifyPropertyChanged
    {
        string description;
        DateTime order_date;
        double total_costs;
        Client client;
        List<Order_position> order_positions;

        #region Properties
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public DateTime Order_date
        {
            get
            {
                return order_date;
            }

            set
            {
                order_date = value;
                NotifyPropertyChanged("Order_date");
            }
        }

        public double Total_costs//auto-calculated, so read only
        {
            get
            {
                return total_costs;
            }
        }

        public Client Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
                NotifyPropertyChanged("Client");
            }
        }

        public List<Order_position> Order_positions
        {
            get
            {
                return order_positions;
            }
        }
        #endregion

        #region Constructors
        private Order() { }
        public Order(string description,
                        DateTime order_date,
                        Client client)
        {
            this.Description = description;
            this.Order_date = order_date;
            this.Client = client;
            this.order_positions = new List<Order_position>();
            this.total_costs = 0.0;
        }
        #endregion

        #region OrderPositions methods
        public void setOrderPosition(Order_position op)
        {
            Order_positions.Add(op);
            total_costs += op.Price;
            if (OrderPositionAdded != null) //event generation
                OrderPositionAdded(this, new OrderEventArgs("Order position added", op));
        }
       
        public bool removeOrderPosition(Order_position op)
        {
            bool result = Order_positions.Remove(op);
            if (result)
            {
                total_costs -= op.Price;
                if (OrderPositionRemoved != null) //event generation
                    OrderPositionRemoved(this, new OrderEventArgs("Order position removed", op));
            }
            return result;
        }

        public Order_position getOrderPosition(int index)
        {
            if (index >= Order_positions.Count)
                throw new IndexOutOfRangeException("No order position with such index found");
            return Order_positions[index];
        }

        public int countOrderPositions
        {
            get { return Order_positions.Count; }
        }

        public void printOrderPositions()
        {
            foreach (Order_position op in Order_positions)
                Console.WriteLine(op);
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Order description: {0}, date: {1}, total costs: {2}, client: {3}",
                description, Order_date, Total_costs, client.Clients_name);
        }
        public override int GetHashCode()
        {
            return order_date.GetHashCode();
        }

        #endregion

        #region Operators overloaded
        public static bool operator == (Order order1, Order order2)
        {
            if ((object)order1 == null || (object)order2 == null)
                return object.Equals(order1, order2);
            return order1.Equals(order2);
        }

        public static bool operator !=(Order order1, Order order2)
        {
            if ((object)order1 == null || (object)order2 == null)
                return !object.Equals(order1, order2);
            return !order1.Equals(order2);
        }

        public static bool operator > (Order order1, Order order2)
        {
            if (order1 == null && order2 != null ||
                order1 == null && order2 == null)
                return false;
            if (order1 != null && order2 == null)
                return true;
            return (order1.order_date > order2.order_date);
        }

        public static bool operator < (Order order1, Order order2)
        {
            if (order1 == null && order2 != null)
                return true;
            if (order1 != null && order2 == null ||
                order1 == null && order2 == null)
                return false;
            return (order1.order_date < order2.order_date);
        }
        #endregion

        #region IEquatable<Order>
        public bool Equals(Order other)
        {
            if ((object)other == null)
                return false;
            return (order_date == other.order_date);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Order order = obj as Order;
            if (order != null)
                return Equals(order);
            else
                throw new ArgumentException("Argument is not an Order");
        }
        #endregion

        #region IComparable
        public int CompareTo(Order other)
        {
            if (((object)other) == null)
                return 1;
            return order_date.CompareTo(other.order_date);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Order order = obj as Order;
            if (order != null)
                return this.CompareTo(order);
            else
                throw new ArgumentException("Argument is not an Order");
        }
        #endregion 

        #region IComparer<Order>
        public int Compare(Order x, Order y)
        {
            if (x == null && y != null)
                return -1;
            if (x != null && y == null)
                return 1;
            return x.CompareTo(y);
        }
        #endregion

        #region ICloneable
        public object Clone()
        {
            Order New = (Order)this.MemberwiseClone();
            New.Description = string.Copy(Description);
            New.Order_date = new DateTime(Order_date.Year, Order_date.Month, Order_date.Day, 
                Order_date.Hour, Order_date.Minute, Order_date.Second);
            New.client = new Client(client.Clients_name, client.Age, client.Region_info,
                client.Clients_phone, client.Clients_mail);
            New.order_positions = new List<Order_position>();
            foreach (Order_position op in Order_positions)
                New.setOrderPosition(op);
            return New;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Event
        public delegate void OrderStateHandler(object sender, OrderEventArgs e);
        private event OrderStateHandler OrderPositionAdded;
        private event OrderStateHandler OrderPositionRemoved;
        

        public void registerEventListener(OrderStateHandler func)
        {
            OrderPositionAdded += func;
            OrderPositionRemoved += func;
        }

        public void removeEventListener(OrderStateHandler func)
        {
            OrderPositionAdded -= func;
            OrderPositionRemoved -= func;
        }
        #endregion
    }

    public class OrderEventArgs
    {
        string message;
        Order_position order_position;

        #region Constructors
        private OrderEventArgs() { }
        public OrderEventArgs(string message, Order_position order_position)
        {
            this.message = message;
            this.order_position = order_position;
        }
        #endregion

        #region Properties
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        public Order_position Order_position
        {
            get
            {
                return order_position;
            }

            set
            {
                order_position = value;
            }
        }
        #endregion
    }
}
