﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientView
{
    public partial class ClientView : UserControl
    {
        public ClientView()
        {
            InitializeComponent();
        }

        public string ClientName { get { return txt_name.Text; } set { txt_name.Text = value; } }


        public int ClientAge
        {
            get
            {
                int age;
                Int32.TryParse(txt_Age.Text, out age);
                return age;
            }
            set
            {
                txt_Age.Text = value.ToString();
            }
        }

        public string Region_info { get { return txt_region.Text; } set { txt_region.Text = value; } }
        public string ClientsPhone { get { return txt_phone.Text; } set { txt_phone.Text = value; } }
        public string ClientsMail { get { return txt_email.Text; } set { txt_email.Text = value; } }
    }
}
