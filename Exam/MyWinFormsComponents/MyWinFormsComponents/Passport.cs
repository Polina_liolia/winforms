﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Passport
{
    public partial class Passport : UserControl
    {
        public Passport()
        {
            InitializeComponent();
            cb_series.Items.AddRange(new string[] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" });
        }

        #region "Глобальные" (для всего компонента) свойства
        [Browsable(true)] //свойство Text будет доступно в свойствах компонента
        public override string Text //перенаправляем текст текстбоксу внутри компонента
        {
            get
            {
                return txt_passportNumber.Text;
            }

            set
            {
                txt_passportNumber.Text = value;
            }
        }

        [Browsable(true)] //свойство Series будет доступно в свойствах компонента
        public string Series //возвращаем значение выбранной серии паспорта
        {
            get
            {
                if (cb_series.SelectedItem != null)
                    return cb_series.SelectedItem.ToString();
                return "";
            }
        }

        [Browsable(true)] //свойство Text будет доступно в свойствах компонента
        public DateTime IssueDate //перенаправляем текст текстбоксу внутри компонента
        {
            get
            {
                return dt_issueDate.Value;
            }
            set
            {
                dt_issueDate.Value = value;
            }
           
        }
        #endregion

        public void DataChanged(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("Series: {0} Number: {1} Date: {2}", this.Series, this.Text, this.IssueDate.ToShortDateString()));
        }

        private void txt_passportNumber_Leave(object sender, EventArgs e)
        { 
            Regex reg = new Regex(@"^\d{6}$");
            if (!reg.IsMatch(txt_passportNumber.Text))
                MessageBox.Show("Passport number must contain 6 digits", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else 
                this.DataChanged(sender, e);
        }
    }
}
