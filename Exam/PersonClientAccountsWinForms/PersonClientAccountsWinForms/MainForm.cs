﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyObjectsLib;

namespace PersonClientAccountsWinForms
{
    public partial class MainForm : Form
    {
        private List<Client> clients = new List<Client>();
        private List<Account> accounts = new List<Account>();
        private List<Product> products = new List<Product>();
        private List<Order_position> order_positions = new List<Order_position>();
        private List<Order> orders = new List<Order>();

        private DataSet dataset = new DataSet();
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Populating List containers
            products.Add(new Product("mobile_phone", 10240.0));
            products.Add(new Product("laptop", 22620.0));
            products.Add(new Product("TV", 15710.0));
            products.Add(new Product("printer", 7125.0));

            clients.Add(new Client("Anton", 22, "Kh", "0663451234", "mail@gmail.com"));
            clients.Add(new Client("Alex", 24, "Kyiv", "+380502058574", "alex@list.ru"));
            clients.Add(new Client("Mary", 32, "Dnepr", "+380982058522", "mary@gmail.com", false));
            clients.Add(new Client("Liz", 21, "Kh", "+380972258578", "liz@mail.ru", false));

            accounts.Add(new Account(clients[0], 356.9, "2620127654322"));
            accounts.Add(new Account(clients[0], 12587.33, "2625127652174"));
            clients[0].Accounts.Add(accounts[0]);
            clients[0].Accounts.Add(accounts[1]);

            accounts.Add(new Account(clients[1], 147.3, "2620127658596"));
            clients[1].Accounts.Add(accounts[2]);

            accounts.Add(new Account(clients[2], 5896.32, "2620452658511"));
            clients[2].Accounts.Add(accounts[3]);

            accounts.Add(new Account(clients[3], 4178.58, "2620745658532"));
            clients[3].Accounts.Add(accounts[4]);
            
            orders.Add(new Order("delivery time after 6pm", DateTime.Now, clients[0]));
            order_positions.Add(new Order_position(products[0], 2, orders[0]));
            order_positions.Add(new Order_position(products[1], 3, orders[0]));
            orders[0].setOrderPosition(order_positions[0]);
            orders[0].setOrderPosition(order_positions[1]);

            orders.Add(new Order("knock at the door only", DateTime.Now, clients[1]));
            order_positions.Add(new Order_position(products[2], 1, orders[1]));
            order_positions.Add(new Order_position(products[3], 1, orders[1]));
            orders[1].setOrderPosition(order_positions[2]);
            orders[1].setOrderPosition(order_positions[3]);
            #endregion

            #region DataSet populating
            //creating a new table in dataset with information about clients:
            dataset.Tables.Add("Clients");
            dataset.Tables["Clients"].Columns.Add(new DataColumn("Name"));
            dataset.Tables["Clients"].Columns.Add(new DataColumn("Age", Type.GetType("System.Int32")));
            dataset.Tables["Clients"].Columns.Add(new DataColumn("Region"));
            dataset.Tables["Clients"].Columns.Add(new DataColumn("E-mail"));
            dataset.Tables["Clients"].Columns.Add(new DataColumn("Sex"));
            dataset.Tables["Clients"].Columns.Add(new DataColumn("Account"));

            foreach(Client c in clients)
            {
                DataRow row = dataset.Tables["Clients"].NewRow();
                row["Name"] = c.Clients_name;
                row["Age"] = c.Age;
                row["Region"] = c.Region_info;
                row["E-mail"] = c.Clients_mail;
                row["Sex"] = c.Sex.ToString();
                row["Account"] = c.Accounts[0].Account_number;
                dataset.Tables["Clients"].Rows.Add(row);
                dataset.Tables["Clients"].AcceptChanges();
            }

            //creating a new table in dataset with information about clients' accounts:
            dataset.Tables.Add("Accounts");
            dataset.Tables["Accounts"].Columns.Add("Account number");
            dataset.Tables["Accounts"].Columns.Add("Client's name");

            foreach(Account account in accounts)
            {
                DataRow row = dataset.Tables["Accounts"].NewRow();
                row["Account number"] = account.Account_number;
                row["Client's name"] = account.Client.Clients_name;
                dataset.Tables["Accounts"].Rows.Add(row);
                dataset.Tables["Accounts"].AcceptChanges();
            }

            //creating a new table in dataset with information about products:
            dataset.Tables.Add("Products");
            dataset.Tables["Products"].Columns.Add("Product name");
            dataset.Tables["Products"].Columns.Add("Price, UAH");

            foreach(Product p in products)
            {
                DataRow row = dataset.Tables["Products"].NewRow();
                row["Product name"] = p.Product_name;
                row["Price, UAH"] = p.Price;
                dataset.Tables["Products"].Rows.Add(row);
                dataset.Tables["Products"].AcceptChanges();
            }

            //creating a new table in dataset with information about orders:
            dataset.Tables.Add("Orders");
            dataset.Tables["Orders"].Columns.Add("Date");
            dataset.Tables["Orders"].Columns.Add("Client's name");
            dataset.Tables["Orders"].Columns.Add("Order positions");

            foreach(Order order in orders)
            {
                DataRow row = dataset.Tables["Orders"].NewRow();
                row["Date"] = order.Order_date;
                row["Client's name"] = order.Client.Clients_name;
                row["Order positions"] = order.Order_positions;
                dataset.Tables["Orders"].Rows.Add(row);
                dataset.Tables["Orders"].AcceptChanges();
            }

            #endregion

            #region Data bindings
            dgv_Clients.DataSource = dataset.Tables["Clients"];
            cBox_Clients.DataSource = dataset.Tables["Clients"];
            cBox_Clients.DisplayMember = "Name";
            cBox_Clients.ValueMember = "Name";

            dgv_Accounts.DataSource = dataset.Tables["Accounts"];
            dgv_Products.DataSource = dataset.Tables["Products"];
            cBox_Orders.DataSource = dataset.Tables["Orders"];
            dgv_OrdersPositions.DataSource = dataset.Tables["Orders"].Columns["Order positions"]; //??????
            
            #endregion

            #region Setting settings for interface elements
            dgvClientsSetSettings();
            cBoxSetSettings(cBox_Clients);
            dgvAccountsSetSettings();
            dgvProductsSetSettings();
            cBoxSetSettings(cBox_Orders);
            #endregion
        }

        #region Settings
        private void dgvClientsSetSettings()
        {
            dgv_Clients.AllowUserToDeleteRows = false;
            dgv_Clients.AllowUserToAddRows = false;
            dgv_Clients.AllowUserToOrderColumns = true;
            dgv_Clients.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv_Clients.ReadOnly = false;
            dgv_Clients.RowHeadersVisible = false;
            dgv_Clients.RowTemplate.Height = 24;
            dgv_Clients.RowTemplate.Resizable = DataGridViewTriState.False;
            dgv_Clients.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv_Clients.Columns["Age"].Width = 55;
            dgv_Clients.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void dgvAccountsSetSettings()
        {
            dgv_Accounts.AllowUserToDeleteRows = false;
            dgv_Accounts.AllowUserToAddRows = false;
            dgv_Accounts.AllowUserToOrderColumns = true;
            dgv_Accounts.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv_Accounts.ReadOnly = false;
            dgv_Accounts.RowHeadersVisible = false;
            dgv_Accounts.RowTemplate.Height = 24;
            dgv_Accounts.RowTemplate.Resizable = DataGridViewTriState.False;
            dgv_Accounts.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv_Accounts.Columns["Account number"].Width = 200;
            dgv_Accounts.Columns["Client's name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void dgvProductsSetSettings()
        {
            dgv_Products.AllowUserToDeleteRows = false;
            dgv_Products.AllowUserToAddRows = false;
            dgv_Products.AllowUserToOrderColumns = true;
            dgv_Products.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv_Products.ReadOnly = false;
            dgv_Products.RowHeadersVisible = false;
            dgv_Products.RowTemplate.Height = 24;
            dgv_Products.RowTemplate.Resizable = DataGridViewTriState.False;
            dgv_Products.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv_Products.Columns["Product name"].Width = 600;
            dgv_Products.Columns["Price, UAH"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void cBoxSetSettings(ComboBox cBox)
        {
            cBox.DropDownStyle = ComboBoxStyle.DropDown;
            cBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            cBox.AutoCompleteSource = AutoCompleteSource.ListItems;
        }
        #endregion

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //changing 1 and 0 value of "Sex" column on "Male" and "Female":
            DataGridView dgv = sender as DataGridView;
            if (dgv != null)
            {
                string columnName = dgv.Columns[e.ColumnIndex].Name;
                if (e.Value != null && columnName == "Sex")
                {
                    e.Value = e.Value.ToString() == "True" ? "Male" : "Female";
                }
            }
            //setting forecolor for numbers (red if <0 and blue if >0):
            int intVal;
            double doubleVal;
            Int32.TryParse(e.Value.ToString(), out intVal);
            Double.TryParse(e.Value.ToString(), out doubleVal);
            if (intVal < 0 || doubleVal < 0)
                e.CellStyle.ForeColor = Color.Red;
            if (intVal > 0 || doubleVal > 0)
                e.CellStyle.ForeColor = Color.Blue;

            //setting light gray backcolor for cells of even rows
            if (e.RowIndex % 2 == 0)
                e.CellStyle.BackColor = Color.LightGray;
        }

        //Occurs when a cell leaves edit mode if the cell value has been modified.
        private void dgv_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            //changing 1 and 0 value of "Sex" column on "Male" and "Female":
            DataGridView dgv = sender as DataGridView;
            if (dgv != null)
            {
                string columnName = dgv.Columns[e.ColumnIndex].Name;
                if (e.Value != null && columnName == "Sex")
                {
                    e.Value = e.Value.ToString() == "True" ? "Male" : "Female";
                }
            }

            //setting forecolor for numbers (red if <0 and blue if >0):
            int intVal;
            double doubleVal;
            Int32.TryParse(e.Value.ToString(), out intVal);
            Double.TryParse(e.Value.ToString(), out doubleVal);
            if (intVal < 0 || doubleVal < 0)
                e.InheritedCellStyle.ForeColor = Color.Red;
            if (intVal > 0 || doubleVal > 0)
                e.InheritedCellStyle.ForeColor = Color.Blue;
        }

        private void dgv_Clients_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv != null)
            {
                // Try to sort based on the cells in the current column
                e.SortResult = System.String.Compare(
                e.CellValue1.ToString(), e.CellValue2.ToString());

                // If the cells are equal, sort based on the Account column (account is always unique)
                if (e.SortResult == 0 && e.Column.Name != "Account")
                {
                    e.SortResult = System.String.Compare(
                        dgv.Rows[e.RowIndex1].Cells["Account"].Value.ToString(),
                        dgv.Rows[e.RowIndex2].Cells["Account"].Value.ToString());
                }
                e.Handled = true;
            }
        }

        //showing date and client's name together in comboBox
        private void cBox_Orders_Format(object sender, ListControlConvertEventArgs e)
        {
            DataRowView dr = e.ListItem as DataRowView;
            if (dr != null)
            {
                e.Value = string.Format("{0}, {1}", dr.Row["Date"].ToString(), dr.Row["Client's name"]);
            }
        }
    }
}
