﻿namespace PersonClientWinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_Clients = new System.Windows.Forms.ListView();
            this.cbox_nameAge = new System.Windows.Forms.ComboBox();
            this.cBoxRegionMail = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbox_regionFilter = new System.Windows.Forms.ComboBox();
            this.cBox_mailFilter = new System.Windows.Forms.ComboBox();
            this.btn_addClient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_Clients
            // 
            this.lv_Clients.Location = new System.Drawing.Point(27, 58);
            this.lv_Clients.Name = "lv_Clients";
            this.lv_Clients.Size = new System.Drawing.Size(585, 186);
            this.lv_Clients.TabIndex = 0;
            this.lv_Clients.UseCompatibleStateImageBehavior = false;
            // 
            // cbox_nameAge
            // 
            this.cbox_nameAge.FormattingEnabled = true;
            this.cbox_nameAge.Location = new System.Drawing.Point(27, 287);
            this.cbox_nameAge.Name = "cbox_nameAge";
            this.cbox_nameAge.Size = new System.Drawing.Size(168, 21);
            this.cbox_nameAge.TabIndex = 3;
            this.cbox_nameAge.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbox_nameAge_Format);
            // 
            // cBoxRegionMail
            // 
            this.cBoxRegionMail.FormattingEnabled = true;
            this.cBoxRegionMail.Location = new System.Drawing.Point(226, 286);
            this.cBoxRegionMail.Name = "cBoxRegionMail";
            this.cBoxRegionMail.Size = new System.Drawing.Size(386, 21);
            this.cBoxRegionMail.TabIndex = 4;
            this.cBoxRegionMail.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cBoxRegionMail_Format);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Filter by region";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(339, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Filter by mail";
            // 
            // cbox_regionFilter
            // 
            this.cbox_regionFilter.FormattingEnabled = true;
            this.cbox_regionFilter.Location = new System.Drawing.Point(108, 24);
            this.cbox_regionFilter.Name = "cbox_regionFilter";
            this.cbox_regionFilter.Size = new System.Drawing.Size(199, 21);
            this.cbox_regionFilter.TabIndex = 7;
            this.cbox_regionFilter.SelectedIndexChanged += new System.EventHandler(this.filters_SelectedIndexChanged);
            // 
            // cBox_mailFilter
            // 
            this.cBox_mailFilter.FormattingEnabled = true;
            this.cBox_mailFilter.Location = new System.Drawing.Point(418, 24);
            this.cBox_mailFilter.Name = "cBox_mailFilter";
            this.cBox_mailFilter.Size = new System.Drawing.Size(194, 21);
            this.cBox_mailFilter.TabIndex = 8;
            this.cBox_mailFilter.SelectedIndexChanged += new System.EventHandler(this.filters_SelectedIndexChanged);
            // 
            // btn_addClient
            // 
            this.btn_addClient.Location = new System.Drawing.Point(537, 250);
            this.btn_addClient.Name = "btn_addClient";
            this.btn_addClient.Size = new System.Drawing.Size(75, 23);
            this.btn_addClient.TabIndex = 9;
            this.btn_addClient.Text = "+";
            this.btn_addClient.UseVisualStyleBackColor = true;
            this.btn_addClient.Click += new System.EventHandler(this.btn_addClient_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 454);
            this.Controls.Add(this.btn_addClient);
            this.Controls.Add(this.cBox_mailFilter);
            this.Controls.Add(this.cbox_regionFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBoxRegionMail);
            this.Controls.Add(this.cbox_nameAge);
            this.Controls.Add(this.lv_Clients);
            this.Name = "MainForm";
            this.Text = "Clients form";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_Clients;
        private System.Windows.Forms.ComboBox cbox_nameAge;
        private System.Windows.Forms.ComboBox cBoxRegionMail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbox_regionFilter;
        private System.Windows.Forms.ComboBox cBox_mailFilter;
        private System.Windows.Forms.Button btn_addClient;
    }
}

