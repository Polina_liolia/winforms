﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PointInOutShape
{
    public partial class MainForm : Form
    {
        private Shape currentShape;
        private Point point;
        private bool shapeCoordsSetted;
        private Point[] lineX = { new Point(250, 550), new Point(1100, 550) };
        private Point[] lineY = { new Point(250, 550), new Point(250, 20) };
        private Rectangle rectangle = new Rectangle();
        private Rectangle circle = new Rectangle();
        private Point[] triangle = new Point[3];

        public MainForm()
        {
            InitializeComponent();
            cBox_shape.Items.AddRange(new string[] { "Rectangle", "Circle", "Triangle" });
            cBox_shape.SelectedIndexChanged += CBox_shape_SelectedIndexChanged;
            point = new Point(0, 0);
            shapeCoordsSetted = false;
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            drawAxes();
        }

        private void CBox_shape_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentShape = cBox_shape.SelectedIndex == 0 ? Shape.RECT :
                cBox_shape.SelectedIndex == 1 ? Shape.CIRCLE : Shape.TRIANGLE;
            ShapeCoordinatesForm coordsForm = new ShapeCoordinatesForm(currentShape);
            coordsForm.ShowDialog();
            shapeCoordsSetted = currentShape == Shape.RECT && Shape_coordinates.HaveRect ? true :
                currentShape == Shape.CIRCLE && Shape_coordinates.HaveCircle ? true :
                currentShape == Shape.TRIANGLE && Shape_coordinates.HaveTriangle ? true : false;
            if (shapeCoordsSetted)
                build_shape();
        }

        private void btn_go_Click(object sender, EventArgs e)
        {
            int x, y;
            bool parsed = Int32.TryParse(txt_X.Text, out x);
            if (parsed)
            {
                parsed = Int32.TryParse(txt_Y.Text, out y);
                if (parsed)
                {
                    point = new Point(x, y);
                    if (shapeCoordsSetted)
                    {
                        mark_point(new Point(x, y));
                        if(isPointInShape())
                        {
                            MessageBox.Show("Selected point lies inside shape.", "Result",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Selected point DOES NOT lie inside shape.", "Result",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Select shape first!", "Shape not selected", MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                    }
                }
            }
            if (!parsed)
                MessageBox.Show("Wrong input format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            shapeCoordsSetted = false;
            destroyShapes();
            this.Invalidate();
            foreach (Control c in Controls)
            {
                if (c.Name.StartsWith("markX") || c.Name.StartsWith("markY"))
                    c.Hide();
            }
        }

        private bool isPointInShape()
        {
           switch(currentShape)
            {
                case Shape.RECT:
                    {
                        return isPointInRect();
                    }
                case Shape.CIRCLE:
                    {
                        return isPointInCircle();
                    }
                case Shape.TRIANGLE:
                    {
                        return isPointInTriangle(Shape_coordinates.TriangleCoords[0], 
                            Shape_coordinates.TriangleCoords[1],
                            Shape_coordinates.TriangleCoords[2], point);
                    }
            }
            return false;
        }

        private bool isPointInTriangle(Point top1, Point top2, Point top3, Point p)
        {
            int statement1 = (top1.X - p.X) * (top2.Y - top1.Y) - (top2.X - top1.X) * (top1.Y - p.Y);
            int statement2 = (top2.X - p.X) * (top3.Y - top2.Y) - (top3.X - top2.X) * (top2.Y - p.Y);
            int statement3 = (top3.X - p.X) * (top1.Y - top3.Y) - (top1.X - top3.X) * (top3.Y - p.Y);

            return (statement1 >= 0 && statement2 >= 0 && statement3 >= 0 ||
                statement1 <= 0 && statement2 <= 0 && statement3 <= 0);
        }

        private bool isPointInCircle()
        {
            //если расстояние от точки до центра окружности меньше радиуса окружности, то точка в круге
            return (calcDistanceBetweenPoints(point, Shape_coordinates.CircleCenter) < Shape_coordinates.Radius);
        }

        private double calcDistanceBetweenPoints(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        private bool isPointInRect()
        {
            return (point.X > Shape_coordinates.RectTopLeft.X &&
                point.X < Shape_coordinates.RectTopLeft.X + Shape_coordinates.RectWidth &&
                point.Y < Shape_coordinates.RectTopLeft.Y &&
                point.Y > Shape_coordinates.RectTopLeft.Y - Shape_coordinates.RectHeight);
        }

        private void build_shape()
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            if (currentShape == Shape.RECT)
            {
                rectangle.Width = Math.Abs(Shape_coordinates.RectWidth);
                rectangle.Height = Math.Abs(Shape_coordinates.RectHeight);
                Point location = new Point(Shape_coordinates.RectTopLeft.X + lineX[0].X,
                    lineY[0].Y - Shape_coordinates.RectTopLeft.Y);
                rectangle.Location = location;
                gr.DrawRectangle(Pens.Black, this.rectangle);
                gr.FillRectangle(Brushes.Red, rectangle);
                //marks on axes:
                mark_point(Shape_coordinates.RectTopLeft, false);
                mark_point(new Point(Shape_coordinates.RectTopLeft.X + Shape_coordinates.RectWidth,
                   Shape_coordinates.RectTopLeft.Y - Shape_coordinates.RectHeight), false);
                return;
            }
            if (currentShape == Shape.CIRCLE)
            {
                int radius = Math.Abs(Shape_coordinates.Radius);
                circle.Location = new Point(lineX[0].X + Shape_coordinates.CircleCenter.X - radius,
                    (lineY[0].Y - Shape_coordinates.CircleCenter.Y - radius));
                circle.Width = circle.Height = radius * 2;
                gr.DrawEllipse(Pens.Black, circle);
                gr.FillEllipse(Brushes.Blue, circle);
                //marks on axes:
                mark_point(new Point(Shape_coordinates.CircleCenter.X - radius,
                    (Shape_coordinates.CircleCenter.Y - radius)), false);
                mark_point(new Point(Shape_coordinates.CircleCenter.X + radius,
                    (Shape_coordinates.CircleCenter.Y + radius)), false);
                return;
            }
            if (currentShape == Shape.TRIANGLE)
            {
                int i = 0;
                foreach(Point p in Shape_coordinates.TriangleCoords)
                {
                    triangle[i++] = new Point(lineX[0].X + p.X, lineY[0].Y - p.Y);
                }
                if (check_triangle(triangle[0], triangle[1], triangle[2]))
                {
                    gr.DrawPolygon(Pens.Black, triangle);
                    gr.FillPolygon(Brushes.Green, triangle);
                }
                else
                {
                    shapeCoordsSetted = false;
                    MessageBox.Show("Such triangle does not exist!", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Stop);
                }
                return;
            }
        }

        private bool check_triangle(Point p1, Point p2, Point p3)
        {
            /*Расстояние между двумя точками равно квадратному корню из
             *  суммы квадратов разностей координат по каждой оси. */
            double a = calcDistanceBetweenPoints(p1, p2);
            double b = calcDistanceBetweenPoints(p2, p3);
            double c = calcDistanceBetweenPoints(p1, p3);
            /*У треугольника сумма любых двух сторон должна быть больше третьей*/
            if (a + b > c || a + c > b || b + c > a)
                return true;
            return false;
        }

        //destroys all shapes, that could be built:
        private void destroyShapes()
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            rectangle.Size = new Size(0, 0);
            circle.Size = new Size(0, 0);
            triangle[0] = new Point(0, 0);
            triangle[1] = new Point(0, 0);
            triangle[2] = new Point(0, 0);
            gr.DrawRectangle(Pens.Black, rectangle);
            gr.DrawEllipse(Pens.Black, circle);
            gr.DrawPolygon(Pens.Black, triangle);
        }

        private void drawAxes()
        {
            //coordinate axes drawing:
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawLine(Pens.Black, lineX[0], lineX[1]);
            gr.DrawLine(Pens.Black, lineY[0], lineY[1]);

            gr.DrawLine(Pens.Black, lineX[1], new Point(lineX[1].X - 10, lineX[1].Y - 7));
            gr.DrawLine(Pens.Black, lineX[1], new Point(lineX[1].X - 10, lineX[1].Y + 7));

            gr.DrawLine(Pens.Black, lineY[1], new Point(lineY[1].X + 7, lineY[1].Y + 10));
            gr.DrawLine(Pens.Black, lineY[1], new Point(lineY[1].X - 7, lineY[1].Y + 10));

            Label lbl_startPoint = new Label();
            lbl_startPoint.Name = "lbl_startPoint";
            lbl_startPoint.Text = "0";
            lbl_startPoint.AutoSize = true;
            lbl_startPoint.Location = new Point(lineX[0].X - 10, lineX[0].Y + 10);

            Label lbl_axeX = new Label();
            lbl_axeX.Name = "lbl_axeX";
            lbl_axeX.Text = "X";
            lbl_axeX.AutoSize = true;
            lbl_axeX.Location = new Point(lineX[1].X - 10, lineX[1].Y + 10);

            Label lbl_axeY = new Label();
            lbl_axeY.Name = "lbl_axeY";
            lbl_axeY.Text = "Y";
            lbl_axeY.AutoSize = true;
            lbl_axeY.Location = new Point(lineY[1].X - 20, lineY[1].Y + 10);

            Controls.AddRange(new Control[] { lbl_startPoint, lbl_axeX, lbl_axeY });
            this.SuspendLayout();
        }

        //marks point on field and X, Y values on axes 
        private void mark_point(Point p, bool draw_point = true)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            //mark on axe X:
            gr.DrawLine(Pens.Black,
                new Point(lineX[0].X + p.X , lineX[0].Y - 5),
                new Point(lineX[0].X + p.X, lineX[0].Y + 5));
            Label markX = new Label();
            markX.AutoSize = true;
            markX.Name = string.Format("markX{0}", p.X);
            markX.Text = p.X.ToString();
            markX.Location = new Point(lineX[0].X + p.X - 10, lineX[0].Y + 6);
            
            //mark on axe Y:
            gr.DrawLine(Pens.Black,
                new Point(lineY[0].X - 5, lineY[0].Y - p.Y),
                new Point(lineY[0].X + 5, lineY[0].Y - p.Y));
            Label markY = new Label();
            markY.AutoSize = true;
            markY.Name = string.Format("markY{0}", p.Y);
            markY.Text = p.Y.ToString();
            markY.Location = new Point(lineY[0].X - 30, lineY[0].Y - p.Y - 6);

            if (draw_point)
            {
                //displaying point:
                gr.FillRectangle(Brushes.Black, lineX[0].X + p.X, lineY[0].Y - p.Y, 2, 2);
            }
            
            Controls.AddRange(new Control[] { markX, markY });
            this.SuspendLayout();
        }

        
    }
}
