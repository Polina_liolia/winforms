﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PointInOutShape
{
    internal enum Shape { RECT, TRIANGLE, CIRCLE }
    internal partial class ShapeCoordinatesForm : Form
    {
        Shape shape;
        TextBox txt_X;
        TextBox txt_Y;
        TextBox txt_width;
        TextBox txt_height;
        TextBox txt_radius;
        TextBox txt_X1;
        TextBox txt_Y1;
        TextBox txt_X2;
        TextBox txt_Y2;
        TextBox txt_X3;
        TextBox txt_Y3;
        Button ok;

        public ShapeCoordinatesForm(Shape s)
        {
            InitializeComponent();
            shape = s;
        }

        private void ShapeCoordinates_Load(object sender, EventArgs e)
        {
            switch(shape)
            {
                case Shape.RECT:
                    {
                        Label lbl_comment = new Label();
                        lbl_setProperties(lbl_comment, new Point(10, 10), 
                            "lbl_comment", "Input coordinates of rectangle top left corner:");
                        Label lbl_X = new Label();
                        lbl_setProperties(lbl_X, new Point(10, 30), "lbl_X", "X:");
                        Label lbl_Y = new Label();
                        lbl_setProperties(lbl_Y, new Point(10, 60), "lbl_Y", "Y:");
                        Label lbl_width = new Label();
                        lbl_setProperties(lbl_width, new Point(10, 90), "lbl_width", "Width:");
                        Label lbl_height = new Label();
                        lbl_setProperties(lbl_height, new Point(10, 120), "lbl_height", "Height:");

                        txt_X = new TextBox();
                        txt_setProperties(txt_X, new Point(70, 30), "txt_X");
                        txt_Y = new TextBox();
                        txt_setProperties(txt_Y, new Point(70, 60), "txt_Y");
                        txt_width = new TextBox();
                        txt_setProperties(txt_width, new Point(70, 90), "txt_width");
                        txt_height = new TextBox();
                        txt_setProperties(txt_height, new Point(70, 120), "txt_height");

                        Controls.AddRange(new Control[] { lbl_comment, lbl_X, lbl_Y, lbl_width, lbl_height,
                        txt_X, txt_Y,  txt_width, txt_height });
                       
                    }
                    break;
                case Shape.CIRCLE:
                    {
                        Label lbl_comment = new Label();
                        lbl_setProperties(lbl_comment, new Point(10, 10),
                            "lbl_comment", "Input coordinates of circle centre and its radius:");
                        Label lbl_X = new Label();
                        lbl_setProperties(lbl_X, new Point(10, 30), "lbl_X", "X:");
                        Label lbl_Y = new Label();
                        lbl_setProperties(lbl_Y, new Point(10, 60), "lbl_Y", "Y:");
                        Label lbl_radius = new Label();
                        lbl_setProperties(lbl_radius, new Point(10, 90), "lbl_radius", "Radius:");

                        txt_X = new TextBox();
                        txt_setProperties(txt_X, new Point(70, 30), "txt_X");
                        txt_Y = new TextBox();
                        txt_setProperties(txt_Y, new Point(70, 60), "txt_Y");
                        txt_radius = new TextBox();
                        txt_setProperties(txt_radius, new Point(70, 90), "txt_radius");

                        Controls.AddRange(new Control[] { lbl_comment, lbl_X, lbl_Y, lbl_radius,
                        txt_X, txt_Y, txt_radius });
                    }
                    break;
                case Shape.TRIANGLE:
                    {
                        Label lbl_comment = new Label();
                        lbl_setProperties(lbl_comment, new Point(10, 10),
                            "lbl_comment", "Input coordinates of each triangle vertex:");
                        Label lbl_X1 = new Label();
                        lbl_setProperties(lbl_X1, new Point(10, 30), "lbl_X1", "X1:");
                        Label lbl_Y1 = new Label();
                        lbl_setProperties(lbl_Y1, new Point(10, 50), "lbl_Y1", "Y1:");

                        Label lbl_X2 = new Label();
                        lbl_setProperties(lbl_X2, new Point(10, 100), "lbl_X2", "X2:");
                        Label lbl_Y2 = new Label();
                        lbl_setProperties(lbl_Y2, new Point(10, 120), "lbl_Y2", "Y2:");

                        Label lbl_X3 = new Label();
                        lbl_setProperties(lbl_X3, new Point(10, 170), "lbl_X3", "X3:");
                        Label lbl_Y3 = new Label();
                        lbl_setProperties(lbl_Y3, new Point(10, 190), "lbl_Y3", "Y3:");

                        txt_X1 = new TextBox();
                        txt_setProperties(txt_X1, new Point(70, 30), "txt_X1");
                        txt_Y1 = new TextBox();
                        txt_setProperties(txt_Y1, new Point(70, 50), "txt_Y1");

                        txt_X2 = new TextBox();
                        txt_setProperties(txt_X2, new Point(70, 100), "txt_X2");
                        txt_Y2 = new TextBox();
                        txt_setProperties(txt_Y2, new Point(70, 120), "txt_Y2");

                        txt_X3 = new TextBox();
                        txt_setProperties(txt_X3, new Point(70, 170), "txt_X2");
                        txt_Y3 = new TextBox();
                        txt_setProperties(txt_Y3, new Point(70, 190), "txt_Y2");

                        Controls.AddRange(new Control[] { lbl_comment, lbl_X1, lbl_Y1, lbl_X2, lbl_Y2,
                        lbl_X3, lbl_Y3, txt_X1, txt_Y1, txt_X2, txt_Y2, txt_X3, txt_Y3 });
                    }
                    break;
            }
            ok = new Button();
            ok.Text = "OK";
            ok.Name = "btn_ok";
            ok.Location = new Point(10, 220);
            ok.Click += Ok_Click;
            Controls.Add(ok);
            this.SuspendLayout();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            switch (shape)
            {
                case Shape.RECT:
                    {
                        int x, y, width, height;
                        bool parsed = Int32.TryParse(this.txt_X.Text, out x);
                        if(parsed)
                        {
                            parsed = Int32.TryParse(this.txt_Y.Text, out y);
                            if (parsed)
                            {
                                parsed = Int32.TryParse(this.txt_width.Text, out width);
                                if (parsed)
                                {
                                    parsed = Int32.TryParse(this.txt_height.Text, out height);
                                    if (parsed) //all data was parsed
                                    {
                                        //setting rectangle properties of static class
                                        Shape_coordinates.clearAll();
                                        Shape_coordinates.HaveRect = true;
                                        Shape_coordinates.RectTopLeft = new Point(x, y);
                                        Shape_coordinates.RectWidth = width;
                                        Shape_coordinates.RectHeight = height;
                                        this.Close();
                                    }
                                }
                            }
                            if (!parsed) // not all data was parsed
                                MessageBox.Show("Wrong input format.", "Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case Shape.CIRCLE:
                    {
                        int x, y, radius;
                        bool parsed = Int32.TryParse(this.txt_X.Text, out x);
                        if (parsed)
                        {
                            parsed = Int32.TryParse(this.txt_Y.Text, out y);
                            if (parsed)
                            {
                                parsed = Int32.TryParse(this.txt_radius.Text, out radius);
                                if (parsed) //all data was parsed
                                {
                                    //setting rectangle properties of static class
                                    Shape_coordinates.clearAll();
                                    Shape_coordinates.HaveCircle = true;
                                    Shape_coordinates.CircleCenter = new Point(x, y);
                                    Shape_coordinates.Radius = radius;
                                    this.Close();

                                }
                            }
                        }
                            if (!parsed) // not all data was parsed
                                MessageBox.Show("Wrong input format.", "Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Stop);

                        }
                    break;
                case Shape.TRIANGLE:
                    {
                        int x1, y1, x2, y2, x3, y3;
                        bool parsed = Int32.TryParse(this.txt_X1.Text, out x1);
                        if (parsed)
                        {
                            parsed = Int32.TryParse(this.txt_Y1.Text, out y1);
                            if (parsed)
                            {
                                parsed = Int32.TryParse(this.txt_X2.Text, out x2);
                                if (parsed)
                                {
                                    parsed = Int32.TryParse(this.txt_Y2.Text, out y2);
                                    if (parsed)
                                    {
                                        parsed = Int32.TryParse(this.txt_X3.Text, out x3);
                                        if (parsed)
                                        {
                                            parsed = Int32.TryParse(this.txt_Y3.Text, out y3);
                                            if (parsed) //all data was parsed
                                            {
                                                //setting rectangle properties of static class
                                                Shape_coordinates.clearAll();
                                                Shape_coordinates.HaveTriangle = true;
                                                Shape_coordinates.TriangleCoords = new Point[]
                                                {
                                                    new Point(x1, y1),
                                                    new Point(x2, y2),
                                                    new Point(x3, y3)
                                                };
                                                this.Close();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                            if (!parsed) // not all data was parsed
                               MessageBox.Show("Wrong input format.", "Error", MessageBoxButtons.OK,
                                   MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void lbl_setProperties(Label lbl, Point location, string name, string text)
        {
            lbl.AutoSize = true;
            lbl.Location = location;
            lbl.Name = name;
            lbl.Text = text;
        }

        private void txt_setProperties(TextBox txt, Point location, string name, string text="")
        {
            txt.Location = location;
            txt.Name = name;
            txt.Size = new Size(120, 20);
            txt.Text = text;
            txt.Parent = this;
        }
    }
}
