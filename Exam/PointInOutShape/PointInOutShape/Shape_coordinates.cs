﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointInOutShape
{
    public static class Shape_coordinates
    {
        public static bool HaveRect { get; set; }
        public static bool HaveCircle { get; set; }
        public static bool HaveTriangle { get; set; }
        public static Point CircleCenter { get; set; }
        public static int Radius { get; set; }
        public static Point [] TriangleCoords { get; set; }
        public static Point RectTopLeft { get; set; }
        public static int RectWidth { get; set; }
        public static int RectHeight { get; set; }

        public static void clearAll() //sets all class flags as false to indicate,
            //that currently class does not contain any valid data  
        {
            HaveRect = HaveCircle = HaveTriangle = false;
        }
    }
}
