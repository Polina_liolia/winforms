﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RunEdit
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Up:
                    {
                        if (!noWayUp())
                            txt_Edit.Top -= 1;
                    } break;
                case Keys.Down:
                    {
                        if (!noWayDown())
                            txt_Edit.Top += 1;
                    } break;
                case Keys.Left:
                    {
                        if (!noWayLeft())
                            txt_Edit.Left -= 1;
                    } break;
                case Keys.Right:
                    {
                        if (!noWayRight())
                            txt_Edit.Left += 1;
                    }break;
            }
        }

        private bool noWayRight()
        {
            return (txt_Edit.Location.X + txt_Edit.Width + 1) > this.ClientSize.Width;
        }

        private bool noWayLeft()
        {
            return (txt_Edit.Location.X + -1 ) < 0;
        }

        private bool noWayUp()
        {
            return (txt_Edit.Location.Y - 1) < 0;
        }

        private bool noWayDown()
        {
            return (txt_Edit.Location.Y + txt_Edit.Height + 1) > this.ClientSize.Height;
        }


    }
}
