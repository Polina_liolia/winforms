﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SexStateTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void sexState_sexChanged(object sender, EventArgs e)
        {
            if (sexState.Sex)
                MessageBox.Show("Male");
            else
                MessageBox.Show("Female");
        }
    }
}
