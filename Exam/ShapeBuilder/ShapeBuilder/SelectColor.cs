﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeBuilder
{
    public partial class SelectColor : Form
    {
        private Color colorSelected;
        private Rectangle testColor;
        public SelectColor()
        {
            InitializeComponent();
            txt_Red.DataBindings.Add(new Binding("Text", trackB_Red, "Value"));
            txt_Green.DataBindings.Add(new Binding("Text", trackB_Green, "Value"));
            txt_Blue.DataBindings.Add(new Binding("Text", trackB_Blue, "Value"));
           
        }

        private void SelectColor_Load(object sender, EventArgs e)
        {
            testColor = new Rectangle(new Point(190, 170), new Size(90, 70));
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawRectangle(Pens.Black, testColor);
        }

        public Color ColorSelected
        {
            get
            {
                return colorSelected;
            }
        }

        private void btn_SelectColor_Click(object sender, EventArgs e)
        {
            colorSelected = Color.FromArgb(trackB_Red.Value, trackB_Green.Value, trackB_Blue.Value);
            this.Hide();
        }

        private void colorChanging(object sender, EventArgs e)
        {
            Color currentColor = Color.FromArgb(trackB_Red.Value, trackB_Green.Value, trackB_Blue.Value);
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.FillRectangle(new SolidBrush(currentColor), testColor);
        }

       
    }
}
