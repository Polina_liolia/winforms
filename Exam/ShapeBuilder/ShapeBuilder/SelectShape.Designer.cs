﻿namespace ShapeBuilder
{
    partial class SelectShape
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_SelectShape = new System.Windows.Forms.Button();
            this.gBox_Shapes = new System.Windows.Forms.GroupBox();
            this.rb_Circle = new System.Windows.Forms.RadioButton();
            this.rb_Triangle = new System.Windows.Forms.RadioButton();
            this.rb_Rectangle = new System.Windows.Forms.RadioButton();
            this.gBox_Shapes.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_SelectShape
            // 
            this.btn_SelectShape.Location = new System.Drawing.Point(12, 122);
            this.btn_SelectShape.Name = "btn_SelectShape";
            this.btn_SelectShape.Size = new System.Drawing.Size(108, 23);
            this.btn_SelectShape.TabIndex = 3;
            this.btn_SelectShape.Text = "Select Shape";
            this.btn_SelectShape.UseVisualStyleBackColor = true;
            this.btn_SelectShape.Click += new System.EventHandler(this.btn_SelectShape_Click);
            // 
            // gBox_Shapes
            // 
            this.gBox_Shapes.Controls.Add(this.rb_Circle);
            this.gBox_Shapes.Controls.Add(this.rb_Triangle);
            this.gBox_Shapes.Controls.Add(this.rb_Rectangle);
            this.gBox_Shapes.Location = new System.Drawing.Point(12, 12);
            this.gBox_Shapes.Name = "gBox_Shapes";
            this.gBox_Shapes.Size = new System.Drawing.Size(108, 95);
            this.gBox_Shapes.TabIndex = 2;
            this.gBox_Shapes.TabStop = false;
            this.gBox_Shapes.Text = "Shapes";
            // 
            // rb_Circle
            // 
            this.rb_Circle.AutoSize = true;
            this.rb_Circle.Location = new System.Drawing.Point(7, 68);
            this.rb_Circle.Name = "rb_Circle";
            this.rb_Circle.Size = new System.Drawing.Size(51, 17);
            this.rb_Circle.TabIndex = 2;
            this.rb_Circle.TabStop = true;
            this.rb_Circle.Text = "Circle";
            this.rb_Circle.UseVisualStyleBackColor = true;
            this.rb_Circle.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rb_Triangle
            // 
            this.rb_Triangle.AutoSize = true;
            this.rb_Triangle.Location = new System.Drawing.Point(7, 44);
            this.rb_Triangle.Name = "rb_Triangle";
            this.rb_Triangle.Size = new System.Drawing.Size(63, 17);
            this.rb_Triangle.TabIndex = 1;
            this.rb_Triangle.TabStop = true;
            this.rb_Triangle.Text = "Triangle";
            this.rb_Triangle.UseVisualStyleBackColor = true;
            this.rb_Triangle.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rb_Rectangle
            // 
            this.rb_Rectangle.AutoSize = true;
            this.rb_Rectangle.Location = new System.Drawing.Point(7, 20);
            this.rb_Rectangle.Name = "rb_Rectangle";
            this.rb_Rectangle.Size = new System.Drawing.Size(74, 17);
            this.rb_Rectangle.TabIndex = 0;
            this.rb_Rectangle.TabStop = true;
            this.rb_Rectangle.Text = "Rectangle";
            this.rb_Rectangle.UseVisualStyleBackColor = true;
            this.rb_Rectangle.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // SelectShape
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(201, 178);
            this.Controls.Add(this.btn_SelectShape);
            this.Controls.Add(this.gBox_Shapes);
            this.Name = "SelectShape";
            this.Text = "SelectShape";
            this.gBox_Shapes.ResumeLayout(false);
            this.gBox_Shapes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_SelectShape;
        private System.Windows.Forms.GroupBox gBox_Shapes;
        private System.Windows.Forms.RadioButton rb_Circle;
        private System.Windows.Forms.RadioButton rb_Triangle;
        private System.Windows.Forms.RadioButton rb_Rectangle;
    }
}