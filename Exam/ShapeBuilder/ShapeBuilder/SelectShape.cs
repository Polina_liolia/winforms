﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeBuilder
{
    public enum Shapes { none, Rectangle, Triangle, Circle}
    public partial class SelectShape : Form
    {

        private Shapes shapeSelected = Shapes.none;

        
        public SelectShape()
        {
            InitializeComponent();
        }

        public Shapes ShapeSelected
        {
            get
            {
                return shapeSelected;
            }
        }

        //changing ShapeSelected prop
        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                if (rb != null)
                {
                    shapeSelected = rb == this.rb_Rectangle ? Shapes.Rectangle :
                        rb == this.rb_Triangle ? Shapes.Triangle :
                        rb == this.rb_Circle ? Shapes.Circle : Shapes.none;
                }
            }
            
        }

        private void btn_SelectShape_Click(object sender, EventArgs e)
        {
            if (!rb_Circle.Checked && !rb_Rectangle.Checked && !rb_Triangle.Checked)
                MessageBox.Show("Error", "No shape was selected.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                this.Hide();
        }
    }
}
