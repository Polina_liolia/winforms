﻿namespace TicTacToeMVC
{
    public interface IView
    {
        void sendGameOver(string msg);
        void clearField();
        void createView();
    }
}