﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeMVC
{
    //обертка над матрицей. Ответственность за поиск "линий" одинаковых значений 
    class TicTacToeModel
    {
        private string[,] matrix;
        public static readonly string XValue = "X";
        public static readonly string OValue = "O";
        public static readonly string EmptyValue = "";
        public TicTacToeModel()
        {
            matrix = new string[3, 3] {
                {EmptyValue, EmptyValue, EmptyValue },
                {EmptyValue, EmptyValue, EmptyValue },
                {EmptyValue, EmptyValue, EmptyValue }
            };
        }
        public bool haveLine(string value)
        {
            if (
                    (matrix[0, 0] == value && matrix[0, 1] == value && matrix[0, 2] == value) ||
                    (matrix[1, 0] == value && matrix[1, 1] == value && matrix[1, 2] == value) ||
                    (matrix[2, 0] == value && matrix[2, 1] == value && matrix[2, 2] == value) ||
                    (matrix[0, 0] == value && matrix[1, 0] == value && matrix[2, 0] == value) ||
                    (matrix[0, 1] == value && matrix[1, 1] == value && matrix[2, 1] == value) ||
                    (matrix[0, 2] == value && matrix[1, 2] == value && matrix[2, 2] == value) ||
                    (matrix[0, 0] == value && matrix[1, 1] == value && matrix[2, 2] == value) ||
                    (matrix[0, 2] == value && matrix[1, 1] == value && matrix[2, 0] == value)
                )
                return true;
            return false;
        }

        public bool haveValue(int i, int j)
        {
            return matrix[i, j] != TicTacToeModel.EmptyValue;
        }

        public bool setValue (int i, int j, string val)
        {
            if (matrix[i, j] == TicTacToeModel.EmptyValue &&
                val == TicTacToeModel.XValue || val == TicTacToeModel.OValue)
            {
                matrix[i, j] = val;
                return true;
            }
            return false;
        }

        public void clearMatrix ()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    matrix[i, j] = TicTacToeModel.EmptyValue;
        }
    }
}
