﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    public partial class View3 : Form, IView
    {
        TicTacToeController controller;
        PictureBox[,] pictures = new PictureBox[3, 3] {
            {
                new PictureBox(),
                new PictureBox(),
                new PictureBox()
            },
            {
                new PictureBox(),
                new PictureBox(),
                new PictureBox()
            },
            {
                new PictureBox(),
                new PictureBox(),
                new PictureBox()
            }
        };

        public View3()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }

        public void createView()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    pictures[i, j].Name = string.Format("pBox_{0}{1}", j, i);
                    pictures[i, j].Size = new Size(150, 150);
                    pictures[i, j].Location = new Point(5+(150+10)*j, 5+(150+10)*i);
                    pictures[i, j].TabIndex = (i+1)*j;
                    pictures[i, j].TabStop = false;
                    pictures[i, j].Click += View3_Click;
                    Controls.Add(pictures[i, j]);
                }
        }

        private void View3_Click(object sender, EventArgs e)
        {
            if (sender is PictureBox)
            {
                PictureBox pBox = sender as PictureBox;
                if (pBox != null)
                {
                    int iIndex, jIndex;
                    bool found = getPicturePosition(pBox, out iIndex, out jIndex);
                    if (found)
                    {
                        string viewValue = string.Empty;
                        bool result = controller.sendPosition(iIndex, jIndex, out viewValue);
                        if (result)
                            pictures[iIndex, jIndex].Image = viewValue == "X" ? Image.FromFile(@"../../img/X.jpg") :
                                Image.FromFile(@"../../img/O.png");
                    }
                }
            }
        }

        private bool getPicturePosition(PictureBox pictureBox, out int iIndex, out int jIndex)
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    if (pictures[i, j] == pictureBox)
                    {
                        jIndex = j;
                        iIndex = i;
                        return true;
                    }
                }
            iIndex = -1;
            jIndex = -1;
            return false;
        }

        public void clearField()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    pictures[i, j].Image = null;
                }
        }

        public void sendGameOver(string msg)
        {
            MessageBox.Show(msg,
               "Резльтат игры",
               MessageBoxButtons.OK,
               MessageBoxIcon.Information);
        }

        private void View3_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawLine(Pens.Black, new Point(160, 5), new Point(160, 470));
            gr.DrawLine(Pens.Black, new Point(320, 5), new Point(320, 470));
            gr.DrawLine(Pens.Black, new Point(5, 160), new Point(470, 160));
            gr.DrawLine(Pens.Black, new Point(5, 320), new Point(470, 320));
        }
    }
}
