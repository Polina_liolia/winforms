﻿namespace ClientView
{
    partial class ClientView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBox_clientInfo = new System.Windows.Forms.GroupBox();
            this.lbl_name = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_Age = new System.Windows.Forms.TextBox();
            this.txt_region = new System.Windows.Forms.TextBox();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.grBox_clientInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // grBox_clientInfo
            // 
            this.grBox_clientInfo.Controls.Add(this.txt_email);
            this.grBox_clientInfo.Controls.Add(this.txt_phone);
            this.grBox_clientInfo.Controls.Add(this.txt_region);
            this.grBox_clientInfo.Controls.Add(this.txt_Age);
            this.grBox_clientInfo.Controls.Add(this.txt_name);
            this.grBox_clientInfo.Controls.Add(this.label4);
            this.grBox_clientInfo.Controls.Add(this.label3);
            this.grBox_clientInfo.Controls.Add(this.label2);
            this.grBox_clientInfo.Controls.Add(this.label1);
            this.grBox_clientInfo.Controls.Add(this.lbl_name);
            this.grBox_clientInfo.Location = new System.Drawing.Point(3, 3);
            this.grBox_clientInfo.Name = "grBox_clientInfo";
            this.grBox_clientInfo.Size = new System.Drawing.Size(403, 173);
            this.grBox_clientInfo.TabIndex = 0;
            this.grBox_clientInfo.TabStop = false;
            this.grBox_clientInfo.Text = "Client\'s info";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(7, 24);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(35, 13);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Age";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Region";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Phone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "E-mail";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(68, 24);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(312, 20);
            this.txt_name.TabIndex = 5;
            // 
            // txt_Age
            // 
            this.txt_Age.Location = new System.Drawing.Point(68, 53);
            this.txt_Age.Name = "txt_Age";
            this.txt_Age.Size = new System.Drawing.Size(312, 20);
            this.txt_Age.TabIndex = 6;
            // 
            // txt_region
            // 
            this.txt_region.Location = new System.Drawing.Point(68, 81);
            this.txt_region.Name = "txt_region";
            this.txt_region.Size = new System.Drawing.Size(312, 20);
            this.txt_region.TabIndex = 7;
            // 
            // txt_phone
            // 
            this.txt_phone.Location = new System.Drawing.Point(68, 109);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(312, 20);
            this.txt_phone.TabIndex = 8;
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(68, 136);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(312, 20);
            this.txt_email.TabIndex = 9;
            // 
            // ClientView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grBox_clientInfo);
            this.Name = "ClientView";
            this.Size = new System.Drawing.Size(414, 185);
            this.grBox_clientInfo.ResumeLayout(false);
            this.grBox_clientInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBox_clientInfo;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.TextBox txt_region;
        private System.Windows.Forms.TextBox txt_Age;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_name;
    }
}
