﻿namespace ClientViewTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientView_addClient = new ClientView.ClientView();
            this.btn_addClient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // clientView_addClient
            // 
            this.clientView_addClient.ClientAge = 0;
            this.clientView_addClient.ClientName = "";
            this.clientView_addClient.ClientsMail = "";
            this.clientView_addClient.ClientsPhone = "";
            this.clientView_addClient.Location = new System.Drawing.Point(13, 13);
            this.clientView_addClient.Name = "clientView_addClient";
            this.clientView_addClient.Region_info = "";
            this.clientView_addClient.Size = new System.Drawing.Size(415, 186);
            this.clientView_addClient.TabIndex = 0;
            // 
            // btn_addClient
            // 
            this.btn_addClient.Location = new System.Drawing.Point(341, 205);
            this.btn_addClient.Name = "btn_addClient";
            this.btn_addClient.Size = new System.Drawing.Size(75, 23);
            this.btn_addClient.TabIndex = 1;
            this.btn_addClient.Text = "Add client";
            this.btn_addClient.UseVisualStyleBackColor = true;
            this.btn_addClient.Click += new System.EventHandler(this.btn_addClient_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 435);
            this.Controls.Add(this.btn_addClient);
            this.Controls.Add(this.clientView_addClient);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ClientView.ClientView clientView_addClient;
        private System.Windows.Forms.Button btn_addClient;
    }
}

