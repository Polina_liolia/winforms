﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyObjectsLib;

namespace ClientViewTest
{
    public partial class MainForm : Form
    {
        List<Client> clients = new List<Client>();
        public MainForm()
        {
            InitializeComponent();
        }

        private void btn_addClient_Click(object sender, EventArgs e)
        {
            string name = clientView_addClient.ClientName;
            int age = clientView_addClient.ClientAge;
            string region = clientView_addClient.Region_info;
            string phone = clientView_addClient.ClientsPhone;
            string mail = clientView_addClient.ClientsMail;

            if (name.Length == 0 || age <= 0 || region.Length == 0 || phone.Length == 0 || mail.Length == 0)
                MessageBox.Show("Not all fields were filled in.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                clients.Add(new Client(name, age, region, phone, mail));
                MessageBox.Show(string.Format("Client {0} was successfully added!", name), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
                
        }
    }
}
