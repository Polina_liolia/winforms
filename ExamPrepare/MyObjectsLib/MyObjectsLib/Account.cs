﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MyObjectsLib
{
    public class Account : IEquatable<Account>, IComparable, IComparer<Account>, ICloneable, INotifyPropertyChanged
    {
        Client client;
        double account;
        string account_number;

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Constructors
        private Account() { }
        public Account(Client client, double account, string account_number)
        {
            this.client = client;
            this.account = account;
            this.account_number = account_number;
        }
        #endregion

        #region Public properties
        public Client Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
                NotifyPropertyChanged("Client");
            }
        }

        public double Account_
        {
            get
            {
                return account;
            }

            set
            {
                account = value;
                NotifyPropertyChanged("Account_");
            }
        }

        public string Account_number
        {
            get
            {
                return account_number;
            }

            set
            {
                account_number = value;
                NotifyPropertyChanged("Account_number");
            }
        }
        #endregion

        public void Put(double sum)
        {
            account += sum;
            NotifyPropertyChanged("Account_");
        }

        public bool Withdraw(double sum)
        {
            if (sum <= account)
            {
                account -= sum;
                NotifyPropertyChanged("Account_");
                return true;
            }
            return false; // not enough money
        }

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Account number: {0}, Client name: {1}, Sum: {2}",
                account_number, client.Clients_name, account);
        }
        public override int GetHashCode()
        {
            return account_number.GetHashCode();
        }

        #endregion

        #region Operators overloaded
        public static bool operator ==(Account account1, Account account2)
        {
            if (((object)account1) == null || ((object)account2) == null)
                return Object.Equals(account1, account2);
            return account1.Equals(account2);
        }
        public static bool operator !=(Account account1, Account account2)
        {
            if (((object)account1) == null || ((object)account2) == null)
                return Object.Equals(account1, account2);
            return !account1.Equals(account2);
        }

        public static bool operator <(Account account1, Account account2)
        {
            if (((object)account1) == null && ((object)account2) != null)
                return true;
            if (((object)account1) != null && ((object)account2) == null ||
                ((object)account1) == null && ((object)account2) == null)
                return false;
            return account1.account_number.CompareTo(account2.account_number) < 0 ? true : false;
        }

        public static bool operator >(Account account1, Account account2)
        {
            if (((object)account1) == null && ((object)account2) != null ||
                ((object)account1) == null && ((object)account2) == null)
                return false;
            if (((object)account1) != null && ((object)account2) == null)
                return true;
            return account1.account_number.CompareTo(account2.account_number) > 0 ? true : false;
        }
        #endregion

        #region IEquatable<Account>
        public bool Equals(Account other)
        {
            if (((object)other) == null)
                return object.Equals(this, other);
            return this.account_number.Equals(other.account_number);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return Object.Equals(this, obj);
            Account other = obj as Account;
            if (other == null)
                return false;
            return this.Equals(other);
        }
        #endregion

        #region IComparable
        public int CompareTo(Account other)
        {
            if (((object)other) == null)
                return 1;
            return account_number.CompareTo(other.account_number);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Account other = obj as Account;
            if (other == null)
                throw new ArgumentException("Argument is not an Account instance");
            return this.CompareTo(other);
        }
        #endregion

        #region IComparer<Account>
        public int Compare(Account x, Account y)
        {
            return x.CompareTo(y);
        }
        #endregion

        #region ICloneable
        public object Clone()
        {
            Account clone = (Account)this.MemberwiseClone();
            clone.account_number = string.Copy(account_number);
            clone.client = new Client(client.Clients_name, client.Age, client.Region_info,
                client.Clients_phone, client.Clients_mail);
            foreach (Account ac in client.Accounts)
                clone.client.Accounts.Add(ac);
            return clone;
        }
        #endregion
    }
}
