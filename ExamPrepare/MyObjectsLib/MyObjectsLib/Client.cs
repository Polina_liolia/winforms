﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjectsLib
{
    public class Client : IEquatable<Client>, IComparable, IComparer<Client>, ICloneable, INotifyPropertyChanged
    {
        
        string clients_name;
        int age;
        string region_info;
        string clients_phone;
        string clients_mail;
        List<Account> accounts;
        bool sex;

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Properties
        public string Clients_name
        {
            get
            {
                return clients_name;
            }

            set
            {
                clients_name = value;
                NotifyPropertyChanged("Clients_name");
            }
        }

        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
                NotifyPropertyChanged("Age");
            }
        }

        public string Region_info
        {
            get
            {
                return region_info;
            }

            set
            {
                region_info = value;
                NotifyPropertyChanged("Region_info");
            }
        }

        public string Clients_phone
        {
            get
            {
                return clients_phone;
            }

            set
            {
                clients_phone = value;
                NotifyPropertyChanged("Clients_phone");
            }
        }

        public string Clients_mail
        {
            get
            {
                return clients_mail;
            }

            set
            {
                clients_mail = value;
                NotifyPropertyChanged("Clients_mail");
            }
        }

        public bool Sex
        {
            get
            {
                return sex;
            }
            set
            {
                sex = value;
                NotifyPropertyChanged("Sex");
            }
        }

        public List<Account> Accounts
        {
            get
            {
                return accounts;
            }

            set
            {
                accounts = value;
                NotifyPropertyChanged("Accounts");
            }
        }

        #endregion

        #region Constructors
        protected Client() { }
        public Client(string clients_name,
                        int age,
                        string region_info,
                        string clients_phone,
                        string clients_mail,
                        bool sex = true)
        {
            this.Clients_name = clients_name;
            this.Age = age;
            this.Region_info = region_info;
            this.Clients_phone = clients_phone;
            this.Clients_mail = clients_mail;
            this.Accounts = new List<Account>();
            this.sex = sex;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("name: {0}, age: {1}, phone: {2}, e-mail: {3}",
                this.Clients_name, this.Age, this.Clients_phone, this.Clients_mail);
        }

        public override int GetHashCode()
        {
            return this.clients_mail.GetHashCode(); 
        }
        #endregion

        #region Operators overloaded
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return Object.Equals(client1, client2);
            return client1.Equals(client2);
        }
        public static bool operator !=(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return !Object.Equals(client1, client2);

            return !(client1.Equals(client2));
        }

        //операторы сравнения могут использовать свою логику для сравнения, в зависимости о  задачи
        public static bool operator <(Client person1, Client person2)
        {
            if (((object)person1) == null && ((object)person2) != null)
                return true;
            if (((object)person1) != null && ((object)person2) == null ||
                ((object)person1) == null && ((object)person2) == null)
                return false;
            return person1.clients_mail.CompareTo(person2.clients_mail) < 0 ? true : false;
        }

        public static bool operator >(Client person1, Client person2)
        {
            if (((object)person1) == null && ((object)person2) != null ||
                ((object)person1) == null && ((object)person2) == null)
                return false;
            if (((object)person1) != null && ((object)person2) == null)
                return true;
            return person1.clients_mail.CompareTo(person2.clients_mail) > 0 ? true : false;
        }
        #endregion

        #region Equals
        //операторы равенства/неравенства должны быть реализованы с помощью принятых ранее реализаций Equals, чтобы сохранить единую логику
        public bool Equals(Client other)//реализация интерфейса IEquatable
        {
            if ((object)other == null)
                return false;
            return (this.clients_mail.Equals(other.clients_mail));
        }
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            Client clientObj = obj as Client;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);
        }
        #endregion

        #region Clone
        public object Clone()
        {
            Client clone = (Client)this.MemberwiseClone();
            clone.clients_name = string.Copy(clients_name);
            clone.Clients_phone = string.Copy(clients_phone);
            clone.clients_mail = string.Copy(clients_mail);
            clone.region_info = string.Copy(region_info);
            clone.accounts = new List<Account>();
            foreach (Account a in accounts)
                clone.accounts.Add(a);
            return clone;
        }
        #endregion

        #region IComparable
        public int CompareTo(Client other)
        {
            if (((object)other) == null)
                return 1;
            return this.clients_mail.CompareTo(other.clients_mail);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Client other_client = obj as Client;
            if (other_client != null) 
                return this.CompareTo(other_client);
            else
                throw new ArgumentException("Object is not a Client");
        }
        #endregion

        #region IComparer
        public int Compare(Client x, Client y)
        {
            if (x == null && y != null)
                return -1;
            if (y == null && x != null)
                return 1;
            return x.CompareTo(y);
        }
        #endregion

        /*
        //order events handler:
        public void getOrderInfo(object sender, OrderEventArgs e)
        {
            Order order = sender as Order;
            if (order == null)
                Console.WriteLine(e.Message);
            else
                Console.WriteLine(string.Format("{0}, Client email: {1}; order position price: {2}, total order sum: {3} ",
                    e.Message, this.Clients_mail, e.Order_position.Price, order.Total_costs));
        }*/
    }
}

