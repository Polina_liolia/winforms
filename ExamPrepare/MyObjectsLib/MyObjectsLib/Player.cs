﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyObjectsLib
{
    public class Player : INotifyPropertyChanged
    {
        private string name;
        public string PlayerName
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("PlayerName");
            }
        }
        private int age;
        public int Age
        {
            get { return age; }
            set
            {
                age = value;
                NotifyPropertyChanged("Age");
            }
        }
        public Player(string name, int age)
        {
            PlayerName = name;
            Age = age;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion
    }
}
