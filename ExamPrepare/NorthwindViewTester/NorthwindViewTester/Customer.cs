﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindViewTester
{
    class Customer
    {
        public string CustomerID { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }


        private Customer() { }

        public Customer(string customerID, string companyName)
        {
            CustomerID = customerID;
            CompanyName = companyName;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", CustomerID, CompanyName);
        }
    }
}
