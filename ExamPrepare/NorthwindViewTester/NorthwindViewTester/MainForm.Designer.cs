﻿namespace NorthwindViewTester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabDBView = new System.Windows.Forms.TabControl();
            this.tPage_Employees = new System.Windows.Forms.TabPage();
            this.dgv_Employees = new System.Windows.Forms.DataGridView();
            this.tPage_Customers = new System.Windows.Forms.TabPage();
            this.lv_Customers = new System.Windows.Forms.ListView();
            this.tPage_Categories = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_Description = new System.Windows.Forms.ComboBox();
            this.cb_CategoryName = new System.Windows.Forms.ComboBox();
            this.cb_CategoryID = new System.Windows.Forms.ComboBox();
            this.cBox_Picture = new System.Windows.Forms.ComboBox();
            this.tabDBView.SuspendLayout();
            this.tPage_Employees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Employees)).BeginInit();
            this.tPage_Customers.SuspendLayout();
            this.tPage_Categories.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabDBView
            // 
            this.tabDBView.Controls.Add(this.tPage_Employees);
            this.tabDBView.Controls.Add(this.tPage_Customers);
            this.tabDBView.Controls.Add(this.tPage_Categories);
            this.tabDBView.Location = new System.Drawing.Point(12, 12);
            this.tabDBView.Name = "tabDBView";
            this.tabDBView.SelectedIndex = 0;
            this.tabDBView.Size = new System.Drawing.Size(1163, 579);
            this.tabDBView.TabIndex = 0;
            // 
            // tPage_Employees
            // 
            this.tPage_Employees.Controls.Add(this.dgv_Employees);
            this.tPage_Employees.Location = new System.Drawing.Point(4, 22);
            this.tPage_Employees.Name = "tPage_Employees";
            this.tPage_Employees.Padding = new System.Windows.Forms.Padding(3);
            this.tPage_Employees.Size = new System.Drawing.Size(1155, 553);
            this.tPage_Employees.TabIndex = 0;
            this.tPage_Employees.Text = "Employees";
            this.tPage_Employees.UseVisualStyleBackColor = true;
            // 
            // dgv_Employees
            // 
            this.dgv_Employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Employees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Employees.Location = new System.Drawing.Point(3, 3);
            this.dgv_Employees.Name = "dgv_Employees";
            this.dgv_Employees.Size = new System.Drawing.Size(1149, 547);
            this.dgv_Employees.TabIndex = 0;
            // 
            // tPage_Customers
            // 
            this.tPage_Customers.Controls.Add(this.lv_Customers);
            this.tPage_Customers.Location = new System.Drawing.Point(4, 22);
            this.tPage_Customers.Name = "tPage_Customers";
            this.tPage_Customers.Padding = new System.Windows.Forms.Padding(3);
            this.tPage_Customers.Size = new System.Drawing.Size(1155, 553);
            this.tPage_Customers.TabIndex = 1;
            this.tPage_Customers.Text = "Customers";
            this.tPage_Customers.UseVisualStyleBackColor = true;
            // 
            // lv_Customers
            // 
            this.lv_Customers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_Customers.Location = new System.Drawing.Point(3, 3);
            this.lv_Customers.Name = "lv_Customers";
            this.lv_Customers.Size = new System.Drawing.Size(1149, 547);
            this.lv_Customers.TabIndex = 0;
            this.lv_Customers.UseCompatibleStateImageBehavior = false;
            // 
            // tPage_Categories
            // 
            this.tPage_Categories.Controls.Add(this.cBox_Picture);
            this.tPage_Categories.Controls.Add(this.label4);
            this.tPage_Categories.Controls.Add(this.label3);
            this.tPage_Categories.Controls.Add(this.label2);
            this.tPage_Categories.Controls.Add(this.label1);
            this.tPage_Categories.Controls.Add(this.cb_Description);
            this.tPage_Categories.Controls.Add(this.cb_CategoryName);
            this.tPage_Categories.Controls.Add(this.cb_CategoryID);
            this.tPage_Categories.Location = new System.Drawing.Point(4, 22);
            this.tPage_Categories.Name = "tPage_Categories";
            this.tPage_Categories.Padding = new System.Windows.Forms.Padding(3);
            this.tPage_Categories.Size = new System.Drawing.Size(1155, 553);
            this.tPage_Categories.TabIndex = 2;
            this.tPage_Categories.Text = "Categories";
            this.tPage_Categories.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(669, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Picture";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(430, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Category Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Category ID";
            // 
            // cb_Description
            // 
            this.cb_Description.FormattingEnabled = true;
            this.cb_Description.Location = new System.Drawing.Point(430, 51);
            this.cb_Description.Name = "cb_Description";
            this.cb_Description.Size = new System.Drawing.Size(217, 21);
            this.cb_Description.TabIndex = 2;
            // 
            // cb_CategoryName
            // 
            this.cb_CategoryName.FormattingEnabled = true;
            this.cb_CategoryName.Location = new System.Drawing.Point(209, 51);
            this.cb_CategoryName.Name = "cb_CategoryName";
            this.cb_CategoryName.Size = new System.Drawing.Size(188, 21);
            this.cb_CategoryName.TabIndex = 1;
            // 
            // cb_CategoryID
            // 
            this.cb_CategoryID.FormattingEnabled = true;
            this.cb_CategoryID.Location = new System.Drawing.Point(19, 51);
            this.cb_CategoryID.Name = "cb_CategoryID";
            this.cb_CategoryID.Size = new System.Drawing.Size(172, 21);
            this.cb_CategoryID.TabIndex = 0;
            // 
            // cBox_Picture
            // 
            this.cBox_Picture.FormattingEnabled = true;
            this.cBox_Picture.Location = new System.Drawing.Point(672, 51);
            this.cBox_Picture.Name = "cBox_Picture";
            this.cBox_Picture.Size = new System.Drawing.Size(217, 21);
            this.cBox_Picture.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 601);
            this.Controls.Add(this.tabDBView);
            this.Name = "MainForm";
            this.Text = "Northwind DB";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabDBView.ResumeLayout(false);
            this.tPage_Employees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Employees)).EndInit();
            this.tPage_Customers.ResumeLayout(false);
            this.tPage_Categories.ResumeLayout(false);
            this.tPage_Categories.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabDBView;
        private System.Windows.Forms.TabPage tPage_Employees;
        private System.Windows.Forms.TabPage tPage_Customers;
        private System.Windows.Forms.TabPage tPage_Categories;
        private System.Windows.Forms.DataGridView dgv_Employees;
        private System.Windows.Forms.ListView lv_Customers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_Description;
        private System.Windows.Forms.ComboBox cb_CategoryName;
        private System.Windows.Forms.ComboBox cb_CategoryID;
        private System.Windows.Forms.ComboBox cBox_Picture;
    }
}

