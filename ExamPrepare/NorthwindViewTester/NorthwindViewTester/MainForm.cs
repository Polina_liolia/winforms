﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthwindViewTester
{
    public partial class MainForm : Form
    {
        private DataSet ds = new DataSet();
        private List<Customer> customers = new List<Customer>();
        public MainForm()
        {
            InitializeComponent();
        }

       

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region DataBase connection
            // string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=NORTHWIND.MDF;Integrated Security=True";
            string connectionString = @"Data Source=POLINA11-30\SQLEXPRESS;Initial Catalog=NORTHWIND;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("select * from[dbo].[EMPLOYEES];", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds, "EMPLOYEES");
                }
                using (SqlCommand command = new SqlCommand("select * from[dbo].[CATEGORIES];", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds, "CATEGORIES");
                }
                using (SqlCommand command = new SqlCommand("select * from[dbo].[CUSTOMERS];", con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string ID = reader["CustomerID"].ToString();
                            string companyName = reader["CompanyName"].ToString();
                            Customer customer = new Customer(ID, companyName);
                            customer.ContactName = reader["ContactName"].ToString();
                            customer.ContactTitle = reader["ContactTitle"].ToString();
                            customer.Address = reader["Address"].ToString();
                            customer.City = reader["City"].ToString();
                            customer.Region = reader["Region"].ToString();
                            customer.PostalCode = reader["PostalCode"].ToString();
                            customer.Country = reader["Country"].ToString();
                            customer.Phone = reader["Phone"].ToString();
                            customer.Fax = reader["Fax"].ToString();
                            customers.Add(customer);
                        }
                    }
                }
            }
            #endregion

            #region Employees DataGridView bindings
            dgv_EmployeesSettings();
            #endregion

            #region Customers listView populating  
            lv_CustomersSettings();
            lv_CustomersPopulate();
            #endregion

            #region Categories combo boxes bindings
            cb_CategoriesDataBindings();
            #endregion
        }

        private void cb_CategoriesDataBindings()
        {
            cb_CategoryID.DropDownStyle = ComboBoxStyle.DropDown;
            cb_CategoryID.AutoCompleteSource = AutoCompleteSource.ListItems;
            cb_CategoryID.AutoCompleteMode = AutoCompleteMode.Suggest;
            cb_CategoryID.DataSource = ds.Tables["CATEGORIES"];
            cb_CategoryID.DisplayMember = "CategoryID";

            cb_CategoryName.DropDownStyle = ComboBoxStyle.DropDown;
            cb_CategoryName.AutoCompleteSource = AutoCompleteSource.ListItems;
            cb_CategoryName.AutoCompleteMode = AutoCompleteMode.Suggest;
            cb_CategoryName.DataSource = ds.Tables["CATEGORIES"];
            cb_CategoryName.DisplayMember = "CategoryName";

            cb_Description.DropDownStyle = ComboBoxStyle.DropDown;
            cb_Description.AutoCompleteSource = AutoCompleteSource.ListItems;
            cb_Description.AutoCompleteMode = AutoCompleteMode.Suggest;
            cb_Description.DataSource = ds.Tables["CATEGORIES"];
            cb_Description.DisplayMember = "Description";

            cBox_Picture.DropDownStyle = ComboBoxStyle.DropDown;
            cBox_Picture.AutoCompleteSource = AutoCompleteSource.ListItems;
            cBox_Picture.AutoCompleteMode = AutoCompleteMode.Suggest;
            cBox_Picture.DataSource = ds.Tables["CATEGORIES"];
            cBox_Picture.DisplayMember = "Picture";
        }

        private void lv_CustomersSettings()
        {
            lv_Customers.View = View.Details; //table view
            lv_Customers.GridLines = true;
            lv_Customers.FullRowSelect = true;
            lv_Customers.MultiSelect = true;
            lv_Customers.Sorting = System.Windows.Forms.SortOrder.Ascending;
            lv_Customers.Columns.Add("CustomerID", 75);
            lv_Customers.Columns.Add("CompanyName", 150);
            lv_Customers.Columns.Add("ContactTitle", 100);
            lv_Customers.Columns.Add("ContactName", 100);
            lv_Customers.Columns.Add("Region", 75);
            lv_Customers.Columns.Add("Country", 100);
            lv_Customers.Columns.Add("PostalCode", 100);
            lv_Customers.Columns.Add("City", 100);
            lv_Customers.Columns.Add("Address", 100);
            lv_Customers.Columns.Add("Fax", 100);
            lv_Customers.Columns.Add("Phone", 100);
        }

        private void lv_CustomersPopulate()
        {
            foreach (Customer c in customers)
            {
                ListViewItem lv = new ListViewItem(c.CustomerID);
                lv.SubItems.Add(c.CompanyName);
                lv.SubItems.Add(c.ContactTitle);
                lv.SubItems.Add(c.ContactName);
                lv.SubItems.Add(c.Region);
                lv.SubItems.Add(c.Country);
                lv.SubItems.Add(c.PostalCode);
                lv.SubItems.Add(c.City);
                lv.SubItems.Add(c.Address);
                lv.SubItems.Add(c.Fax);
                lv.SubItems.Add(c.Phone);
                lv_Customers.Items.Add(lv);
            }
        }

        private void dgv_EmployeesSettings()
        {
            dgv_Employees.DataSource = ds.Tables["EMPLOYEES"];
            dgv_Employees.AllowUserToDeleteRows = false;
            dgv_Employees.AllowUserToAddRows = false;
            dgv_Employees.AllowUserToOrderColumns = true;
            dgv_Employees.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv_Employees.ReadOnly = true;
            dgv_Employees.RowHeadersVisible = false;
            dgv_Employees.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgv_Employees.RowTemplate.Resizable = DataGridViewTriState.False;
            dgv_Employees.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        
    }
}
