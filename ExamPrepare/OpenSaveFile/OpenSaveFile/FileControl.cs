﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace OpenSaveFile
{
    public partial class FileControl : UserControl
    {
        public FileControl()
        {
            InitializeComponent();
        }

        #region Обработка кликов
        public event EventHandler BtnClick; //"свое" событие - клик по кнопке (произвольное имя)
                                            //этот обработчик подчиняется четкому правилу именования OnИмяСобытия:
                                            //чтобы можно было программно присвоить обработчик только моей кнопке
        public void OnBtnClick(object sender, EventArgs e) 
        {
            //openFileDialog -> путь к файлу в поле Text
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open file";
            dlg.Filter = "All files (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
                txt_Path.Text = dlg.FileName;
            dlg.Dispose();
        }
        public void OnClick(object sender, EventArgs e) //клик по всему компоненту    
        {
            if (e == null)
                e = new EventArgs();
            if (BtnClick != null)
                BtnClick(sender, e);
            else //действие по умолчанию
                btn_OpenFile.Text = "Спасибо за ваш клик";
        }
        public void btnClick(object sender, EventArgs e) //клик именно по кнопке на созданном элементе
        {
            BtnClick?.Invoke(this, e);
        }
        #endregion

        #region "Глобальное" (для всего компонента) свойство Text
        [Browsable(true)] //свойство Text будет доступно в свойствах компонента
        public override string Text //перенаправляем текст текстбоксу внутри компонента
        {
            get
            {
                return txt_Path.Text;
            }

            set
            {
                txt_Path.Text = value;
            }
        }
        #endregion

        #region Отрисовка
        private void FileControl_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawEllipse(Pens.Azure, 364, 19, 25, 25);

            //дорисовать кнопке тень
            Rectangle shadow = new Rectangle(btn_OpenFile.Left + 10, btn_OpenFile.Top + 5, btn_OpenFile.Width, btn_OpenFile.Height);
           // gr.DrawRectangle(Pens.Gray, shadow);
            LinearGradientBrush myBrush = new LinearGradientBrush(shadow,  Color.FromArgb(120, Color.LightGray),Color.FromArgb(150, Color.Gray), LinearGradientMode.Horizontal);
            gr.FillRectangle(myBrush, shadow);
        }
        #endregion
    }
}
