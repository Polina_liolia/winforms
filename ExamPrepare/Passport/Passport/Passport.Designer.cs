﻿namespace Passport
{
    partial class Passport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_series = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_passportNumber = new System.Windows.Forms.TextBox();
            this.dt_issueDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // cb_series
            // 
            this.cb_series.FormattingEnabled = true;
            this.cb_series.Location = new System.Drawing.Point(54, 14);
            this.cb_series.Name = "cb_series";
            this.cb_series.Size = new System.Drawing.Size(58, 21);
            this.cb_series.TabIndex = 3;
            this.cb_series.Leave += new System.EventHandler(this.DataChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Серия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(127, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "№";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Выдан:";
            // 
            // txt_passportNumber
            // 
            this.txt_passportNumber.Location = new System.Drawing.Point(152, 14);
            this.txt_passportNumber.Name = "txt_passportNumber";
            this.txt_passportNumber.Size = new System.Drawing.Size(86, 20);
            this.txt_passportNumber.TabIndex = 4;
            this.txt_passportNumber.Leave += new System.EventHandler(this.txt_passportNumber_Leave);
            // 
            // dt_issueDate
            // 
            this.dt_issueDate.Location = new System.Drawing.Point(294, 14);
            this.dt_issueDate.Name = "dt_issueDate";
            this.dt_issueDate.Size = new System.Drawing.Size(151, 20);
            this.dt_issueDate.TabIndex = 5;
            this.dt_issueDate.ValueChanged += new System.EventHandler(this.DataChanged);
            this.dt_issueDate.Leave += new System.EventHandler(this.DataChanged);
            // 
            // Passport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dt_issueDate);
            this.Controls.Add(this.cb_series);
            this.Controls.Add(this.txt_passportNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Name = "Passport";
            this.Size = new System.Drawing.Size(454, 49);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_series;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_passportNumber;
        private System.Windows.Forms.DateTimePicker dt_issueDate;
    }
}
