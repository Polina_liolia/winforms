﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyObjectsLib;

namespace PersonClientWinForms
{
    public partial class MainForm : Form
    {
        private BindingList<Client> clients = new BindingList<Client>();
        private BindingList<string> regions = new BindingList<string>();
        private BindingList<string> mails = new BindingList<string>();

        public MainForm()
        {
            InitializeComponent();
           
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            lv_CustomersSettings();

            //initial items for filter comboBoxes:
            addUniqRegion("All regions");
            addUniqMail("All mails");

            //comboBoxes bindings:
            cbox_regionFilter.DataSource = regions;
            cBox_mailFilter.DataSource = mails;

            cbox_nameAge.DataSource = clients;
            cBoxRegionMail.DataSource = clients;


            //populating clients list:
            Client c = new Client("Vasilii", 25, "Ukraine", "+380506287421", "vasia@gmail.com");
            clients.Add(c);
            addUniqRegion(c.Region_info);
            addUniqMail(c.Clients_mail);
            lv_CustomersPopulate();            
        }

        private void lv_CustomersSettings()
        {
            lv_Clients.View = View.Details; //table view
            lv_Clients.GridLines = true;
            lv_Clients.FullRowSelect = true;
            lv_Clients.MultiSelect = true;
            lv_Clients.Sorting = System.Windows.Forms.SortOrder.Ascending;
            lv_Clients.Columns.Add("Name", 150);
            lv_Clients.Columns.Add("Age", 70);
            lv_Clients.Columns.Add("Region", 150);
            lv_Clients.Columns.Add("Phone", 100);
            lv_Clients.Columns.Add("Email", 100);   
        }

        private void lv_CustomersPopulate()
        {
            foreach (Client c in clients)
            {
                ListViewItem lv = new ListViewItem(c.Clients_name);
                lv.SubItems.Add(c.Age.ToString());
                lv.SubItems.Add(c.Region_info);
                lv.SubItems.Add(c.Clients_phone);
                lv.SubItems.Add(c.Clients_mail);
                lv_Clients.Items.Add(lv);
            }
        }

        //to avoid adding non-unique items to the region filter comboBox
        private bool addUniqRegion(string region)
        {
            if (regions.Contains(region) == false)
            {
                regions.Add(region);
                return true;
            }
            return false;
        }
        //to avoid adding non-unique items to the mail filter comboBox
        private bool addUniqMail(string mail)
        {
            if (mails.Contains(mail) == false)
            {
                mails.Add(mail);
                return true;
            }
            return false;
        }

        //fills list view with only clients, who have region and mail, choosen in filter comboBoxes
        private void filters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lv_Clients.Items.Clear();
            filterClients(cbox_regionFilter.SelectedItem.ToString(), cBox_mailFilter.SelectedItem.ToString());
        }

        //populates listView with only clients, who has pointed region and mail
        private void filterClients(string region, string mail)
        {
            
            foreach (Client c in clients)
            {
                if ((c.Region_info == region || region == "All regions") &&
                    (c.Clients_mail == mail || mail == "All mails"))
                {
                    ListViewItem lv = new ListViewItem(c.Clients_name);
                    lv.SubItems.Add(c.Age.ToString());
                    lv.SubItems.Add(c.Region_info);
                    lv.SubItems.Add(c.Clients_phone);
                    lv.SubItems.Add(c.Clients_mail);
                    lv_Clients.Items.Add(lv);
                }
            }
        }

        private void btn_addClient_Click(object sender, EventArgs e)
        {
            string name = clientView_addClient.ClientName;
            int age = clientView_addClient.ClientAge;
            string region = clientView_addClient.Region_info;
            string phone = clientView_addClient.ClientsPhone;
            string mail = clientView_addClient.ClientsMail;

            if (name.Length == 0 || age <= 0 || region.Length == 0 || phone.Length == 0 || mail.Length == 0)
                MessageBox.Show("Not all fields were filled in correctly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                addClient(name, age, region, phone, mail);
                
                MessageBox.Show(string.Format("Client {0} was successfully added!", name), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //clearing all text boxes:
                clientView_addClient.ClientName = "";
                clientView_addClient.ClientAge = 0;
                clientView_addClient.Region_info = "";
                clientView_addClient.ClientsPhone = "";
                clientView_addClient.ClientsMail = "";
            }
        }

        private void addClient(string name, int age, string region, string phone, string mail)
        {
            Client newClient = new Client(name, age, region, phone, mail);
            ListViewItem lv = new ListViewItem(newClient.Clients_name);
            lv.SubItems.Add(newClient.Age.ToString());
            lv.SubItems.Add(newClient.Region_info);
            lv.SubItems.Add(newClient.Clients_phone);
            lv.SubItems.Add(newClient.Clients_mail);
            lv_Clients.Items.Add(lv);
            clients.Add(newClient);
            addUniqRegion(newClient.Region_info);
            addUniqMail(newClient.Clients_mail);
        }

        //to display client's name and age in the same string of a comboBox
        private void cbox_nameAge_Format(object sender, ListControlConvertEventArgs e)
        {
            Client c = e.ListItem as Client;
            if (c != null)
            {
                e.Value = string.Format("{0}, {1} year(s)", c.Clients_name, c.Age);
            }
        }

        //to display client's region, mail and phone in the same string of a comboBox
        private void cBoxRegionMail_Format(object sender, ListControlConvertEventArgs e)
        {
            Client c = e.ListItem as Client;
            if (c != null)
            {
                e.Value = string.Format("{0}, e-mail: {1}, phone: {2}", c.Region_info, c.Clients_mail, c.Clients_phone);
            }
        }

        
    }
}
