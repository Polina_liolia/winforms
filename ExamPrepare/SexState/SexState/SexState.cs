﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SexState
{
    public partial class SexState : UserControl
    {
        public SexState()
        {
            InitializeComponent();
            rb_Male.Checked = true;
        }

        [Browsable(true)]
        public event EventHandler sexChanged;

        //true if Male, false if Female
        [Browsable(true)]
        public bool Sex
        {
            get
            {
                return rb_Male.Checked;
            }
            set
            {
                rb_Male.Checked = value;
            }
        }

        private void rb_CheckedChanged (object sender, EventArgs e)
        {
            if (e == null)
                e = new EventArgs();
            if (sexChanged != null)
                sexChanged(this, e);
        }
    }
}
