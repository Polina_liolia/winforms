﻿namespace SexStateTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sexState = new SexState.SexState();
            this.SuspendLayout();
            // 
            // sexState
            // 
            this.sexState.Location = new System.Drawing.Point(21, 21);
            this.sexState.Name = "sexState";
            this.sexState.Sex = true;
            this.sexState.Size = new System.Drawing.Size(94, 80);
            this.sexState.TabIndex = 0;
            this.sexState.sexChanged += new System.EventHandler(this.sexState_sexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.sexState);
            this.Name = "MainForm";
            this.Text = "Switcher";
            this.ResumeLayout(false);

        }

        #endregion

        private SexState.SexState sexState;
    }
}

