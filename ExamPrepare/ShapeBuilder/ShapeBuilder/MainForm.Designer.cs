﻿namespace ShapeBuilder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu_shapes = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_selectShape = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_SelectColor = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_userHelp = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_shapes});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(693, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu_shapes
            // 
            this.menu_shapes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_selectShape,
            this.menuItem_SelectColor});
            this.menu_shapes.Name = "menu_shapes";
            this.menu_shapes.Size = new System.Drawing.Size(50, 20);
            this.menu_shapes.Text = "Menu";
            // 
            // menuItem_selectShape
            // 
            this.menuItem_selectShape.Name = "menuItem_selectShape";
            this.menuItem_selectShape.Size = new System.Drawing.Size(169, 22);
            this.menuItem_selectShape.Text = "Select shape";
            this.menuItem_selectShape.Click += new System.EventHandler(this.menuItem_selectShape_Click);
            // 
            // menuItem_SelectColor
            // 
            this.menuItem_SelectColor.Name = "menuItem_SelectColor";
            this.menuItem_SelectColor.Size = new System.Drawing.Size(169, 22);
            this.menuItem_SelectColor.Text = "Select shape color";
            this.menuItem_SelectColor.Click += new System.EventHandler(this.menuItem_SelectColor_Click);
            // 
            // lbl_userHelp
            // 
            this.lbl_userHelp.AutoSize = true;
            this.lbl_userHelp.Location = new System.Drawing.Point(193, 24);
            this.lbl_userHelp.Name = "lbl_userHelp";
            this.lbl_userHelp.Size = new System.Drawing.Size(0, 13);
            this.lbl_userHelp.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 440);
            this.Controls.Add(this.lbl_userHelp);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Shape Builder";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseClick);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_shapes;
        private System.Windows.Forms.ToolStripMenuItem menuItem_selectShape;
        private System.Windows.Forms.ToolStripMenuItem menuItem_SelectColor;
        private System.Windows.Forms.Label lbl_userHelp;
    }
}

