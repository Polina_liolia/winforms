﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeBuilder
{
    public partial class MainForm : Form
    {
        private SelectShape selectShape = new SelectShape();
        private SelectColor selectColor = new SelectColor();
        private Shapes currentShape = Shapes.none;
        private Point[] clicks; //results of user's clicks
        private Color currentColor = Color.Empty;
        private bool fieldState = false;
        private Rectangle rectangle = new Rectangle();
        private Rectangle circle = new Rectangle();
        private Point[] triangle = new Point[3];


        public MainForm()
        {
            InitializeComponent();
            clicks = new Point[3] { new Point(-1,-1), new Point(-1, -1), new Point(-1, -1) };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //registering callbacks for FormClosing events of subforms:
            selectShape.VisibleChanged += (_sender, _e) =>
            {
                currentShape = selectShape.ShapeSelected;
                //indicating if everything is ready to build a new shape:
                check_fieldState();
            };

            selectColor.VisibleChanged += (_sender, _e) =>
            {
                currentColor = selectColor.ColorSelected;
                //indicating if everything is ready to build a new shape:
                check_fieldState();
            };
        }

        private void check_fieldState()
        {
            fieldState = (currentShape != Shapes.none && currentColor != Color.Empty) ? true : false;
            if (fieldState)
            {
                destroyShapes(); //to clear field
                string userMsg = currentShape == Shapes.Rectangle ?
                "Make two clicks to mark left top and right bottom corners of rectangle." :
                currentShape == Shapes.Triangle ? "Make three clicks to mark triangle's corners." :
                currentShape == Shapes.Circle ? "Make two clicks to mark the centre and the radius of a circle." : "";
                showHelpMsg(userMsg);
            }
        }

        //destroys all shapes, that could be built:
        private void destroyShapes()
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            rectangle.Size = new Size(0, 0);
            circle.Size = new Size(0, 0);
            triangle[0] = new Point(0, 0);
            triangle[1] = new Point(0, 0);
            triangle[2] = new Point(0, 0);

            clicks[0] = new Point(-1, -1);
            clicks[1] = new Point(-1, -1);
            clicks[2] = new Point(-1, -1);
            gr.DrawRectangle(Pens.Black, rectangle);
            gr.DrawEllipse(Pens.Black, circle);
            gr.DrawPolygon(Pens.Black, triangle);
            this.Invalidate();

        }

        //show shape builder user help
        private void showHelpMsg (string msg)
        {
            lbl_userHelp.Text = msg;
        }

        private void menuItem_selectShape_Click(object sender, EventArgs e)
        {
            selectShape.Show();
        }

        private void menuItem_SelectColor_Click(object sender, EventArgs e)
        {
            selectColor.Show();
        }

        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (!fieldState) //if user is not going to build a shape yet
                return;
            int clicks_needed = currentShape == Shapes.Triangle ? 3 : 2;
            bool click_saved = false;
            if (clicksLeft(clicks_needed))
            {
                for (int i = 0; i < clicks_needed; i++)
                {
                    if (clicks[i].X == -1 && clicks[i].Y == -1)
                    {
                        clicks[i] = e.Location;
                        click_saved = true;
                        break;
                    }
                }
            }
            if (click_saved)
            {
                if (clicksLeft(clicks_needed))
                    return;
                else
                    buildShape();
            }
            else
                buildShape();
        }

        private void buildShape()
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            if(currentShape == Shapes.Rectangle)
            {
                rectangle.Width = Math.Abs(clicks[0].X - clicks[1].X);
                rectangle.Height = Math.Abs(clicks[0].Y - clicks[1].Y);
                rectangle.Location = (clicks[0].Y < clicks[1].Y || clicks[0].X < clicks[1].X) ? clicks[0] : clicks[1];
                gr.DrawRectangle(Pens.Black, this.rectangle);
                gr.FillRectangle(new SolidBrush(currentColor), rectangle);
                showHelpMsg("");
                //hiding all other shapes:
                gr.DrawPolygon(Pens.Black, triangle);
                gr.DrawEllipse(Pens.Black, circle);
                return;
            }
            if (currentShape == Shapes.Circle)
            {
                circle.Location = new Point((clicks[0].X - Math.Abs(clicks[0].X - clicks[1].X)), (clicks[0].Y - Math.Abs(clicks[0].Y - clicks[1].Y)));
                circle.Width = circle.Height = Math.Abs(clicks[0].X - clicks[1].X) * 2;
                gr.DrawEllipse(Pens.Black, circle);
                gr.FillEllipse(new SolidBrush(currentColor), circle);
                showHelpMsg("");
                //hiding all other shapes:
                gr.DrawRectangle(Pens.Black, this.rectangle);
                gr.DrawPolygon(Pens.Black, triangle);
                return;
            }
            if (currentShape == Shapes.Triangle)
            {
                triangle[0] = clicks[0];
                triangle[1] = clicks[1];
                triangle[2] = clicks[2];
                gr.DrawPolygon(Pens.Black, triangle);
                gr.FillPolygon(new SolidBrush(currentColor), triangle);
                showHelpMsg("");
                //hiding all other shapes:
                gr.DrawRectangle(Pens.Black, this.rectangle);
                gr.DrawEllipse(Pens.Black, circle);
                return;
            }
        }

        private bool clicksLeft(int clicks_needed)
        {
            for (int i = 0; i < clicks_needed; i++)
            {
                if (clicks[i].X == -1 && clicks[i].Y == -1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
