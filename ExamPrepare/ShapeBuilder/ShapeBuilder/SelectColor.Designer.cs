﻿namespace ShapeBuilder
{
    partial class SelectColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackB_Red = new System.Windows.Forms.TrackBar();
            this.trackB_Green = new System.Windows.Forms.TrackBar();
            this.trackB_Blue = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_SelectColor = new System.Windows.Forms.Button();
            this.txt_Red = new System.Windows.Forms.TextBox();
            this.txt_Green = new System.Windows.Forms.TextBox();
            this.txt_Blue = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Blue)).BeginInit();
            this.SuspendLayout();
            // 
            // trackB_Red
            // 
            this.trackB_Red.Location = new System.Drawing.Point(64, 20);
            this.trackB_Red.Maximum = 255;
            this.trackB_Red.Name = "trackB_Red";
            this.trackB_Red.Size = new System.Drawing.Size(104, 45);
            this.trackB_Red.TabIndex = 0;
            this.trackB_Red.Scroll += new System.EventHandler(this.colorChanging);
            // 
            // trackB_Green
            // 
            this.trackB_Green.Location = new System.Drawing.Point(64, 71);
            this.trackB_Green.Maximum = 255;
            this.trackB_Green.Name = "trackB_Green";
            this.trackB_Green.Size = new System.Drawing.Size(104, 45);
            this.trackB_Green.TabIndex = 1;
            this.trackB_Green.Scroll += new System.EventHandler(this.colorChanging);
            // 
            // trackB_Blue
            // 
            this.trackB_Blue.Location = new System.Drawing.Point(64, 126);
            this.trackB_Blue.Maximum = 255;
            this.trackB_Blue.Name = "trackB_Blue";
            this.trackB_Blue.Size = new System.Drawing.Size(104, 45);
            this.trackB_Blue.TabIndex = 2;
            this.trackB_Blue.Scroll += new System.EventHandler(this.colorChanging);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Red";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Green";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Blue";
            // 
            // btn_SelectColor
            // 
            this.btn_SelectColor.Location = new System.Drawing.Point(13, 182);
            this.btn_SelectColor.Name = "btn_SelectColor";
            this.btn_SelectColor.Size = new System.Drawing.Size(75, 23);
            this.btn_SelectColor.TabIndex = 6;
            this.btn_SelectColor.Text = "Select color";
            this.btn_SelectColor.UseVisualStyleBackColor = true;
            this.btn_SelectColor.Click += new System.EventHandler(this.btn_SelectColor_Click);
            // 
            // txt_Red
            // 
            this.txt_Red.Location = new System.Drawing.Point(184, 20);
            this.txt_Red.Name = "txt_Red";
            this.txt_Red.Size = new System.Drawing.Size(100, 20);
            this.txt_Red.TabIndex = 7;
            // 
            // txt_Green
            // 
            this.txt_Green.Location = new System.Drawing.Point(184, 71);
            this.txt_Green.Name = "txt_Green";
            this.txt_Green.Size = new System.Drawing.Size(100, 20);
            this.txt_Green.TabIndex = 8;
            // 
            // txt_Blue
            // 
            this.txt_Blue.Location = new System.Drawing.Point(184, 126);
            this.txt_Blue.Name = "txt_Blue";
            this.txt_Blue.Size = new System.Drawing.Size(100, 20);
            this.txt_Blue.TabIndex = 9;
            // 
            // SelectColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 286);
            this.Controls.Add(this.txt_Blue);
            this.Controls.Add(this.txt_Green);
            this.Controls.Add(this.txt_Red);
            this.Controls.Add(this.btn_SelectColor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackB_Blue);
            this.Controls.Add(this.trackB_Green);
            this.Controls.Add(this.trackB_Red);
            this.Name = "SelectColor";
            this.Text = "SelectColor";
            this.Load += new System.EventHandler(this.SelectColor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB_Blue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackB_Red;
        private System.Windows.Forms.TrackBar trackB_Green;
        private System.Windows.Forms.TrackBar trackB_Blue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_SelectColor;
        private System.Windows.Forms.TextBox txt_Red;
        private System.Windows.Forms.TextBox txt_Green;
        private System.Windows.Forms.TextBox txt_Blue;
    }
}