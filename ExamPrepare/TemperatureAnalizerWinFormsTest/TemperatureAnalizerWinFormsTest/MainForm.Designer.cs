﻿namespace TemperatureAnalizerWinFormsTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fc_OpenFile = new OpenSaveFile.FileControl();
            this.lb_temperatures = new System.Windows.Forms.ListBox();
            this.fc_CheckTemp = new OpenSaveFile.FileControl();
            this.chlb_Options = new System.Windows.Forms.CheckedListBox();
            this.btn_check = new System.Windows.Forms.Button();
            this.txt_accuracy = new System.Windows.Forms.TextBox();
            this.lbl_accuracy = new System.Windows.Forms.Label();
            this.lbl_selectFile = new System.Windows.Forms.Label();
            this.lbl_TemperatureToFind = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fc_OpenFile
            // 
            this.fc_OpenFile.Location = new System.Drawing.Point(38, 25);
            this.fc_OpenFile.Name = "fc_OpenFile";
            this.fc_OpenFile.Size = new System.Drawing.Size(476, 57);
            this.fc_OpenFile.TabIndex = 0;
            this.fc_OpenFile.BtnClick += new System.EventHandler(this.ucClickOpenFile);
            // 
            // lb_temperatures
            // 
            this.lb_temperatures.FormattingEnabled = true;
            this.lb_temperatures.Location = new System.Drawing.Point(42, 88);
            this.lb_temperatures.Name = "lb_temperatures";
            this.lb_temperatures.Size = new System.Drawing.Size(173, 225);
            this.lb_temperatures.TabIndex = 1;
            // 
            // fc_CheckTemp
            // 
            this.fc_CheckTemp.Location = new System.Drawing.Point(38, 344);
            this.fc_CheckTemp.Name = "fc_CheckTemp";
            this.fc_CheckTemp.Size = new System.Drawing.Size(476, 62);
            this.fc_CheckTemp.TabIndex = 2;
            this.fc_CheckTemp.BtnClick += new System.EventHandler(this.fc_CheckTemp_BtnClick);
            // 
            // chlb_Options
            // 
            this.chlb_Options.ColumnWidth = 300;
            this.chlb_Options.FormattingEnabled = true;
            this.chlb_Options.Items.AddRange(new object[] {
            "find max temperature",
            "find min temperature",
            "find popular temperature",
            "check if stable temperatures interval exists"});
            this.chlb_Options.Location = new System.Drawing.Point(281, 130);
            this.chlb_Options.MultiColumn = true;
            this.chlb_Options.Name = "chlb_Options";
            this.chlb_Options.Size = new System.Drawing.Size(233, 79);
            this.chlb_Options.TabIndex = 3;
            // 
            // btn_check
            // 
            this.btn_check.Location = new System.Drawing.Point(439, 215);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(75, 23);
            this.btn_check.TabIndex = 4;
            this.btn_check.Text = "Check it!";
            this.btn_check.UseVisualStyleBackColor = true;
            this.btn_check.Click += new System.EventHandler(this.btn_check_Click);
            // 
            // txt_accuracy
            // 
            this.txt_accuracy.Location = new System.Drawing.Point(339, 92);
            this.txt_accuracy.Name = "txt_accuracy";
            this.txt_accuracy.Size = new System.Drawing.Size(100, 20);
            this.txt_accuracy.TabIndex = 5;
            this.txt_accuracy.TextChanged += new System.EventHandler(this.txt_accuracy_TextChanged);
            // 
            // lbl_accuracy
            // 
            this.lbl_accuracy.AutoSize = true;
            this.lbl_accuracy.Location = new System.Drawing.Point(281, 95);
            this.lbl_accuracy.Name = "lbl_accuracy";
            this.lbl_accuracy.Size = new System.Drawing.Size(52, 13);
            this.lbl_accuracy.TabIndex = 6;
            this.lbl_accuracy.Text = "Accuracy";
            // 
            // lbl_selectFile
            // 
            this.lbl_selectFile.AutoSize = true;
            this.lbl_selectFile.Location = new System.Drawing.Point(42, 13);
            this.lbl_selectFile.Name = "lbl_selectFile";
            this.lbl_selectFile.Size = new System.Drawing.Size(151, 13);
            this.lbl_selectFile.TabIndex = 7;
            this.lbl_selectFile.Text = "Select a file with temperatures:";
            // 
            // lbl_TemperatureToFind
            // 
            this.lbl_TemperatureToFind.AutoSize = true;
            this.lbl_TemperatureToFind.Location = new System.Drawing.Point(40, 344);
            this.lbl_TemperatureToFind.Name = "lbl_TemperatureToFind";
            this.lbl_TemperatureToFind.Size = new System.Drawing.Size(241, 13);
            this.lbl_TemperatureToFind.TabIndex = 8;
            this.lbl_TemperatureToFind.Text = "Input temperature to check if it is presented in file:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 455);
            this.Controls.Add(this.lbl_TemperatureToFind);
            this.Controls.Add(this.lbl_selectFile);
            this.Controls.Add(this.lbl_accuracy);
            this.Controls.Add(this.txt_accuracy);
            this.Controls.Add(this.btn_check);
            this.Controls.Add(this.chlb_Options);
            this.Controls.Add(this.fc_CheckTemp);
            this.Controls.Add(this.lb_temperatures);
            this.Controls.Add(this.fc_OpenFile);
            this.Name = "MainForm";
            this.Text = "TemperatureAnalizer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private OpenSaveFile.FileControl fc_OpenFile;
        private System.Windows.Forms.ListBox lb_temperatures;
        private OpenSaveFile.FileControl fc_CheckTemp;
        private System.Windows.Forms.CheckedListBox chlb_Options;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.TextBox txt_accuracy;
        private System.Windows.Forms.Label lbl_accuracy;
        private System.Windows.Forms.Label lbl_selectFile;
        private System.Windows.Forms.Label lbl_TemperatureToFind;
    }
}

