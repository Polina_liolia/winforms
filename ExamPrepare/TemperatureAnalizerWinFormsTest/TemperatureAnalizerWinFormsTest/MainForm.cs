﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenSaveFile;
using TemperatureAnalizatorTestConsole;

namespace TemperatureAnalizerWinFormsTest
{
    public partial class MainForm : Form
    {
        private TemperatureAnalizator tempAnaliz;
        public MainForm()
        {
            InitializeComponent();
        }

       private void ucClickOpenFile(object sender, EventArgs e)
        {
            if (sender is FileControl)
            {
                FileControl uk = sender as FileControl;
                if (uk != null)
                {
                    uk.OnBtnClick(sender, e);
                    tempAnaliz = new TemperatureAnalizator();
                    if (uk.Text.Length > 0)
                    {
                        bool reading_result = tempAnaliz.readTemperatureFromFile(uk.Text);
                        if (reading_result)
                        {
                            foreach (double t in tempAnaliz.Temperatures)
                            {
                                lb_temperatures.Items.Add(t);
                            }
                        }
                    }
                }
            }
        }
        
        private void fc_CheckTemp_BtnClick(object sender, EventArgs e)
        {
            string userTemperature = fc_CheckTemp.Text;
            try
            {
                bool searchTempResult = tempAnaliz.FindValue(userTemperature);
                if(searchTempResult)
                    MessageBox.Show(string.Format("Temperature value {0} was found in file", userTemperature));
                else
                    MessageBox.Show(string.Format("Temperature value {0} was NOT found in file", userTemperature));
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(string.Format("A wrong temperature value was inputed! Error: {0}", ex.Message));
            }
        }

        private void btn_check_Click(object sender, EventArgs e)
        {
            if (tempAnaliz!= null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (int i in chlb_Options.CheckedIndices)
                {
                    switch (i)
                    {
                        case 0:
                            {
                                sb.Append(string.Format("Max temperature: {0}\n", tempAnaliz.findMaxTemperature()));
                            }
                            break;
                        case 1:
                            {
                                sb.Append(string.Format("Min temperature: {0}\n", tempAnaliz.findMinTemperature()));
                            }
                            break;
                        case 2:
                            {
                                sb.Append(string.Format("Max temperature: {0}\n", tempAnaliz.findPopularTemperature()));
                            }
                            break;
                        case 3:
                            {
                                int checkStableInterval = tempAnaliz.findStableIntervalFirstElementIndex();
                                if (checkStableInterval != -1)
                                    sb.Append(string.Format("Stable temperature interval of 3 and more similar temperatures fas found on index: {0}\n", checkStableInterval));
                                else
                                    sb.Append("Stable temperature interval of 3 and more similar temperatures fas NOT found");
                            }
                            break;
                    }
                }
                MessageBox.Show(sb.ToString());
            }
            else
                MessageBox.Show("Error: no file with temperatures was selected.");
        }

        private void txt_accuracy_TextChanged(object sender, EventArgs e)
        {
            if (tempAnaliz != null)
            {
                double eps = 0;
                bool convertionResult = double.TryParse(txt_accuracy.Text, out eps);
                if (convertionResult)
                    tempAnaliz.Accuracy = eps;
                else
                    MessageBox.Show("Error: wrong format of accuracy inputed.");
            }
            else
                MessageBox.Show("Error: no file with temperatures was selected.");

        }
    }
}
