﻿namespace graphics_test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRect = new System.Windows.Forms.Button();
            this.btnEllipse = new System.Windows.Forms.Button();
            this.btnLine = new System.Windows.Forms.Button();
            this.btnTrg = new System.Windows.Forms.Button();
            this.btnCaption = new System.Windows.Forms.Button();
            this.btnDraw = new System.Windows.Forms.Button();
            this.pBox = new System.Windows.Forms.PictureBox();
            this.btn_openImg = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRect
            // 
            this.btnRect.Location = new System.Drawing.Point(13, 262);
            this.btnRect.Name = "btnRect";
            this.btnRect.Size = new System.Drawing.Size(75, 23);
            this.btnRect.TabIndex = 0;
            this.btnRect.Text = "Rectangle";
            this.btnRect.UseVisualStyleBackColor = true;
            this.btnRect.Click += new System.EventHandler(this.btnRect_Click);
            // 
            // btnEllipse
            // 
            this.btnEllipse.Location = new System.Drawing.Point(95, 262);
            this.btnEllipse.Name = "btnEllipse";
            this.btnEllipse.Size = new System.Drawing.Size(75, 23);
            this.btnEllipse.TabIndex = 1;
            this.btnEllipse.Text = "Ellipse";
            this.btnEllipse.UseVisualStyleBackColor = true;
            this.btnEllipse.Click += new System.EventHandler(this.btnEllipse_Click);
            // 
            // btnLine
            // 
            this.btnLine.Location = new System.Drawing.Point(177, 262);
            this.btnLine.Name = "btnLine";
            this.btnLine.Size = new System.Drawing.Size(75, 23);
            this.btnLine.TabIndex = 2;
            this.btnLine.Text = "Line";
            this.btnLine.UseVisualStyleBackColor = true;
            this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
            // 
            // btnTrg
            // 
            this.btnTrg.Location = new System.Drawing.Point(259, 262);
            this.btnTrg.Name = "btnTrg";
            this.btnTrg.Size = new System.Drawing.Size(75, 23);
            this.btnTrg.TabIndex = 3;
            this.btnTrg.Text = "Triangle";
            this.btnTrg.UseVisualStyleBackColor = true;
            this.btnTrg.Click += new System.EventHandler(this.btnTrg_Click);
            // 
            // btnCaption
            // 
            this.btnCaption.Location = new System.Drawing.Point(341, 262);
            this.btnCaption.Name = "btnCaption";
            this.btnCaption.Size = new System.Drawing.Size(75, 23);
            this.btnCaption.TabIndex = 4;
            this.btnCaption.Text = "Caption";
            this.btnCaption.UseVisualStyleBackColor = true;
            this.btnCaption.Click += new System.EventHandler(this.btnCaption_Click);
            // 
            // btnDraw
            // 
            this.btnDraw.Location = new System.Drawing.Point(440, 13);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(129, 39);
            this.btnDraw.TabIndex = 5;
            this.btnDraw.Text = "DRAW";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnDraw_MouseMove);
            // 
            // pBox
            // 
            this.pBox.Image = global::graphics_test.Properties.Resources.myresult;
            this.pBox.Location = new System.Drawing.Point(607, 13);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(246, 283);
            this.pBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBox.TabIndex = 6;
            this.pBox.TabStop = false;
            // 
            // btn_openImg
            // 
            this.btn_openImg.Location = new System.Drawing.Point(607, 322);
            this.btn_openImg.Name = "btn_openImg";
            this.btn_openImg.Size = new System.Drawing.Size(75, 23);
            this.btn_openImg.TabIndex = 7;
            this.btn_openImg.Text = "OpenImg";
            this.btn_openImg.UseVisualStyleBackColor = true;
            this.btn_openImg.Click += new System.EventHandler(this.btn_openImg_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 402);
            this.Controls.Add(this.btn_openImg);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.btnDraw);
            this.Controls.Add(this.btnCaption);
            this.Controls.Add(this.btnTrg);
            this.Controls.Add(this.btnLine);
            this.Controls.Add(this.btnEllipse);
            this.Controls.Add(this.btnRect);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Simple Graphics Test";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRect;
        private System.Windows.Forms.Button btnEllipse;
        private System.Windows.Forms.Button btnLine;
        private System.Windows.Forms.Button btnTrg;
        private System.Windows.Forms.Button btnCaption;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.PictureBox pBox;
        private System.Windows.Forms.Button btn_openImg;
    }
}

