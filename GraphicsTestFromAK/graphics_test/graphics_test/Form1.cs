﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graphics_test
{
    public partial class Form1 : Form
    {
        int circleX = 100,
            circleY = 100;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRect_Click(object sender, EventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Red;
            Brush myBrush = Brushes.Red;
            gr.DrawRectangle(myPen, 10, 25, 75, 200);
            gr.FillRectangle(myBrush, 10, 25, 75, 200);
        }

        private void btnEllipse_Click(object sender, EventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Blue;
            Brush myBrush = Brushes.Blue;
            gr.DrawEllipse(myPen, 95, 25, 75, 200);
            gr.FillEllipse(myBrush, 95, 25, 75, 200);
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
            Point p1 = new Point(180, 25);
            Point p2 = new Point(250, 225);
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Green;
            gr.DrawLine(myPen, p1, p2);
        }

        private void btnTrg_Click(object sender, EventArgs e)
        {
            Point p1 = new Point(260, 25);
            Point p2 = new Point(325, 225);
            Point p3 = new Point(260, 225);
            Point[] points = new Point[3];
            points[0] = p1;
            points[1] = p2;
            points[2] = p3;
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Red; //standart color
            Pen myCustomPen = new Pen(Color.Red, 3/*width (default=1)*/);
            Color myColor = Color.FromArgb(70/*opacity*/, 255/*r*/, 55/*g*/, 120/*b*/);
            Pen myCustomPen2 = new Pen(myColor, 10);

            Brush myBrush = Brushes.Gray;
            gr.DrawPolygon(myCustomPen2, points); //Pen - контур
            gr.FillPolygon(myBrush, points); //Brush - заливка
        }

        private void btnCaption_Click(object sender, EventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Gray;
            Brush myBrush = Brushes.Black;
            gr.DrawString("Hello", this.Font, myBrush, 350, 25);

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Gray;
            Brush myBrush = Brushes.Black;
            gr.DrawString("Hello", this.Font, myBrush, 5, 5);

            Pen myPen2 = Pens.Aqua;
            Brush myBrush2 = Brushes.Aqua;

           Rectangle movingCircle = new Rectangle(circleX, circleY, 25, 25);
           gr.DrawEllipse(myPen2, movingCircle);
           gr.FillEllipse(myBrush2, movingCircle);
        }

        private void btnDraw_MouseMove(object sender, MouseEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(btnDraw.Handle);
            Pen myPen = Pens.Gray;
            Brush myBrush = Brushes.Black;
            gr.DrawString("Hello", this.Font, myBrush, 5, 5);

        }

        private void btn_openImg_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open Image";
            dlg.Filter = "bmp files (*.bmp)|*.bmp|jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
                pBox.Image = new Bitmap(dlg.FileName);
            dlg.Dispose();//т.к. при открытии OpenFileDialog происходит обращение к устаревшим технологиям (WinAPI)
                          //(OpenFileDialog - обертка над WinAPI)
        }

        //это событие выполняется один раз - по окончании загрузки формы
        //здесь происходит настройка интерфейсных компонентов
        private void Form1_Load(object sender, EventArgs e)
        {
            //дорисовываем круг на предварительно загруеженную в PictureBox картинку
            Graphics gr = Graphics.FromImage(pBox.Image);
            RectangleF rect = new RectangleF(new Point(10, 80), new Size(30, 30));
            gr.DrawEllipse(new Pen(Color.Green, 3), rect);
        }

        //перегрузка метода для изменения приоритета, в соответствии с которым вызывается обработчик событий
        //клавиш управления
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Right)
            {
                circleX += 5;
            }
            else if (keyData == Keys.Left)
            {
                circleX -= 5;
            }
            else if (keyData == Keys.Up)
            {
                circleY -= 5;
            }
            else if (keyData == Keys.Down)
            {
                circleY += 5;
            }
            this.Invalidate(); //redraw
            return false; //событие обработано
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e) 
        {
            //реакция на стрелки по умолчанию отсутствует из-заболее высокого приоритета операций изменения
            //фокуса на элементах управления
            //если нужно пеагировать на нажатие клавиш управления именно другим способом - нужно перегрузить 
            //ProcessCmdKey
            
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar == 'd')
           {
                circleX += 5;
           }
           else if (e.KeyChar == 'a')
            {
                circleX -= 5;
            }
            else if (e.KeyChar == 'w')
            {
                circleY -= 5;
            }
            else if (e.KeyChar == 'x')
            {
                circleY += 5;
            }
            this.Invalidate(); //redraw
        }

       
    }
}
