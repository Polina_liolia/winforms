﻿namespace KeyMouseEventTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblMessage2 = new System.Windows.Forms.Label();
            this.lblTestPress = new System.Windows.Forms.Label();
            this.txtBoxKeyPress = new System.Windows.Forms.TextBox();
            this.lblTestDown = new System.Windows.Forms.Label();
            this.txtBoxKeyDown = new System.Windows.Forms.TextBox();
            this.btnPressMe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 9);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(72, 13);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "MouseEvents";
            // 
            // lblMessage2
            // 
            this.lblMessage2.AutoSize = true;
            this.lblMessage2.Location = new System.Drawing.Point(160, 9);
            this.lblMessage2.Name = "lblMessage2";
            this.lblMessage2.Size = new System.Drawing.Size(72, 13);
            this.lblMessage2.TabIndex = 1;
            this.lblMessage2.Text = "MouseEvents";
            // 
            // lblTestPress
            // 
            this.lblTestPress.AutoSize = true;
            this.lblTestPress.Location = new System.Drawing.Point(12, 217);
            this.lblTestPress.Name = "lblTestPress";
            this.lblTestPress.Size = new System.Drawing.Size(78, 13);
            this.lblTestPress.TabIndex = 2;
            this.lblTestPress.Text = "Test Key Press";
            // 
            // txtBoxKeyPress
            // 
            this.txtBoxKeyPress.Location = new System.Drawing.Point(15, 233);
            this.txtBoxKeyPress.Name = "txtBoxKeyPress";
            this.txtBoxKeyPress.Size = new System.Drawing.Size(100, 20);
            this.txtBoxKeyPress.TabIndex = 3;
            this.txtBoxKeyPress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxKeyPress_KeyPress);
            // 
            // lblTestDown
            // 
            this.lblTestDown.AutoSize = true;
            this.lblTestDown.Location = new System.Drawing.Point(12, 294);
            this.lblTestDown.Name = "lblTestDown";
            this.lblTestDown.Size = new System.Drawing.Size(80, 13);
            this.lblTestDown.TabIndex = 4;
            this.lblTestDown.Text = "Test Key Down";
            // 
            // txtBoxKeyDown
            // 
            this.txtBoxKeyDown.Location = new System.Drawing.Point(15, 310);
            this.txtBoxKeyDown.Name = "txtBoxKeyDown";
            this.txtBoxKeyDown.Size = new System.Drawing.Size(100, 20);
            this.txtBoxKeyDown.TabIndex = 5;
            this.txtBoxKeyDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBoxKeyDown_KeyDown);
            // 
            // btnPressMe
            // 
            this.btnPressMe.Location = new System.Drawing.Point(257, 154);
            this.btnPressMe.Name = "btnPressMe";
            this.btnPressMe.Size = new System.Drawing.Size(75, 23);
            this.btnPressMe.TabIndex = 6;
            this.btnPressMe.Text = "Press Me";
            this.btnPressMe.UseVisualStyleBackColor = true;
            this.btnPressMe.Click += new System.EventHandler(this.btnPressMe_Click);
            this.btnPressMe.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnPressMe_MouseMove);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 354);
            this.Controls.Add(this.btnPressMe);
            this.Controls.Add(this.txtBoxKeyDown);
            this.Controls.Add(this.lblTestDown);
            this.Controls.Add(this.txtBoxKeyPress);
            this.Controls.Add(this.lblTestPress);
            this.Controls.Add(this.lblMessage2);
            this.Controls.Add(this.lblMessage);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Тест мышиных событий";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblMessage2;
        private System.Windows.Forms.Label lblTestPress;
        private System.Windows.Forms.TextBox txtBoxKeyPress;
        private System.Windows.Forms.Label lblTestDown;
        private System.Windows.Forms.TextBox txtBoxKeyDown;
        private System.Windows.Forms.Button btnPressMe;
    }
}

