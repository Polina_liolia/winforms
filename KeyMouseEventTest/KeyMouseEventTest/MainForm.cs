﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyMouseEventTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            int X = e.X; //кооррдинаты курсора мыши
            int Y = e.Y;
            lblMessage.Text = "MouseDown x = " + e.X.ToString() + "; y = " + e.Y.ToString();
            Graphics gr = Graphics.FromHwnd(this.Handle);            
            gr.FillEllipse(Brushes.Green, X, Y, 10, 10);
            this.Text = "Buttons: " + e.Button.ToString();
        }

        private void MainForm_MouseUp(object sender, MouseEventArgs e)
        {
            lblMessage2.Text = "MousUp x = " + e.X.ToString() + "; y = " + e.Y.ToString();
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawEllipse(Pens.Red, e.X, e.Y, 10, 10);
        }

        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            //аналог MouseDown
            //MainForm_MouseDown(sender, e);
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            //возникает когда курсор мыши оказывается над компонентом
            #region Команды
            /*int X = e.X; //кооррдинаты курсора мыши
            int Y = e.Y;
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawLine(Pens.Red, X, Y, 10, 10);*/
            #endregion         
        }

        private void txtBoxKeyPress_KeyPress(object sender, KeyPressEventArgs e)
        {
            //перехватывает нажатия от любых клавиш
            string tmp = "0123456789";
            if (tmp.Contains(e.KeyChar.ToString()))
            {
                lblTestPress.Text = "Pressed key is " + e.KeyChar.ToString();
            }
            else {
                e.KeyChar = (char)0; //символ с кодом 0 - пустота
            }
        }

        private void txtBoxKeyDown_KeyDown(object sender, KeyEventArgs e)
        {
            //реагирует на нажатие любых клавиш
            lblTestDown.Text = "Pressed key code is " + e.KeyValue.ToString();
        }

        private void btnPressMe_MouseMove(object sender, MouseEventArgs e)
        {
            Random rnd = new Random();
            int n = rnd.Next(1, 175);
            btnPressMe.Left = n;
            btnPressMe.Top = n + 10;
        }

        private void btnPressMe_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You win!");
            btnPressMe.MouseMove -= btnPressMe_MouseMove;
        }
    }
}
