﻿namespace ListViewTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_quarters = new System.Windows.Forms.ListView();
            this.lbl_quarters_list = new System.Windows.Forms.Label();
            this.lv_monthes = new System.Windows.Forms.ListView();
            this.lbl_monthes = new System.Windows.Forms.Label();
            this.lvEmployees = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowSelected = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_quarters
            // 
            this.lv_quarters.Location = new System.Drawing.Point(25, 48);
            this.lv_quarters.Name = "lv_quarters";
            this.lv_quarters.Size = new System.Drawing.Size(121, 155);
            this.lv_quarters.TabIndex = 0;
            this.lv_quarters.UseCompatibleStateImageBehavior = false;
            this.lv_quarters.SelectedIndexChanged += new System.EventHandler(this.lv_quarters_SelectedIndexChanged);
            // 
            // lbl_quarters_list
            // 
            this.lbl_quarters_list.AutoSize = true;
            this.lbl_quarters_list.Location = new System.Drawing.Point(25, 29);
            this.lbl_quarters_list.Name = "lbl_quarters_list";
            this.lbl_quarters_list.Size = new System.Drawing.Size(100, 13);
            this.lbl_quarters_list.TabIndex = 1;
            this.lbl_quarters_list.Text = "Список кварталов";
            // 
            // lv_monthes
            // 
            this.lv_monthes.Location = new System.Drawing.Point(152, 48);
            this.lv_monthes.Name = "lv_monthes";
            this.lv_monthes.Size = new System.Drawing.Size(121, 251);
            this.lv_monthes.TabIndex = 2;
            this.lv_monthes.UseCompatibleStateImageBehavior = false;
            this.lv_monthes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lv_monthes_ColumnClick);
            this.lv_monthes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_monthes_MouseClick);
            // 
            // lbl_monthes
            // 
            this.lbl_monthes.AutoSize = true;
            this.lbl_monthes.Location = new System.Drawing.Point(152, 29);
            this.lbl_monthes.Name = "lbl_monthes";
            this.lbl_monthes.Size = new System.Drawing.Size(91, 13);
            this.lbl_monthes.TabIndex = 3;
            this.lbl_monthes.Text = "Список месяцев";
            // 
            // lvEmployees
            // 
            this.lvEmployees.Location = new System.Drawing.Point(307, 48);
            this.lvEmployees.Name = "lvEmployees";
            this.lvEmployees.Size = new System.Drawing.Size(401, 251);
            this.lvEmployees.TabIndex = 4;
            this.lvEmployees.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Список сотрудников";
            // 
            // btnShowSelected
            // 
            this.btnShowSelected.Location = new System.Drawing.Point(307, 317);
            this.btnShowSelected.Name = "btnShowSelected";
            this.btnShowSelected.Size = new System.Drawing.Size(401, 23);
            this.btnShowSelected.TabIndex = 6;
            this.btnShowSelected.Text = "Показать выделенные";
            this.btnShowSelected.UseVisualStyleBackColor = true;
            this.btnShowSelected.Click += new System.EventHandler(this.btnShowSelected_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 472);
            this.Controls.Add(this.btnShowSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvEmployees);
            this.Controls.Add(this.lbl_monthes);
            this.Controls.Add(this.lv_monthes);
            this.Controls.Add(this.lbl_quarters_list);
            this.Controls.Add(this.lv_quarters);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Lists";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_quarters;
        private System.Windows.Forms.Label lbl_quarters_list;
        private System.Windows.Forms.ListView lv_monthes;
        private System.Windows.Forms.Label lbl_monthes;
        private System.Windows.Forms.ListView lvEmployees;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShowSelected;
    }
}

