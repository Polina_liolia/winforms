﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace ListViewTest
{
    //ДЗ:
    /*
     по клику на месяц в списке выдать message box с содержимым элемента списка, по которому кликнули
         */
    public partial class MainForm : Form
    {
        private string[] first_quarter = { "January", "Fabruary", "March" };
        private string[] second_quarter = { "April", "May", "Jun" };
        private string[] third_quarter = { "July", "August", "September" };
        private string[] fourth_quarter = { "October", "November", "December" };
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lv_quarters.View = View.Details; //внешний вид - таблица
            lv_quarters.GridLines = true;//строки прорисовывать
            lv_quarters.FullRowSelect = true;
            lv_quarters.MultiSelect = true; //разрешить множественный выбор
            lv_quarters.Columns.Add("Quarters", 100);//создаём одну колонку "Quarters" шириной 120 символов
            lv_quarters.Items.Add("The first quarter");
            lv_quarters.Items.Add("The second quarter");
            lv_quarters.Items.Add("The thirdt quarter");
            lv_quarters.Items.Add("The fourth quarter");

            lv_monthes.View = View.Details;
            lv_monthes.GridLines = true;
            lv_monthes.FullRowSelect = true;
            lv_monthes.Columns.Add("Monthes", 100);
            lv_monthes.MultiSelect = true; //разрешить множественный выбор


            lvEmployees.View = View.Details;
            lvEmployees.GridLines = true;
            lvEmployees.FullRowSelect = true;
            lvEmployees.CheckBoxes = true;
            //Add column header
            lvEmployees.Columns.Add("ID", 50);//0
            lvEmployees.Columns.Add("Phone", 100);//1
            lvEmployees.Columns.Add("Last Name", 120);//2
            lvEmployees.Columns.Add("First Name", 140);//3

            #region Add Items to lvEmployees
            string[] arr = new string[4];
            arr[0] = "5001";
            arr[1] = "159-224-77-199";
            arr[2] = "Петров";
            arr[3] = "Иван";
            ListViewItem itm = new ListViewItem(arr);
            itm.Checked = true;
            lvEmployees.Items.Add(itm);

            string[] arr1 = new string[4];
            arr1[0] = "5002";
            arr1[1] = "159-224-77-200";
            arr1[2] = "Петрова";
            arr1[3] = "Иванна";
            ListViewItem itm1 = new ListViewItem(arr1);
            itm1.Checked = false;
            itm1.ForeColor = Color.Red;
            itm1.BackColor = Color.Lavender;
            lvEmployees.Items.Add(itm1);

            #endregion


        }

        private void lv_quarters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lv_monthes.Items.Clear(); //очистить список месяцев
            for (int i = 0; i < lv_quarters.SelectedIndices.Count; i++)
            {
                int index = lv_quarters.SelectedIndices[i];
                if (0 == index)
                {
                    for (int j = 0; j < first_quarter.Length; j++)
                        lv_monthes.Items.Add(first_quarter[j]);
                }
                if (1 == index)
                {
                    for (int j = 0; j < second_quarter.Length; j++)
                        lv_monthes.Items.Add(second_quarter[j]);
                }
                if (2 == index)
                {
                    for (int j = 0; j < third_quarter.Length; j++)
                        lv_monthes.Items.Add(third_quarter[j]);
                }
                if (3 == index)
                {
                    for (int j = 0; j < fourth_quarter.Length; j++)
                        lv_monthes.Items.Add(fourth_quarter[j]);
                }
            }
        }

        private void lv_monthes_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            //sorting monthes after click on header:
            ArrayList sortlist = new ArrayList();
            for (int i = 0; i < lv_monthes.Items.Count; i++)
                sortlist.Add(lv_monthes.Items[i].Text);
            sortlist.Sort();
            lv_monthes.Items.Clear();
            foreach (string s in sortlist)
                lv_monthes.Items.Add(s);
        }

        private void lv_monthes_MouseClick(object sender, MouseEventArgs e)
        {
            if (sender is ListView)
            {
                ListView click_target = sender as ListView;
                if (click_target != null)
                {
                    MessageBox.Show(click_target.SelectedItems[0].Text, 
                        "Month selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
        }

        private void btnShowSelected_Click(object sender, EventArgs e)
        {
            if (lvEmployees.SelectedItems.Count > 0)
            {
                string id = lvEmployees.SelectedItems[0].SubItems[0].Text;
                string phone = lvEmployees.SelectedItems[0].SubItems[1].Text;
                string lname = lvEmployees.SelectedItems[0].SubItems[2].Text;
                string info = string.Format("ID: {0}\nPhone: {1}\nLast name: {2}",
                    id, phone, lname);
                MessageBox.Show(info, "Selected item info");
            }
        }
    }
}
