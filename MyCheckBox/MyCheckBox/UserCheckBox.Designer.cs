﻿namespace MyCheckBox
{
    partial class UserCheckBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chb_Man = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chb_Man
            // 
            this.chb_Man.AutoSize = true;
            this.chb_Man.Location = new System.Drawing.Point(18, 19);
            this.chb_Man.Name = "chb_Man";
            this.chb_Man.Size = new System.Drawing.Size(71, 17);
            this.chb_Man.TabIndex = 0;
            this.chb_Man.Text = "Мужчина";
            this.chb_Man.UseVisualStyleBackColor = true;
            this.chb_Man.CheckedChanged += new System.EventHandler(this.chb_Man_CheckedChanged);
            // 
            // UserCheckBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chb_Man);
            this.Name = "UserCheckBox";
            this.Size = new System.Drawing.Size(105, 60);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UserCheckBox_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chb_Man;
    }
}
