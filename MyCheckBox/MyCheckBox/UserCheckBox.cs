﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyCheckBox
{
    public partial class UserCheckBox : UserControl
    {
        public UserCheckBox()
        {
            InitializeComponent();


        }

        [Browsable(true)] //свойство Text будет доступно в свойствах компонента
        public override string Text //перенаправляем текст текстбоксу внутри компонента
        {
            get
            {
                return chb_Man.Text;
            }

            set
            {
                chb_Man.Text = value;
            }
        }

        private void chb_Man_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void UserCheckBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen pen = Pens.Red;
            Brush brush = Brushes.Red;
            gr.DrawEllipse(pen, chb_Man.Left-15, chb_Man.Top-10, chb_Man.Width+20, chb_Man.Height+20);
        }
    }
}
