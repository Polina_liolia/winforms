﻿namespace MyCheckBoxTester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userChB_Sex = new MyCheckBox.UserCheckBox();
            this.SuspendLayout();
            // 
            // userChB_Sex
            // 
            this.userChB_Sex.Location = new System.Drawing.Point(23, 12);
            this.userChB_Sex.Name = "userChB_Sex";
            this.userChB_Sex.Size = new System.Drawing.Size(111, 53);
            this.userChB_Sex.TabIndex = 0;
            this.userChB_Sex.Load += new System.EventHandler(this.userCheckBox1_Load);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 262);
            this.Controls.Add(this.userChB_Sex);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MyCheckBox.UserCheckBox userChB_Sex;
    }
}

