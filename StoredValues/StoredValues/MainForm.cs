﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StoredValues.Properties; //to use class Settings

namespace StoredValues
{
    public partial class MainForm : Form
    {
        Settings settings = Settings.Default;

        public MainForm()
        {
            //загрузка сохраненных значений компонентов в Settings
            settings.Reload();
            InitializeComponent();
            //применить загруженные значения к компонентам:
            InitializeMyComponents();
        }
        private void InitializeMyComponents()
        {
            txtInput.Text = settings.TextValue;
            chTest.Checked = settings.BoolValue;
            txtNumber.Text = settings.IntValue.ToString();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true; //сделать невозможным закрытие окна
            SaveSettings();
        }

        private void SaveSettings()
        {
            settings.TextValue = txtInput.Text;
            settings.BoolValue = chTest.Checked;
            int number;
            bool convertResult = Int32.TryParse(txtNumber.Text, out number);
            settings.IntValue = convertResult ? number : settings.IntValue;
            settings.Save();
        }
    }
}
