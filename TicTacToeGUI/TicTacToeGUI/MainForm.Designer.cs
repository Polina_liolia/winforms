﻿namespace TicTacToeGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_11 = new System.Windows.Forms.Button();
            this.btn_12 = new System.Windows.Forms.Button();
            this.btn_13 = new System.Windows.Forms.Button();
            this.btn_21 = new System.Windows.Forms.Button();
            this.btn_22 = new System.Windows.Forms.Button();
            this.btn_23 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_11
            // 
            this.btn_11.Location = new System.Drawing.Point(24, 38);
            this.btn_11.Name = "btn_11";
            this.btn_11.Size = new System.Drawing.Size(75, 46);
            this.btn_11.TabIndex = 0;
            this.btn_11.Text = "0";
            this.btn_11.UseVisualStyleBackColor = true;
            this.btn_11.Click += new System.EventHandler(this.btn_11_Click);
            // 
            // btn_12
            // 
            this.btn_12.Location = new System.Drawing.Point(106, 38);
            this.btn_12.Name = "btn_12";
            this.btn_12.Size = new System.Drawing.Size(75, 46);
            this.btn_12.TabIndex = 1;
            this.btn_12.Text = "0";
            this.btn_12.UseVisualStyleBackColor = true;
            this.btn_12.Click += new System.EventHandler(this.btn_12_Click);
            // 
            // btn_13
            // 
            this.btn_13.Location = new System.Drawing.Point(188, 38);
            this.btn_13.Name = "btn_13";
            this.btn_13.Size = new System.Drawing.Size(75, 46);
            this.btn_13.TabIndex = 2;
            this.btn_13.Text = "0";
            this.btn_13.UseVisualStyleBackColor = true;
            this.btn_13.Click += new System.EventHandler(this.btn_13_Click);
            // 
            // btn_21
            // 
            this.btn_21.Location = new System.Drawing.Point(24, 91);
            this.btn_21.Name = "btn_21";
            this.btn_21.Size = new System.Drawing.Size(75, 44);
            this.btn_21.TabIndex = 3;
            this.btn_21.Text = "button1";
            this.btn_21.UseVisualStyleBackColor = true;
            this.btn_21.Click += new System.EventHandler(this.btn_13_Click);
            // 
            // btn_22
            // 
            this.btn_22.Location = new System.Drawing.Point(106, 91);
            this.btn_22.Name = "btn_22";
            this.btn_22.Size = new System.Drawing.Size(75, 44);
            this.btn_22.TabIndex = 4;
            this.btn_22.Text = "button2";
            this.btn_22.UseVisualStyleBackColor = true;
            this.btn_22.Click += new System.EventHandler(this.btn_13_Click);
            // 
            // btn_23
            // 
            this.btn_23.Location = new System.Drawing.Point(188, 91);
            this.btn_23.Name = "btn_23";
            this.btn_23.Size = new System.Drawing.Size(75, 44);
            this.btn_23.TabIndex = 5;
            this.btn_23.Text = "button3";
            this.btn_23.UseVisualStyleBackColor = true;
            this.btn_23.Click += new System.EventHandler(this.btn_13_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 451);
            this.Controls.Add(this.btn_23);
            this.Controls.Add(this.btn_22);
            this.Controls.Add(this.btn_21);
            this.Controls.Add(this.btn_13);
            this.Controls.Add(this.btn_12);
            this.Controls.Add(this.btn_11);
            this.Name = "MainForm";
            this.Text = "TicTacToe";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_11;
        private System.Windows.Forms.Button btn_12;
        private System.Windows.Forms.Button btn_13;
        private System.Windows.Forms.Button btn_21;
        private System.Windows.Forms.Button btn_22;
        private System.Windows.Forms.Button btn_23;
    }
}

