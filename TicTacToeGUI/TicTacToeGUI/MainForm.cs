﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeGUI
{
    public partial class MainForm : Form
    {
        private Button[] buttons_line3 =
        {
            new Button(),
            new Button(),
            new Button()
        };
        
        public MainForm()
        {
            InitializeComponent();
            myInitializeComponent();

        }

        private void myInitializeComponent()
        {
            int x = 24,
                y = 144,
                width = 75,
                height = 44,
                space_horisontal = 7,
                i = 0;
            foreach (Button btn in buttons_line3)
            {
                btn.Location = new Point(x + (width + space_horisontal) * i++, y);
                btn.Name = string.Format("btn_3{0}", i);
                btn.Size = new Size(width, height);
                btn.TabIndex = 6 + i;
                btn.Text = string.Format("button3{0}", i);
                btn.UseVisualStyleBackColor = true;
                btn.Click += new EventHandler(this.btn_13_Click);
                Controls.Add(btn); //помещаем в контейнер (в данном случае - форма)
            }
            SuspendLayout(); //применить стили Windows к созданным компонентам (если они заданы)
        }

        private void btn_11_Click(object sender, EventArgs e)
        {
            btn_11.Text = "Clicked";
        }

        private void btn_12_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.Text = "Clicked";
        }

        private void btn_13_Click(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button btn = sender as Button;
                if (btn != null)
                    btn.Text = string.Format("Clicked {0}", btn.Name);
            }
        }
    }
}
