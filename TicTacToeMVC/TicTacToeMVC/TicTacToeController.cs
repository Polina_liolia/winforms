﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    //отвечает за логику игры - чередование ходов и поиск победителя
    //Передает данные из View в Model
    public sealed class  TicTacToeController
    {
        private TicTacToeModel model = new TicTacToeModel();
        private int counter;
        private TicTacToeController() {}
        IView current_view;
        
        public TicTacToeController(View1 view)
        {
            this.current_view = view; 
        }

        public TicTacToeController(View2 view)
        {
            this.current_view = view;
        }

        public TicTacToeController(View3 view)
        {
            this.current_view = view;
        }


        public bool sendPosition(int iposition, int jposition, out string viewValue)
        {
            if (!model.haveValue(iposition, jposition))
            {
                counter++;
                if (counter % 2 == 0) //четные ходы - нолики
                {
                    viewValue = TicTacToeModel.OValue;
                    model.setValue(iposition, jposition, TicTacToeModel.OValue);
                    if (model.haveLine(TicTacToeModel.OValue))
                    {
                        current_view.sendGameOver("O wins!");
                        current_view.clearField();
                        model.clearMatrix();
                        counter = 0;
                        return false;
                    }

                }
                else
                {
                    viewValue = TicTacToeModel.XValue;
                    model.setValue(iposition, jposition, TicTacToeModel.XValue);
                    if (model.haveLine(TicTacToeModel.XValue))
                    {
                        current_view.sendGameOver("X wins!");
                        current_view.clearField();
                        model.clearMatrix();
                        counter = 0;
                        return false;
                    }
                }
                if (counter == 9 && model.haveLine(TicTacToeModel.OValue) == false &&
                     model.haveLine(TicTacToeModel.XValue) == false)
                {
                    current_view.sendGameOver("Nobody wins!");
                    current_view.clearField();
                    model.clearMatrix();
                    counter = 0;
                    return false;

                }

                return true;
            }
            viewValue = TicTacToeModel.EmptyValue;
            return false;
        }

        
    }
}
