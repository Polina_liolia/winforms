﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    
    public partial class View1 : Form, IView
    {
        private Button[,] buttons =
            {
                {
                    new Button(),
                    new Button(),
                    new Button()
                 },
                {
                    new Button(),
                    new Button(),
                    new Button()
                 },
                {
                    new Button(),
                    new Button(),
                    new Button()
                 }
            };
        private TicTacToeController controller;
        
        public View1()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }

        public void createView()
        {
            int start_x = 5,
               start_y = 5,
               space_horisontal = 7,
               space_vertical = 7;
               
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    buttons[i,j].Size = new Size(70, 70);
                    buttons[i,j].Location = new Point(start_x + (buttons[i,j].Width + space_horisontal) * (j), (start_y + (buttons[i,j].Height + space_vertical) * (i)));
                    buttons[i,j].Name = string.Format("btn_{0}{1}", i+1, j+1);
                    buttons[i,j].TabIndex = 6 + i;
                    buttons[i,j].Text = "";
                    buttons[i,j].UseVisualStyleBackColor = true;
                    buttons[i, j].Click += btnClick;
                    Controls.Add(buttons[i, j]); //помещаем в контейнер (в данном случае - форма)
                    
                }
            SuspendLayout(); //применить стили Windows к созданным компонентам (если они заданы)
       
        }

        public void btnClick(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    int iposition;
                    int jposition;
                    findViewItem(btn, out iposition, out jposition);
                    if (iposition != -1 && jposition != -1)
                    {
                        string val;
                        bool result = controller.sendPosition(iposition, jposition, out val);
                        if (result)
                        {
                            btn.Text = val;
                            
                        }
                    }
                }
            }
        }

        private void findViewItem(Button btn, out int iposition, out int jposition)
        {
            iposition = -1;
            jposition = -1;
            for (int i = 0; i < 3; i++)
            {
                bool f = false;
                for (int j = 0; j < 3; j++)
                {
                    if (buttons[i, j] == btn)
                    {
                        iposition = i;
                        jposition = j;
                        f = true;
                        break;
                    }
                }
                if (f) break;
            }
        }

        public void sendGameOver(string msg)
        {
            MessageBox.Show(msg, "Результат игры");
        }

        public void clearField()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    buttons[i, j].Text = TicTacToeModel.EmptyValue;
                }
            this.Update();
        }
    }
}
