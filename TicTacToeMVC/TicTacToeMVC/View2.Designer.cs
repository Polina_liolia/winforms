﻿namespace TicTacToeMVC
{
    partial class View2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_PlayField = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PlayField)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_PlayField
            // 
            this.dgv_PlayField.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_PlayField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_PlayField.Location = new System.Drawing.Point(0, 0);
            this.dgv_PlayField.Name = "dgv_PlayField";
            this.dgv_PlayField.Size = new System.Drawing.Size(364, 320);
            this.dgv_PlayField.TabIndex = 0;
            this.dgv_PlayField.Resize += new System.EventHandler(this.dgv_PlayField_Resize);
            // 
            // View2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 320);
            this.Controls.Add(this.dgv_PlayField);
            this.Name = "View2";
            this.Text = "View2 DGV";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PlayField)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_PlayField;
    }
}