﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    public partial class View2 : Form, IView
    {
        TicTacToeController controller;
        public View2()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }
       

        private void populateDataGridView()
        {
            // Add columns to the DataGridView.
            dgv_PlayField.ColumnCount = 3;
            // Add rows of data to the DataGridView.
            dgv_PlayField.Rows.Add(new string[] { "", "", "" });
            dgv_PlayField.Rows.Add(new string[] { "", "", "" });
            dgv_PlayField.Rows.Add(new string[] { "", "", "" });
        }

        public void createView()
        {
            populateDataGridView();
            //rows:
            dgv_PlayField.AllowUserToAddRows = false;
            dgv_PlayField.AllowUserToDeleteRows = false;
            dgv_PlayField.RowHeadersVisible = false;
            dgv_PlayField.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgv_PlayField.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgv_PlayField.Rows[0].Height = dgv_PlayField.Height / 3;
            dgv_PlayField.Rows[1].Height = dgv_PlayField.Height / 3;
            dgv_PlayField.Rows[2].Height = dgv_PlayField.Height / 3;
            dgv_PlayField.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            //columns:
            dgv_PlayField.AllowUserToOrderColumns = false;
            dgv_PlayField.ColumnHeadersVisible = false;
            dgv_PlayField.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_PlayField.ReadOnly = true;
           
            dgv_PlayField.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.viewClickAction);
            dgv_PlayField.CellFormatting += Dgv_PlayField_CellFormatting;
            dgv_PlayField.CellParsing += Dgv_PlayField_CellParsing;
            
        }

        private void Dgv_PlayField_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (sender is DataGridView)
            {
                DataGridView dgv = sender as DataGridView;
                if (dgv != null)
                {
                    if (e.Value.ToString() == TicTacToeModel.XValue)
                        e.InheritedCellStyle.BackColor = Color.Green;
                    else if (e.Value.ToString() == TicTacToeModel.OValue)
                        e.InheritedCellStyle.BackColor = Color.Yellow;

                }
            }
        }

        private void Dgv_PlayField_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (sender is DataGridView)
            {
                DataGridView dgv = sender as DataGridView;
                if (dgv != null)
                {
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    if (e.Value.ToString() == TicTacToeModel.XValue)
                        e.CellStyle.BackColor = Color.Green;
                    else if (e.Value.ToString() == TicTacToeModel.OValue)
                        e.CellStyle.BackColor = Color.Yellow;

                }
            }
        }

        private void viewClickAction(object sender, DataGridViewCellEventArgs e)
        {
            int numRow = e.RowIndex;
            //dgv.CurrentRow.Cells[e.ColumnIndex].Style.BackColor = Color.Red;
            int iposition = e.RowIndex;
            int jposition = e.ColumnIndex;
            string viewValue = string.Empty;
            bool canSetValue = controller.sendPosition(iposition, jposition, out viewValue);
            if (canSetValue)
            {
                dgv_PlayField.CurrentRow.Cells[e.ColumnIndex].Value = viewValue;
            }
        }

        //sets empty string as value of all cells
        public void clearField()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    dgv_PlayField.Rows[i].Cells[j].Value = "";
        }

        public void sendGameOver(string msg)
        {
            MessageBox.Show(msg,
                "Резльтат игры",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        //to change rows height on dgv resizing
        private void dgv_PlayField_Resize(object sender, EventArgs e)
        {
            dgv_PlayField.Rows[0].Height = dgv_PlayField.Height / 3;
            dgv_PlayField.Rows[1].Height = dgv_PlayField.Height / 3;
            dgv_PlayField.Rows[2].Height = dgv_PlayField.Height / 3;
        }
    }
}
