﻿using System;
namespace TicTacToeMVC
{
    interface IView
    {
        void sendGameOver(string message);
    }
}
