﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToeMVC
{
    /// <summary>
    /// Класс - управленец - контроллер. Отвечает: за логику игры (чередование ходов и поиск победителя). Передает данные из View в Model и наоборот.
    /// </summary>
    public sealed class TicTacToeController
    {
        private TicTacToeModel model = new TicTacToeModel();
        protected TicTacToeController()
        {

        }
        IView currentView;
        public TicTacToeController(View1 view)
        {
            this.currentView = view;
        }
        public TicTacToeController(View2 view)
        {
            this.currentView = view;
        }
        int counter;
        public bool sendPosition(int iposition, int jposition, out string viewValue)
        {
            if (!model.haveValue(iposition, jposition))
            {
                counter++;
                if (counter % 2 == 0)//четные ходы - Нолики
                {
                    viewValue = model.OValue;
                    model.setValue(model.OValue, iposition, jposition);
                }
                else
                {
                    viewValue = model.XValue;
                    model.setValue(model.XValue, iposition, jposition);
                }
                if (   counter == 9 
                    && model.haveLine(model.XValue) == false
                    && model.haveLine(model.OValue) == false)
                {//ничья
                    currentView.sendGameOver("Ничья!");
                }
                return true;
            }
            else
            {
                viewValue = string.Empty;
                return false;
            }
        }
    }
}
