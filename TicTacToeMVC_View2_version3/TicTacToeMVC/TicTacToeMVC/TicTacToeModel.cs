﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToeMVC
{
    /// <summary>
    /// Модель игры - обертка на матрицей. Ответственность: поиск "линий" одинаковых значений.
    /// </summary>
    public class TicTacToeModel
    {
        private string[,] matrix;
        public string XValue = "X";
        public string OValue = "0";
        string EmptyValue = "-";
        public TicTacToeModel()
        {
            matrix = new string[3, 3] { 
                                       { EmptyValue, EmptyValue, EmptyValue },
                                       { EmptyValue, EmptyValue, EmptyValue },
                                       { EmptyValue, EmptyValue, EmptyValue }
                                      };
        }
        public bool haveLine(string value)
        {
            return false;
        }

        public  bool haveValue(int iposition, int jposition)
        {
            string curValue = matrix[iposition, jposition];
            bool result = curValue.Contains(EmptyValue);
            if (result == false)
                return true;
            else
                return false;
        }
        public void setValue(string value, int iposition, int jposition)
        {
            matrix[iposition, jposition] = value;
        }
    }
}
