﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    public partial class View1 : Form, TicTacToeMVC.IView
    {
        private TicTacToeController controller;
        private Button btn31;
        private Button[,] btnLines;
        public View1()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }
        private void createView()
        {
            #region Пример настройки одного компонента
            /*
            this.btn31 = new System.Windows.Forms.Button();
            //создал компонент в памяти
            //задал внешний вид
            this.btn31.Location = new System.Drawing.Point(19, 81 + 47);
            this.btn31.Name = "btn31";
            this.btn31.Size = new System.Drawing.Size(75, 47);
            this.btn31.TabIndex = 3;
            this.btn31.Text = "31";
            this.btn31.UseVisualStyleBackColor = true;
            this.btn31.Click += new System.EventHandler(this.btn31_Click);
            //поместил в контейнер, который будет отвечать за отображение
            this.Controls.Add(this.btn31);
            */
            #endregion
            btnLines = new Button[3,3];
            int x = 19;
            int y = 21;
            int width = 75;
            int height = 47;
            for (int i = 0; i < 3; i++)
            {
                y = y + height;
                x = 19;
                for (int j = 0; j < 3; j++)
                {
                    btnLines[i, j] = new System.Windows.Forms.Button();
                    //создал компонент в памяти
                    //задал внешний вид
                    btnLines[i, j].Location = new System.Drawing.Point(x, y);
                    btnLines[i, j].Name = "btn" + i.ToString() + (j + 1).ToString();
                    btnLines[i, j].Size = new System.Drawing.Size(width, height);
                    btnLines[i, j].TabIndex = 3;
                    btnLines[i, j].Text = i.ToString() + (j + 1).ToString();
                    btnLines[i, j].UseVisualStyleBackColor = true;
                    btnLines[i, j].Click += new System.EventHandler(this.ViewClickAction);
                    //поместил в контейнер, который будет отвечать за отображение
                    this.Controls.Add(btnLines[i, j]);
                    x = x + width;
                }
            }
        }
        private void ViewClickAction(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    int iposition=0;
                    int jposition=0;
                    findViewItem(btn, out iposition, out jposition);
                    string viewValue = string.Empty;
                    bool canSetValue = controller.sendPosition(iposition, jposition, out viewValue);
                    if (canSetValue)
                    {
                        btn.Text = viewValue;
                    }
                }
            }
        }
        private void findViewItem(Button btn, out int iposition, out int jposition)
        {
           iposition = -1;
           jposition = -1;
            for (int i = 0; i < 3; i++)
            {
                bool f = false;
                for (int j = 0; j < 3; j++)
                {
                    if (btnLines[i, j] == btn)
                    {
                        iposition = i;
                        jposition = j;
                        f = true;
                        break;
                    }
                }
                if (f) break;
            }
        }
        public void sendGameOver(string message)
        {
            MessageBox.Show(message, 
                "Резльтат игры!", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Information);
        }
    }
}
