﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    public partial class View2 : Form, TicTacToeMVC.IView
    {
        private TicTacToeController controller;
        public View2()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            CreateView();

        }
        private void CreateView()
        {
            dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ViewClickAction);

            PopulateDataGridView();
        }
        private void PopulateDataGridView()
        {
            // Add columns to the DataGridView.
            dgv.ColumnCount = 3;
            // Set the properties of the DataGridView columns.
            dgv.Columns[0].Name = "ID";
            dgv.Columns[1].Name = "Name";
            dgv.Columns[2].Name = "City";
            dgv.Columns["ID"].HeaderText = "ID";
            dgv.Columns["Name"].HeaderText = "Name";
            dgv.Columns["City"].HeaderText = "City";
            // Add rows of data to the DataGridView.
            dgv.Rows.Add(new string[] { "11", "12", "13" });
            dgv.Rows.Add(new string[] { "21", "22", "23" });
            dgv.Rows.Add(new string[] { "31", "32", "33" });
            // Autosize the columns.
            dgv.AutoResizeColumns();
        }
        private void ViewClickAction(object sender, DataGridViewCellEventArgs e)
        {
            int numRow = e.RowIndex;
            //dgv.CurrentRow.Cells[e.ColumnIndex].Style.BackColor = Color.Red;
            int iposition = e.RowIndex;
            int jposition = e.ColumnIndex;
            string viewValue = string.Empty;
            bool canSetValue = controller.sendPosition(iposition, jposition, out viewValue);
            if (canSetValue)
            {
                dgv.CurrentRow.Cells[e.ColumnIndex].Value = viewValue; 
            }
        }
        public void sendGameOver(string message)
        {
            MessageBox.Show(message,
                "Резльтат игры!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
    }
}
