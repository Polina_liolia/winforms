﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeWinForms
{
    public partial class MainForm : Form
    {
        private Label[] all_labels = new Label[9];
        private bool togglePlayer = Convert.ToBoolean(new Random().Next(0, 1)); // 0 - false, X - true

        public MainForm()
        {
            InitializeComponent();
            myInitializeComponent();
        }

        private void myInitializeComponent()
        {
            int start_x = 5,
                start_y = 5,
                space_horisontal = 7,
                space_vertical = 7;

            for (int i = 0; i < all_labels.Length; i++)
            {
                all_labels[i] = new Label();
                all_labels[i].Size = new Size(50, 50);
                all_labels[i].TextAlign = ContentAlignment.MiddleCenter;
                all_labels[i].Location = new Point(start_x + (all_labels[i].Width + space_horisontal) * (i/3+1), (start_y + (all_labels[i].Height + space_vertical) * (i%3+1)));
                all_labels[i].Name = string.Format("lbl_{0}{1}", (i / 3) + 1, (i % 3) + 1);
                all_labels[i].TabIndex = i + 1;
                all_labels[i].ForeColor = Color.Red;
                all_labels[i].Text = string.Empty;
                all_labels[i].Click += new EventHandler(this.labels_Click);
                all_labels[i].BorderStyle = BorderStyle.FixedSingle;
                Controls.Add(all_labels[i]); //помещаем в контейнер (в данном случае - форма)
            }
            this.Invalidate();
            SuspendLayout(); //применить стили Windows к созданным компонентам (если они заданы)
        }

        private void labels_Click (object sender, EventArgs e)
        {
            if (sender is Label)
            {
                Label lbl = sender as Label;
                if (lbl != null)
                {
                    lbl.Text = togglePlayer ? "X" : "0";
                    togglePlayer = !togglePlayer;
                    lbl_Player.Text = togglePlayer ? "X" : "0";
                }

            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lbl_Player.Text = togglePlayer ? "X" : "0";
        }
    }
}
