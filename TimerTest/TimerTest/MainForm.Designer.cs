﻿namespace TimerTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnStopTimer = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.btnMoveMsg = new System.Windows.Forms.Button();
            this.timerFormText = new System.Windows.Forms.Timer(this.components);
            this.btn_StopCount = new System.Windows.Forms.Button();
            this.timerCount = new System.Windows.Forms.Timer(this.components);
            this.btnSnowFall = new System.Windows.Forms.Button();
            this.timerSnowfall = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTime.ForeColor = System.Drawing.Color.DarkRed;
            this.lblTime.Location = new System.Drawing.Point(118, 110);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(70, 25);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStopTimer
            // 
            this.btnStopTimer.Location = new System.Drawing.Point(13, 192);
            this.btnStopTimer.Name = "btnStopTimer";
            this.btnStopTimer.Size = new System.Drawing.Size(75, 23);
            this.btnStopTimer.TabIndex = 1;
            this.btnStopTimer.Text = "Stop Timer";
            this.btnStopTimer.UseVisualStyleBackColor = true;
            this.btnStopTimer.Click += new System.EventHandler(this.btnStopTimer_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMsg.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblMsg.Location = new System.Drawing.Point(13, 13);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(120, 22);
            this.lblMsg.TabIndex = 2;
            this.lblMsg.Text = "Привет! Ура!";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // btnMoveMsg
            // 
            this.btnMoveMsg.Location = new System.Drawing.Point(113, 192);
            this.btnMoveMsg.Name = "btnMoveMsg";
            this.btnMoveMsg.Size = new System.Drawing.Size(75, 23);
            this.btnMoveMsg.TabIndex = 3;
            this.btnMoveMsg.Text = "Move it?";
            this.btnMoveMsg.UseVisualStyleBackColor = true;
            this.btnMoveMsg.Click += new System.EventHandler(this.btnMoveMsg_Click);
            // 
            // timerFormText
            // 
            this.timerFormText.Enabled = true;
            this.timerFormText.Tick += new System.EventHandler(this.timerFormText_Tick);
            // 
            // btn_StopCount
            // 
            this.btn_StopCount.Location = new System.Drawing.Point(224, 191);
            this.btn_StopCount.Name = "btn_StopCount";
            this.btn_StopCount.Size = new System.Drawing.Size(75, 23);
            this.btn_StopCount.TabIndex = 4;
            this.btn_StopCount.Text = "Stop";
            this.btn_StopCount.UseVisualStyleBackColor = true;
            this.btn_StopCount.Click += new System.EventHandler(this.btn_StopCount_Click);
            // 
            // timerCount
            // 
            this.timerCount.Interval = 1000;
            this.timerCount.Tick += new System.EventHandler(this.timerCount_Tick);
            // 
            // btnSnowFall
            // 
            this.btnSnowFall.Location = new System.Drawing.Point(336, 190);
            this.btnSnowFall.Name = "btnSnowFall";
            this.btnSnowFall.Size = new System.Drawing.Size(75, 23);
            this.btnSnowFall.TabIndex = 5;
            this.btnSnowFall.Text = "Let it snow!";
            this.btnSnowFall.UseVisualStyleBackColor = true;
            this.btnSnowFall.Click += new System.EventHandler(this.btnSnowFall_Click);
            // 
            // timerSnowfall
            // 
            this.timerSnowfall.Tick += new System.EventHandler(this.timerSnowfall_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 262);
            this.Controls.Add(this.btnSnowFall);
            this.Controls.Add(this.btn_StopCount);
            this.Controls.Add(this.btnMoveMsg);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnStopTimer);
            this.Controls.Add(this.lblTime);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnStopTimer;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button btnMoveMsg;
        private System.Windows.Forms.Timer timerFormText;
        private System.Windows.Forms.Button btn_StopCount;
        private System.Windows.Forms.Timer timerCount;
        private System.Windows.Forms.Button btnSnowFall;
        private System.Windows.Forms.Timer timerSnowfall;
    }
}

