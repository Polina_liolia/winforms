﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerTest
{
    /*
     ДЗ
     1. На кнопке Stop, которая изначально не активна, запустить отсчет 9, 8, 7...0
     при достижении ноля показать на ней надпись Stop и сделать её кликабельной
     2. Добавить под движением метки движение красного круга
         */
    public partial class MainForm : Form
    {
        private string caption = "__________Работа с Таймерами!";
        private bool textMovingDirection = true;
        private bool ellipseMovingDirection = true;
        private int count = 9;

        private bool ellipse1MovingDirection = true;
        private bool ellipse2MovingDirection = true;
        private bool ellipse3MovingDirection = true;

        private Rectangle ellipse = new Rectangle(20, 45, 25, 25);
        private Rectangle ellipse1 = new Rectangle(50, 45, 25, 25);
        private Rectangle ellipse2 = new Rectangle(80, 45, 25, 25);
        private Rectangle ellipse3 = new Rectangle(110, 45, 25, 25);

        public MainForm()
        {
            InitializeComponent();
            // lblTime.Text = DateTime.Now.ToLongTimeString(); //аналогично использованию
            //в Form1_Load
            this.Text = caption;   
        }

        private void moveElementHorisontal<T>(ref T el, int startX, int step, ref bool direction) where T : Control
        {
        int workWidth = this.ClientRectangle.Width - el.Width;
        //calculated next position:
        int currentPosition = direction ? el.Left + step :
            el.Left - step;
        //change direction if needed:
        direction = currentPosition >= workWidth ? false :
                 currentPosition <= startX ? true 
                 : direction;
            //correcting current position to aviod text outgoing:
            currentPosition = currentPosition > workWidth ? workWidth :
                currentPosition < startX ? startX : currentPosition;
            el.Left = currentPosition;
        }

        private void moveEllipseHorisontal(ref Rectangle ellipse, int startX, int step, ref bool direction)
        {
            int ellipseWorkWidth = this.ClientRectangle.Width - ellipse.Width;
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Point ellipsePosition = new Point();
            ellipsePosition.X = direction ? (ellipse.Left + step) :
                (ellipse.Left - step);
            ellipsePosition.Y = ellipse.Top;
            //change direction if needed:
            direction = ellipse.Left >= ellipseWorkWidth ? false :
                 ellipse.Left <= startX ? true
                 : direction;
            //correcting current position to aviod ellipse outgoing:
            ellipsePosition.X = ellipsePosition.X > ellipseWorkWidth ? ellipseWorkWidth :
                ellipsePosition.X < startX ? startX : ellipsePosition.X;
            ellipse.Location = ellipsePosition;
            gr.DrawEllipse(Pens.Red, ellipse);
            gr.FillEllipse(Brushes.Red, ellipse);
          
        }

        private void moveEllipseVertical(ref Rectangle ellipse, int startY, int step, ref bool direction)
        {
            int ellipseWorkBottom = this.ClientRectangle.Height - ellipse.Height;
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Point ellipsePosition = new Point();
            ellipsePosition.Y = direction ? (ellipse.Top + step) :
                (ellipse.Top - step);
            ellipsePosition.X = ellipse.Left;
            //change direction if needed:
            direction = ellipse.Top >= ellipseWorkBottom ? false :
                 ellipse.Top <= startY ? true : direction;
            //correcting current position to aviod ellipse outgoing:
            ellipsePosition.Y = ellipsePosition.Y > ellipseWorkBottom ? ellipseWorkBottom :
                ellipsePosition.Y < startY ? startY : ellipsePosition.Y;
            ellipse.Location = ellipsePosition;
            gr.DrawEllipse(Pens.Red, ellipse);
            gr.FillEllipse(Brushes.Red, ellipse);
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           lblTime.Text = DateTime.Now.ToLongTimeString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
        }

        private void btnStopTimer_Click(object sender, EventArgs e)
        {
            timer1.Stop();
           // timer1.Enabled = false;//то же самое
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            //int maxWidth = this.Width - lblMsg.Width; //this.Width включает рамку окна
            //this.Height включает заголовок окна

            //Moving text
            moveElementHorisontal<Label>(ref lblMsg, 0, 10, ref textMovingDirection);

            //Moving ellipse:
            moveEllipseHorisontal(ref ellipse, 20, 10, ref ellipseMovingDirection);
            this.Invalidate();
        }

        private void btnMoveMsg_Click(object sender, EventArgs e)
        {
            timer2.Interval = 1000;
            timer2.Start();
            btnMoveMsg.Text = "Running...";
            btnMoveMsg.Enabled = false;
        }

        private void timerFormText_Tick(object sender, EventArgs e)
        {
            if (this.Text.Length > 0)
            {
                string newCaption = this.Text;
                this.Text = newCaption.Remove(0, 1);
                this.Update(); // обязательно для lbl, btn... для form - избыточно
            }
            else
                this.Text = caption;

        }

        private void btn_StopCount_Click(object sender, EventArgs e)
        {
            btn_StopCount.Enabled = false;
            timerCount.Start();
        }

        private void timerCount_Tick(object sender, EventArgs e)
        {
            if (count == -1)
            {
                count = 9;
                timerCount.Stop();
                btn_StopCount.Text = "Stop";
                btn_StopCount.Enabled = true;
                return;
            }
            btn_StopCount.Text = count--.ToString();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            //drawing a red circle:
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawEllipse(Pens.Red, ellipse);
            gr.FillEllipse(Brushes.Red, ellipse);

            //drawing snow:
            gr.DrawEllipse(Pens.Blue, ellipse1);
            gr.FillEllipse(Brushes.Blue, ellipse1);
            gr.DrawEllipse(Pens.Blue, ellipse2);
            gr.FillEllipse(Brushes.Blue, ellipse2);
            gr.DrawEllipse(Pens.Blue, ellipse3);
            gr.FillEllipse(Brushes.Blue, ellipse3);

        }

        private void timerSnowfall_Tick(object sender, EventArgs e)
        {
            //Moving ellipses:
            moveEllipseVertical(ref ellipse1, 45, 2, ref ellipse1MovingDirection);
            moveEllipseVertical(ref ellipse2, 45, 3, ref ellipse2MovingDirection);
            moveEllipseVertical(ref ellipse3, 45, 5, ref ellipse3MovingDirection);
            this.Invalidate();
        }

        private void btnSnowFall_Click(object sender, EventArgs e)
        {
            timerSnowfall.Start();
        }
    }
}
