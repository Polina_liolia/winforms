﻿namespace UserControlsTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileControl1 = new OpenSaveFile.FileControl();
            this.UC_fileControl1 = new OpenSaveFile.FileControl();
            this.UC_fileControl2 = new OpenSaveFile.FileControl();
            this.UC_fileControl3 = new OpenSaveFile.FileControl();
            this.SuspendLayout();
            // 
            // fileControl1
            // 
            this.fileControl1.Location = new System.Drawing.Point(12, 22);
            this.fileControl1.Name = "fileControl1";
            this.fileControl1.Size = new System.Drawing.Size(476, 62);
            this.fileControl1.TabIndex = 0;
            // 
            // UC_fileControl1
            // 
            this.UC_fileControl1.Location = new System.Drawing.Point(12, 50);
            this.UC_fileControl1.Name = "UC_fileControl1";
            this.UC_fileControl1.Size = new System.Drawing.Size(476, 62);
            this.UC_fileControl1.TabIndex = 0;
            this.UC_fileControl1.BtnClick += new System.EventHandler(this.userControlsClick);
            // 
            // UC_fileControl2
            // 
            this.UC_fileControl2.Location = new System.Drawing.Point(12, 136);
            this.UC_fileControl2.Name = "UC_fileControl2";
            this.UC_fileControl2.Size = new System.Drawing.Size(476, 62);
            this.UC_fileControl2.TabIndex = 1;
            this.UC_fileControl2.BtnClick += new System.EventHandler(this.userControlsClick);
            // 
            // UC_fileControl3
            // 
            this.UC_fileControl3.Location = new System.Drawing.Point(13, 234);
            this.UC_fileControl3.Name = "UC_fileControl3";
            this.UC_fileControl3.Size = new System.Drawing.Size(476, 62);
            this.UC_fileControl3.TabIndex = 2;
            this.UC_fileControl3.BtnClick += new System.EventHandler(this.userControlsClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 450);
            this.Controls.Add(this.UC_fileControl3);
            this.Controls.Add(this.UC_fileControl2);
            this.Controls.Add(this.UC_fileControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private OpenSaveFile.FileControl fileControl1;
        private OpenSaveFile.FileControl UC_fileControl1;
        private OpenSaveFile.FileControl UC_fileControl2;
        private OpenSaveFile.FileControl UC_fileControl3;
    }
}

