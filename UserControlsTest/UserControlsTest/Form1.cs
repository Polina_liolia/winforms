﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenSaveFile;

namespace UserControlsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UC_fileControl1.Text = "Some text";
            
        }

        private void userControlsClick(object sender, EventArgs e)
        {
            if(sender is FileControl)
            {
                FileControl uk = sender as FileControl;
                if (uk != null)
                {
                    uk.OnBtnClick(sender, e);
                }
            }
        }
    }
}
