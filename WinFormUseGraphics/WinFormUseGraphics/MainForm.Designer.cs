﻿namespace WinFormUseGraphics
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblTask = new System.Windows.Forms.Label();
            this.cmbTasks = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtX = new System.Windows.Forms.TextBox();
            this.txtY = new System.Windows.Forms.TextBox();
            this.btnView = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pBox = new System.Windows.Forms.PictureBox();
            this.panelGraphic = new System.Windows.Forms.Panel();
            this.lbl20_23 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl22 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl25rec = new System.Windows.Forms.Label();
            this.lbl5rec = new System.Windows.Forms.Label();
            this.lbl10rec = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            this.panelGraphic.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTask.Location = new System.Drawing.Point(12, 9);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(62, 25);
            this.lblTask.TabIndex = 0;
            this.lblTask.Text = "Task:";
            // 
            // cmbTasks
            // 
            this.cmbTasks.FormattingEnabled = true;
            this.cmbTasks.Items.AddRange(new object[] {
            "Задание 21",
            "Задание 22",
            "Задание 23"});
            this.cmbTasks.Location = new System.Drawing.Point(12, 37);
            this.cmbTasks.Name = "cmbTasks";
            this.cmbTasks.Size = new System.Drawing.Size(121, 21);
            this.cmbTasks.TabIndex = 1;
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnStart.Location = new System.Drawing.Point(27, 133);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(121, 28);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Points coords:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "X:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Y:";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(48, 230);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(100, 20);
            this.txtX.TabIndex = 6;
            this.txtX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtX_KeyPress);
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(48, 254);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(100, 20);
            this.txtY.TabIndex = 7;
            this.txtY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtY_KeyPress);
            // 
            // btnView
            // 
            this.btnView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnView.Location = new System.Drawing.Point(73, 298);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(532, 320);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "© 2017 Sergey Ostruk";
            // 
            // pBox
            // 
            this.pBox.Image = ((System.Drawing.Image)(resources.GetObject("pBox.Image")));
            this.pBox.Location = new System.Drawing.Point(651, 301);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(56, 32);
            this.pBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBox.TabIndex = 10;
            this.pBox.TabStop = false;
            // 
            // panelGraphic
            // 
            this.panelGraphic.BackColor = System.Drawing.Color.LightGray;
            this.panelGraphic.Controls.Add(this.lbl20_23);
            this.panelGraphic.Controls.Add(this.lbl3);
            this.panelGraphic.Controls.Add(this.lbl22);
            this.panelGraphic.Controls.Add(this.lbl12);
            this.panelGraphic.Controls.Add(this.lbl25rec);
            this.panelGraphic.Controls.Add(this.lbl5rec);
            this.panelGraphic.Controls.Add(this.lbl10rec);
            this.panelGraphic.Controls.Add(this.lbl20);
            this.panelGraphic.Controls.Add(this.lbl10);
            this.panelGraphic.Controls.Add(this.lbl15);
            this.panelGraphic.Controls.Add(this.lbl5);
            this.panelGraphic.Location = new System.Drawing.Point(198, 12);
            this.panelGraphic.Name = "panelGraphic";
            this.panelGraphic.Size = new System.Drawing.Size(447, 285);
            this.panelGraphic.TabIndex = 11;
            this.panelGraphic.Visible = false;
            this.panelGraphic.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphic_Paint);
            // 
            // lbl20_23
            // 
            this.lbl20_23.AutoSize = true;
            this.lbl20_23.Location = new System.Drawing.Point(16, 42);
            this.lbl20_23.Name = "lbl20_23";
            this.lbl20_23.Size = new System.Drawing.Size(19, 13);
            this.lbl20_23.TabIndex = 10;
            this.lbl20_23.Text = "20";
            this.lbl20_23.Visible = false;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(22, 213);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(13, 13);
            this.lbl3.TabIndex = 9;
            this.lbl3.Text = "3";
            this.lbl3.Visible = false;
            // 
            // lbl22
            // 
            this.lbl22.AutoSize = true;
            this.lbl22.Location = new System.Drawing.Point(264, 257);
            this.lbl22.Name = "lbl22";
            this.lbl22.Size = new System.Drawing.Size(19, 13);
            this.lbl22.TabIndex = 8;
            this.lbl22.Text = "22";
            this.lbl22.Visible = false;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.Location = new System.Drawing.Point(158, 257);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(19, 13);
            this.lbl12.TabIndex = 7;
            this.lbl12.Text = "12";
            this.lbl12.Visible = false;
            // 
            // lbl25rec
            // 
            this.lbl25rec.AutoSize = true;
            this.lbl25rec.Location = new System.Drawing.Point(304, 257);
            this.lbl25rec.Name = "lbl25rec";
            this.lbl25rec.Size = new System.Drawing.Size(19, 13);
            this.lbl25rec.TabIndex = 6;
            this.lbl25rec.Text = "25";
            this.lbl25rec.Visible = false;
            // 
            // lbl5rec
            // 
            this.lbl5rec.AutoSize = true;
            this.lbl5rec.Location = new System.Drawing.Point(90, 257);
            this.lbl5rec.Name = "lbl5rec";
            this.lbl5rec.Size = new System.Drawing.Size(13, 13);
            this.lbl5rec.TabIndex = 5;
            this.lbl5rec.Text = "5";
            this.lbl5rec.Visible = false;
            // 
            // lbl10rec
            // 
            this.lbl10rec.AutoSize = true;
            this.lbl10rec.Location = new System.Drawing.Point(16, 142);
            this.lbl10rec.Name = "lbl10rec";
            this.lbl10rec.Size = new System.Drawing.Size(19, 13);
            this.lbl10rec.TabIndex = 4;
            this.lbl10rec.Text = "10";
            this.lbl10rec.Visible = false;
            // 
            // lbl20
            // 
            this.lbl20.AutoSize = true;
            this.lbl20.Location = new System.Drawing.Point(239, 257);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(19, 13);
            this.lbl20.TabIndex = 3;
            this.lbl20.Text = "20";
            this.lbl20.Visible = false;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(133, 257);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(19, 13);
            this.lbl10.TabIndex = 2;
            this.lbl10.Text = "10";
            this.lbl10.Visible = false;
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.Location = new System.Drawing.Point(16, 90);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(22, 13);
            this.lbl15.TabIndex = 1;
            this.lbl15.Text = "15 ";
            this.lbl15.Visible = false;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(22, 191);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(16, 13);
            this.lbl5.TabIndex = 0;
            this.lbl5.Text = "5 ";
            this.lbl5.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 341);
            this.Controls.Add(this.panelGraphic);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.txtY);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.cmbTasks);
            this.Controls.Add(this.lblTask);
            this.Name = "MainForm";
            this.Text = "HomeTask: Use Graphics";
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            this.panelGraphic.ResumeLayout(false);
            this.panelGraphic.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.ComboBox cmbTasks;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pBox;
        private System.Windows.Forms.Panel panelGraphic;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl5rec;
        private System.Windows.Forms.Label lbl10rec;
        private System.Windows.Forms.Label lbl25rec;
        private System.Windows.Forms.Label lbl22;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl20_23;
    }
}

