﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormUseGraphics
{
    public partial class MainForm : Form
    {
        private string MyTextX = String.Empty;
        private string MyTextY = String.Empty;
        public MainForm()
        {
            InitializeComponent();
            cmbTasks.SelectedIndex = 0;
        }
        private void drawLines(Graphics gr, Pen myPen)
        {
            gr.DrawLine(myPen, new Point(panelGraphic.Location.X - 153, panelGraphic.Location.Y + 234),
                    new Point(panelGraphic.Location.X + 180, panelGraphic.Location.Y + 234));
            gr.DrawLine(myPen, new Point(panelGraphic.Location.X - 153, panelGraphic.Location.Y + 233),
                new Point(panelGraphic.Location.X - 153, panelGraphic.Location.Y + 10));
            gr.DrawString("X", this.Font, Brushes.Black, panelGraphic.Location.X + 185, panelGraphic.Location.Y + 245);
            gr.DrawString("Y", this.Font, Brushes.Black, panelGraphic.Location.Y + 8, panelGraphic.Location.Y);
            gr.DrawString("/", this.Font, Brushes.Black, panelGraphic.Location.Y + 28, panelGraphic.Location.Y + 8);
            gr.DrawString("\\", this.Font, Brushes.Black, panelGraphic.Location.Y + 31, panelGraphic.Location.Y + 8);
            gr.DrawString(">", this.Font, Brushes.Black, panelGraphic.Location.X + 172, panelGraphic.Location.Y + 227);
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(panelGraphic.Handle);
            Pen myPen = new Pen(Color.Black, 1);
            if (cmbTasks.Text == "Задание 21")
            {
                panelGraphic.Refresh();
                lbl3.Visible = false;
                lbl12.Visible = false;
                lbl22.Visible = false;
                lbl20_23.Visible = false;
                lbl5.Visible = true;
                lbl10.Visible = true;
                lbl15.Visible = true;
                lbl20.Visible = true;
                lbl5rec.Visible = false;
                lbl10rec.Visible = false;
                lbl25rec.Visible = false;
                panelGraphic.Visible = true;          
                Brush myBrush = Brushes.Red;
                drawLines(gr, myPen);
                gr.DrawLine(myPen, new Point(lbl15.Location.X + 35, lbl15.Location.Y + 5),
                   new Point(lbl15.Location.X + 15, lbl15.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl5.Location.X + 30, lbl5.Location.Y + 5),
                    new Point(lbl5.Location.X + 15, lbl5.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl15.Location.X + 35, lbl15.Location.Y + 5),
                  new Point(lbl15.Location.X + 15, lbl15.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl10.Location.X + 10, lbl10.Location.Y),
                    new Point(lbl10.Location.X + 10, lbl10.Location.Y - 18));
                gr.DrawLine(myPen, new Point(lbl20.Location.X + 10, lbl20.Location.Y),
                    new Point(lbl20.Location.X + 10, lbl20.Location.Y - 18));               
                gr.DrawEllipse(new Pen(Color.Black, 2), panelGraphic.Location.X - 55, panelGraphic.Location.Y + 75, 110, 110);
                gr.FillEllipse(myBrush, panelGraphic.Location.X - 55, panelGraphic.Location.Y + 75, 110, 110);
            }
            if (cmbTasks.Text == "Задание 22")
            {
                panelGraphic.Refresh();
                lbl3.Visible = false;
                lbl12.Visible = false;
                lbl22.Visible = false;
                lbl20_23.Visible = false;
                lbl5.Visible = false;
                lbl20.Visible = false;
                lbl10.Visible = false;
                lbl15.Visible = true;
                lbl5rec.Visible = true;
                lbl10rec.Visible = true;             
                lbl25rec.Visible = true;
                panelGraphic.Visible = true;
                Brush myBrush = Brushes.Red;
                drawLines(gr, myPen);
                gr.DrawLine(myPen, new Point(lbl15.Location.X + 35, lbl15.Location.Y + 5),
                   new Point(lbl15.Location.X + 15, lbl15.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl10rec.Location.X + 35, lbl10rec.Location.Y + 5),
                    new Point(lbl10rec.Location.X + 15, lbl10rec.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl15.Location.X + 35, lbl15.Location.Y + 5),
                  new Point(lbl15.Location.X + 15, lbl15.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl5rec.Location.X + 5, lbl5rec.Location.Y),
                    new Point(lbl5rec.Location.X + 5, lbl5rec.Location.Y - 18));
                gr.DrawLine(myPen, new Point(lbl25rec.Location.X + 10, lbl25rec.Location.Y),
                    new Point(lbl25rec.Location.X + 10, lbl25rec.Location.Y - 18));
                gr.DrawRectangle(new Pen(Color.Black, 2), lbl15.Location.X + 80, panelGraphic.Location.Y + 82, 218, 55);
                gr.FillRectangle(myBrush, lbl15.Location.X + 80, panelGraphic.Location.Y + 82, 218, 55);
            }
            if (cmbTasks.Text == "Задание 23")
            {
                panelGraphic.Refresh();
                lbl5.Visible = false;
                lbl3.Visible = true;
                lbl12.Visible = true;
                lbl22.Visible = true;
                lbl20_23.Visible = true;
                lbl20.Visible = true;
                lbl10.Visible = true;
                lbl15.Visible = true;       
                lbl5rec.Visible = false;
                lbl10rec.Visible = false;
                lbl25rec.Visible = false;
                panelGraphic.Visible = true;
                Brush myBrush = Brushes.Red;
                Brush myBrush2 = Brushes.Green;              
                drawLines(gr, myPen);
                gr.DrawLine(myPen, new Point(lbl3.Location.X + 30, lbl3.Location.Y + 5),
                    new Point(lbl3.Location.X + 15, lbl3.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl15.Location.X + 35, lbl15.Location.Y + 5),
                  new Point(lbl15.Location.X + 15, lbl15.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl20_23.Location.X + 35, lbl20_23.Location.Y + 5),
                  new Point(lbl20_23.Location.X + 15, lbl20_23.Location.Y + 5));
                gr.DrawLine(myPen, new Point(lbl10.Location.X + 10, lbl10.Location.Y),
                    new Point(lbl10.Location.X + 10, lbl10.Location.Y - 18));
                gr.DrawLine(myPen, new Point(lbl12.Location.X + 10, lbl12.Location.Y),
                    new Point(lbl12.Location.X + 10, lbl12.Location.Y - 18));
                gr.DrawLine(myPen, new Point(lbl20.Location.X + 10, lbl20.Location.Y),
                    new Point(lbl20.Location.X + 10, lbl20.Location.Y - 18));
                gr.DrawLine(myPen, new Point(lbl22.Location.X + 10, lbl22.Location.Y),
                    new Point(lbl22.Location.X + 10, lbl22.Location.Y - 18));
                gr.DrawRectangle(new Pen(Color.Black, 2), lbl12.Location.X + 11, lbl20_23.Location.Y, 80, 205);
                gr.FillRectangle(myBrush, lbl12.Location.X + 11, lbl20_23.Location.Y, 80, 205);
                gr.DrawEllipse(new Pen(Color.Black, 2), lbl15.Location.X + 130, lbl15.Location.Y, 130, 130);
                gr.FillEllipse(new SolidBrush(Color.FromArgb(200, 0, 102, 0)), lbl15.Location.X + 130, lbl15.Location.Y, 130, 130);               
            }
        }

        private void panelGraphic_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(panelGraphic.Handle);
            panelGraphic.Visible = true;
            drawLines(gr, new Pen(Color.Black, 1));
        }

        private void btnView_Click(object sender, EventArgs e)
        {         
            Graphics gr = Graphics.FromHwnd(panelGraphic.Handle);
            if (txtX.Text == "" || txtY.Text == "" || Convert.ToInt32(txtX.Text) > 30 || Convert.ToInt32(txtY.Text) > 20 || Convert.ToInt32(txtX.Text) < 0 || Convert.ToInt32(txtY.Text) < 0)
            {
                txtX.BackColor = Color.Red;
                txtY.BackColor = Color.Red;
            }
            else
            {
                txtX.BackColor = Color.White;
                txtY.BackColor = Color.White;
                gr.DrawEllipse(new Pen(Color.Black, 2), Convert.ToInt32(txtX.Text) * 10 + 45, 245 - Convert.ToInt32(txtY.Text) * 10, 1, 1);
            }
        }

        private void txtX_KeyPress(object sender, KeyPressEventArgs e)
        {
            string tmp = "0123456789\b";
            if (tmp.Contains(e.KeyChar.ToString()))
            {
                MyTextX = txtX.Text;
            }
            else
            {
                e.KeyChar = (char)0;
            }
        }

        private void txtY_KeyPress(object sender, KeyPressEventArgs e)
        {
            string tmp = "0123456789\b";
            if (tmp.Contains(e.KeyChar.ToString()))
            {
                MyTextY = txtX.Text;
            }
            else
            {
                e.KeyChar = (char)0;
            }

        }
    }
}
