﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsControlsTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            //Пример работы с основными компонентами: label, TextBox, ComboBox, FileDialogs
            cboxCity.SelectedItem = "Kiev"; //some element from the items collection in combobox
        }
        private void btnClickMe_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Привет, " + txtName.Text);
            string messageText = String.Format("Привет, {0}", txtName.Text);
            string titleCaption = "Важное сообщение";
            DialogResult result = MessageBox.Show(messageText, //Текст сообщения
                titleCaption, //Надпись-зашоловок окошка
                MessageBoxButtons.YesNoCancel, //Типы кнопок в заголовке
                MessageBoxIcon.Information //тип окна диалога
                );
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                this.Text = "Спасибо";
            }
            else {
                this.Text = "Нет-и ладно";
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void cboxCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCity.Text = cboxCity.Text;
        }

        private void btnSelectCity_Click(object sender, EventArgs e)
        {
            if (cboxCity.SelectedItem.ToString() == "Kharkov")
                MessageBox.Show("You choose Kharkov", "Right choise",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show(string.Format("You choose {0}", cboxCity.SelectedItem), "Some city from cbox",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
