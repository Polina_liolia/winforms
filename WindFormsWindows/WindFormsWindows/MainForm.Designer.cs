﻿namespace WindFormsWindows
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnModal = new System.Windows.Forms.Button();
            this.btnNotModal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnModal
            // 
            this.btnModal.Location = new System.Drawing.Point(5, 74);
            this.btnModal.Name = "btnModal";
            this.btnModal.Size = new System.Drawing.Size(144, 23);
            this.btnModal.TabIndex = 0;
            this.btnModal.Text = "Модальное окно";
            this.btnModal.UseVisualStyleBackColor = true;
            this.btnModal.Click += new System.EventHandler(this.btnModal_Click);
            // 
            // btnNotModal
            // 
            this.btnNotModal.Location = new System.Drawing.Point(5, 103);
            this.btnNotModal.Name = "btnNotModal";
            this.btnNotModal.Size = new System.Drawing.Size(144, 23);
            this.btnNotModal.TabIndex = 1;
            this.btnNotModal.Text = "Немодальное окно";
            this.btnNotModal.UseVisualStyleBackColor = true;
            this.btnNotModal.Click += new System.EventHandler(this.btnNotModal_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(154, 189);
            this.Controls.Add(this.btnNotModal);
            this.Controls.Add(this.btnModal);
            this.Name = "MainForm";
            this.Text = "WindFormsWindows";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnModal;
        private System.Windows.Forms.Button btnNotModal;
    }
}

