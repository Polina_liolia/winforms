﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindFormsWindows
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            //Пример работы с модальными/немодальными окнами
        }
        private void btnModal_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.ShowDialog(); //Модальное - все окна диалогов
        }
        private void btnNotModal_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show(); //Обычное - перекрывающееся окно
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.FillEllipse(Brushes.CadetBlue, 120, 150, 25, 25);
        }
    }
}
